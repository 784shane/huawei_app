Huawei Facebook Application
===========================

CMS
---
/cms
/cms/experiences
/cms/stockimages
/cms/csv

API
---

> GET /api/stockimages
Will return a list of stock images (id, url, thumb) in JSON  


> POST /api/experience
It will create an image in the server and return the experience (id, url, shortUrl, src) as a JSON objec.
URL / short URL are the shareable URLs to use later to publish the Open Graph story.
SRC refers to the image itself

Parameters:
provider
(image_url|image_id) (string|int)
text (string)
x (float)
y (float)
width (float)
height (float)
tags (Comma-separated list of facebook IDs as string)


CMS Password
------------

From the command line: 

> htdigest -c [file to generate] 'Huawei Experiences CMS' admin

Type and repeat password, and point that file in the local config:

<?php
'cms_auth' => array(
    'passwd_file' => [generated file]
),
?>


Introduction
------------
1. Huawei Facebook Application for G6 Smartphone.
2. This project uses ZF2, Doctrine, Backbone, SASS
3. Requires MaidoCommon
4. Requires a Facebook App and Instagram App

Installation
------------
1. Clone project from Bitbucket `git clone git@bitbucket.org:maido/huawei.git`
2. Run `composer install`
3. Config files setup - located in `/config/autoload/`
	1. Copy `local.php.dist` as `local.php` and add your credentials
	2. Copy `doctrine.local.php.dist` as `doctrine.local.php` and add your credentials
	3. Database name should be `huawei`
4. MaidoCommon is used so run `git submodule init` and then `git submodule update`
5. Doctrine is used for database so create a database called `huawei` 
6. From project root run `./vendor/bin/doctrine-module migrations:migrate` to add the tables


Additional Notes
----------------
1. A Front-End boilerplate is used for this project, documentation is at `https://github.com/zizther/fe-docs`
2. SASS / COMPASS is used for this project so run `compass watch .` in `/public/assets/css` or use CodeKit
3. Backbone is used for this project
4. Certain errors are logged in the database under the table `log` 

Doctrine Notes
--------------
1. Doctrine migrations are located in `/data/DoctrinORMModule/Migrations`
2. Migrations naming convention is a combination of the word 'Version' and the current timestamp
3. In installation step 6, migrations are run in order so will ensure the database is up-to-date
4. To create a new migration, run `./vendor/bin/doctrine-module migrations:generate` from project root
5. A new migration file will be added with an `up()` and `down()` method
6. The `up()` method defines changes made to the database
7. The `down()` method reverses everything in the `up()` method and is used when rolling back to a migration
8. To roll back a migration, run `./vendor/bin/doctrine-module migrations:migrate MIGRATION_FILE_NAME` from project root
9. If you do rollback or create a new migration, the database models/entities needs to be updated so run `./vendor/bin/doctrine-module orm:generate-entities module/Application/src/Application/Entity/` from project root
10. You will also need to update getters and setters so run `./vendor/bin/doctrine-module orm:generate-entities ./module/Application/src/ --generate-annotations=true` from project root