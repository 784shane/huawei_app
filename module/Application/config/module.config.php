<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),

            'redirect' => array(
                'type' => 'literal',
                'options' => array(
                    'route'    => '/redirect',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'redirect',
                    ),
                ),
            ),

            'canvas' => array(
                'type' => 'literal',
                'options' => array(
                    'route'    => '/canvasapp',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'canvasRedirect',
                    ),
                ),
            ),

            'instagram-login' => array(
                'type' => 'literal',
                'options' => array(
                    'route'    => '/login/instagram',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Instagram',
                        'action'     => 'handleLogin',
                    ),
                ),
            ),
            
            // RESTful API
            'experience' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/api/experience',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Experience',
                    ),
                ),
            ),

            'stockimages' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/api/stockimages',
                    'defaults' => array(
                        'controller' => 'Application\Controller\StockImage',
                    ),
                ),
            ),

            // Facebook open graph
            'experience-open-graph' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/experience/:id',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                ),
            ),
            'update-recieve-emails' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/user/recieveemails',
                    'defaults' => array(
                        'controller' => 'Application\Controller\User',
                        'action'     => 'updateRecieveEmail',
                    ),
                ),
            ),
            'validate-text' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/user/validatetext',
                    'defaults' => array(
                        'controller' => 'Application\Controller\User',
                        'action'     => 'validateText',
                    ),
                ),
            ),
            //testing route for app
            'app' => array(
                //'type' => 'literal',
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/app[/:id]',
                    'constraints' => array(
                            'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\App',
                        'action'     => 'app',
                        'id' => 'value'
                    ),
                ),
            ),
            //testing route for app
            'app/realdimensions' => array(
                //'type' => 'literal',
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/app/realdimensions',
                    'constraints' => array(
                            'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\App',
                        'action'     => 'realdimensions',
                        'id' => 'value'
                    ),
                ),
            ),

            //testing route for app
            'app/generatephoto' => array(
                //'type' => 'literal',
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/app/generatephoto',
                    'constraints' => array(
                            'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\App',
                        'action'     => 'generatephoto',
                        'id' => 'value'
                    ),
                ),
            ),

            //testing route for app
            'app/imagereturn' => array(
                //'type' => 'literal',
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/app/imagereturn',
                    'constraints' => array(
                            'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\App',
                        'action'     => 'imagereturn',
                        'id' => 'value'
                    ),
                ),
            ),

            //testing route for app
            'app/fblogin' => array(
                //'type' => 'literal',
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/app/fblogin',
                    'constraints' => array(
                            'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\App',
                        'action'     => 'fblogin',
                        'id' => 'value'
                    ),
                ),
            ),

            //testing route for app
            'pinching' => array(
                //'type' => 'literal',
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/pinching',
                    'constraints' => array(
                            'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                        'id' => 'value'
                    ),
                ),
            ),

        ),
    ),
    
    
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\App' => 'Application\Controller\AppController',
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\User' => 'Application\Controller\UserController',
            'Application\Controller\Instagram' => 'Application\Controller\InstagramController',
            'Application\Controller\Experience' => 'Application\Controller\ExperienceController',
            'Application\Controller\StockImage' => 'Application\Controller\StockImageController'
        ),
    ),
    'controller_plugins' => array(
        'invokables' => array(
            'publishMedia' => 'Application\Controller\Plugin\UploadedMediaPublisher',
            'shortUrl' => 'Application\Controller\Plugin\ShortUrl',
            'fetchRemoteImage' => 'Application\Controller\Plugin\FetchRemoteImage',
            'viewportUrl' => 'Application\Controller\Plugin\MediaViewport',
        ),
    ),
    'service_manager' => array(
        'invokables' => array(
            'Application\Service\User' => 'Application\Service\UserService',
            'Application\Service\Experience' => 'Application\Service\ExperienceService',
            'Application\Service\SourceImage' => 'Application\Service\SourceImageService',
            'Application\Auth\Facebook' => 'Application\Auth\Facebook',
            'Application\Renderer\Experience' => 'Application\Renderer\ExperienceRenderer',
            'Application\Renderer\Text' => 'Application\Renderer\ExperienceText',
            'Application\Renderer\Image' => 'Application\Renderer\ExperienceImage',
            'Imagine' => 'Imagine\Imagick\Imagine',
        ),
        'factories' => array(
            'Application\Form\RemoteSourceImage' => 'Application\Form\Service\RemoteSourceImageFormFactory',
            'Application\Form\Experience' => 'Application\Form\Service\ExperienceFormFactory',
            'Facebook' => 'Application\Api\Service\FacebookSdkFactory',
            'Logger' => 'Application\Log\Service\LoggerFactory'
        ),
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),

    'view_helpers' => array(
        'factories' => array(
            'config' => 'Application\View\Service\ViewConfigurationFactory',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),

    'logger' => array(
        'emergency' => array(
            'template' => 'application/mailer/logger-emergency',
            'subject' => 'Huawei app - Attention'
        ),
    ),    


    /**
     * Facebook
     */
    'facebook' => array(
        'app' => array(
            'scope' => array('email'),
            'description' => 'The G6 is designed to let you share your experiences with the world - all you need to do is tell us what your ULTIMATE EXPERIERNCE and you could win a £10,000 experience of your choice!',
            'title' => 'The Huawei Ultimate Experience Competition',
            'image' => '',
        ),
        'open_graph' => array(
            'type' => 'huawei_competition',
            'action' => 'enter',
            'description' => 'The G6 is designed to let you share your experiences with the world - all you need to do is tell us your ULTIMATE EXPERIENCE and you could be in with a chance to win an experience worth £10,000!',
        ),
    ),

    /**
     * Twitter
     */
    'twitter' => array(
        'card' => array(
            'title' => 'Huawei Ultimate Experience Competition',
        ),
        'share' => 'Lorem ipsum dolor sit amet'
    ),

    /**
     * Instagram
     */
    'instagram' => array(
        'authorize_url' => 'https://api.instagram.com/oauth/authorize/?client_id=%s&redirect_uri=%s&response_type=code',
        'access_token_url' => 'https://api.instagram.com/oauth/access_token',
        'recent_media_endpoint' => 'https://api.instagram.com/v1/users/self/media/recent/?count=%s&access_token=%s',
    ),

    /**
     * Banned words
     */
    'banned_words' => array(
        'cazzo', 'merda', 'merde',
    ),

    /**
     * Media viewports
     */
    'viewports' => array(
        'experience' => array(
            'fullsize' => array(),
            'thumbnail' => array(
                array(
                    'method' => 'constrain',
                    'width' => 300,
                ),
            ),
            'open-graph' => array(
                array(
                    'method' => 'resize-cropped',
                    'width' => 300,
                    'height' => 300,
                    'from_x' => 0,
                    'from_y' => 0,
                )
            ),
        ),
        'source_image' => array(
            'fullsize' => array(
            ),
            'thumbnail' => array(
                array(
                    'method' => 'resize-cropped',
                    'width' => 300,
                    'height' => 300,
                ),
            ),
        ),
    ),

    'media_options' => array(
        'public_dir' => 'public/media',
        'public_url' => '/media',
    ),

    /**
     * Source image / crop tool configuration
     */
    
    'experience' => array(
        'minlength' => 1,
        'maxlength' => 255,
        'image' => array(
            'final_width' => 698,
            'final_height' => 698,
            'max_resample' => 10,
            'jpeg_quality' => 95,
            'max_size_bytes' => 1024 * 1024 * 10, // 10 MB
        ),
        'banner' => array(
            'src' => __DIR__ . '/../assets/banner.png',
            'width' => 698,
            'height' => 130,
        ),
        'text' => array(
            'font' => array(
                'family' => array(
                    'bold' => __DIR__ . '/../assets/font/bold.ttf',
                    'regular' => __DIR__ . '/../assets/font/regular.ttf',
                ),
                'color' => array(
                    'dark' => '000000',
                    'light' => 'FFFFFF',
                ),
                'size' => array(
                    'paragraph' => 35,
                    'lead' => 27,
                ),
                'line_height' => 52,
            ),
            'box_width' => 558, // 20px each side for margin
            'box_height' => 658,
            'template' => array(
                'lead' => 'My Ultimate Experience is',
            ),
        ),
    ),
);
