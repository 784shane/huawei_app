<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Experience
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="experience", indexes={@ORM\Index(name="IDX_590C103A76ED395", columns={"user_id"}), @ORM\Index(name="IDX_590C10323E1FCEE", columns={"source_image_id"})})
 * @ORM\Entity
 */
class Experience
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="source_image_id", type="integer", nullable=false)
     */
    private $sourceImageId;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=255, nullable=false)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="x", type="string", length=255, nullable=false)
     */
    private $x;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=false)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=false)
     */
    private $updatedDate;

    /**
     * @var string
     *
     * @ORM\Column(name="y", type="string", length=255, nullable=false)
     */
    private $y;

    /**
     * @var string
     *
     * @ORM\Column(name="width", type="string", length=255, nullable=false)
     */
    private $width;

    /**
     * @var string
     *
     * @ORM\Column(name="height", type="string", length=255, nullable=false)
     */
    private $height;

    /**
     * @var \Application\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \Application\Entity\SourceImage
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\SourceImage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="source_image_id", referencedColumnName="id")
     * })
     */
    private $sourceImage;

    /**
     * @var \Application\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Media")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="rendered_media_id", referencedColumnName="id")
     * })
     */
    private $renderedMedia;

    /**
     * @ORM\OneToMany(targetEntity="ExperienceTag", mappedBy="experience", orphanRemoval=true, cascade={"persist", "remove"})
     * @var ArrayCollection
     */
    protected $experienceTags;


    public function __construct ()
    {
        $this->experienceTags = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->setCreatedDate(new \DateTime());
        $this->setUpdatedDate(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->setUpdatedDate(new \DateTime());
    }

    public function getExperienceTags()
    {
        return $this->experienceTags;
    }


    public function addExperienceTags(ArrayCollection $experienceTags)
    {
        foreach ($experienceTags as $experienceTags) {
            $this->experienceTags->add($experienceTags);
        }
    }

    public function removeExperienceTags(ArrayCollection $experienceTags)
    {
        foreach ($experienceTags as $experienceTags) {
            $this->experienceTags->removeElement($experienceTags);
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sourceImageId
     *
     * @param integer $sourceImageId
     * @return Experience
     */
    public function setSourceImageId($sourceImageId)
    {
        $this->sourceImageId = $sourceImageId;

        return $this;
    }

    /**
     * Get sourceImageId
     *
     * @return integer 
     */
    public function getSourceImageId()
    {
        return $this->sourceImageId;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Experience
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set x
     *
     * @param string $x
     * @return Experience
     */
    public function setX($x)
    {
        $this->x = $x;

        return $this;
    }

    /**
     * Get x
     *
     * @return string 
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     * @return Experience
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime 
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set updatedDate
     *
     * @param \DateTime $updatedDate
     * @return Experience
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * Get updatedDate
     *
     * @return \DateTime 
     */
    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }

    /**
     * Set y
     *
     * @param string $y
     * @return Experience
     */
    public function setY($y)
    {
        $this->y = $y;

        return $this;
    }

    /**
     * Get y
     *
     * @return string 
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * Set width
     *
     * @param string $width
     * @return Experience
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return string 
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param string $height
     * @return Experience
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return string 
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set user
     *
     * @param \Application\Entity\User $user
     * @return Experience
     */
    public function setUser(\Application\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set source image
     *
     * @param \Application\Entity\SourceImage $user
     * @return Experience
     */
    public function setSourceImage(\Application\Entity\SourceImage $sourceImage = null)
    {
        $this->sourceImage = $sourceImage;

        return $this;
    }

    /**
     * Get source image
     *
     * @return \Application\Entity\SourceImage 
     */
    public function getSourceImage()
    {
        return $this->sourceImage;
    }

    /**
     * Set source image
     *
     * @param \Application\Entity\SourceImage $user
     * @return Experience
     */
    public function setRenderedMedia(\Application\Entity\Media $media = null)
    {
        $this->renderedMedia = $media;

        return $this;
    }

    /**
     * Get source image
     *
     * @return \Application\Entity\SourceImage 
     */
    public function getRenderedImage()
    {
        return $this->renderedMedia;
    }

}
