<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Application\Entity\User;


class UserController extends AbstractActionController
{
    /**
     * Facebook SDK
     * @var \Facebook
     */
    protected $facebookSdk;

    /**
     * Auth service instance.
     * @var Application\Auth\Facebook
     */
    protected $authService;

     /**
     * User service instance.
     * @var Application\Service\UserService
     */
    protected $userService;

    /**
    * Update users option to recieve emails
    * 
    **/ 
    public function updateRecieveEmailAction()
    {
        $recieveEmail = $this->getRequest()->getPost('recieveemail');
        $userFacebookId = $this->getUserFacebookId();
        
        $user = $this->getUserService()->getUserByFacebookId($userFacebookId);
        
        $user->setMarketing($recieveEmail);

        try {
            
            $this->getUserService()->saveUser($user);   
            $status = 'ok';

        } catch (\Exception $e) {
            $this->getServiceLocator()->get('Logger')->err('Error updating recieve email:' . $endpoint . ' ' . $e->getMessage());
            $status = 'error';
        }

        return new JsonModel(array(
            'status' => $status
            ));
        
    }

    /**
    * Validate text against bad words
    * @param POST string $text
    * @return string $status 'ok'| text with span class around bad word
    **/ 
    public function validateTextAction()
    {
        $text = $this->getRequest()->getPost('text');
        // $text = $this->params()->fromQuery('text');
        $finalText = $text;
        $badWords = require(__DIR__ . '/../../../../../data/banned-words.php');

        $foundBadWords = array();

        foreach($badWords as $badWord)
        {
            if(preg_match('/\b'. $badWord . '\b/', $text))
            {
                $text = str_replace($badWord, "", $text);
                array_push($foundBadWords, $badWord);
            }
        }

        if((count($foundBadWords)) > 0)
        {
            foreach($foundBadWords as $badWord)
            {    
                $span = '*' . $badWord . '*'; 
                $finalText = preg_replace('/\b'. $badWord . '\b/', $span, $finalText);
            }

            $status = $finalText;
        }
        else
        {
            $status = 'ok';
        }

        return new JsonModel(array(
            'status' => $status
        ));
         
    }

    /**
     * Get auth service
     * 
     * @return \Application\Auth\Facebook
     */
    protected function getAuthService()
    {
        if (null === $this->authService) {
            $this->authService = $this->getServiceLocator()->get('Application\Auth\Facebook');
        }
        return $this->authService;
    }

    /**
     * Get user service
     * 
     * @return \Application\Service\User
     */
    protected function getUserService()
    {
        if (null === $this->userService) {
            $this->userService = $this->getServiceLocator()->get('Application\Service\User');
        }
        return $this->userService;
    }
    
    /**
     * Get user facebook id
     * 
     * @return $fbid
     */
    protected function getUserFacebookId()
    {
        $facebook = $this->getFacebook();
        $userFBdata = $facebook->api('/me', 'GET');
        $fbId = $userFBdata['id'];
        return $fbId;
    }

    /**
     * Get facebook sdk
     * 
     * @return \Facebook
     */
    protected function getFacebook () {
        if (null === $this->facebookSdk) 
        {
            $this->facebookSdk = $this->getServiceLocator()->get('Facebook');
        }
        return $this->facebookSdk;
    }
}
