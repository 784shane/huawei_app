<?php 

namespace Application\Controller\Plugin;
 
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Session\Container;

class ShortUrl extends AbstractPlugin{

	const API_URL = 'http://is.gd/api.php?longurl=%s';

  	/** 
     * Return shortened URL
     * 
     * @param string $longurl
     * @return string
     */
    public function shorten($longurl = '')
    {
        $adapter = new \Zend\Http\Client\Adapter\Curl();
        $client = new \Zend\Http\Client(sprintf(self::API_URL, $longurl));
        $client->setAdapter($adapter);
        $client->setMethod('get');
        $response = $client->send();       
       
        if ($response->isSuccess()) {
            $url = $response->getBody();
        } else {
            $url = $longurl;
        }

        return $url;
    }
}