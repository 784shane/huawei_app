<?php

namespace Application\Controller\Plugin;


use Application\Entity\Project;
use Application\Entity\ProjectImage;
use MaidoCommon\Model\Media;
use MaidoCommon\Service\AbstractService;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class FetchRemoteImage extends AbstractPlugin
{
    /**
     * Read remote image and return a array
     * 
     * @param  string  $url
     * @return array
     */
    public function __invoke($url) 
    {   
        // Getting an image from a remote URL
        try {

            $config = $this->getController()->getServiceLocator()->get('config');
            $client = new \Zend\Http\Client($url);
            $adapter = new \Zend\Http\Client\Adapter\Curl();
            $client->setAdapter($adapter);
            $response = $client->send();

            if ($response->isOk()){
                
                $file = tmpfile();
                fwrite($file, $response->getContent());
                $meta = stream_get_meta_data($file);

                $result = array(
                    'name' => basename($url),
                    'tmp_name' => $meta['uri'],
                    'type' => image_type_to_mime_type(exif_imagetype($meta['uri'])),
                    'file' => $file,
                    'error' => '',
                );
                return $result;
            }

            throw new \RuntimeException('Bad HTTP error code ' . $response->getStatusCode());
        
        } catch (\Exception $e) {
            $logger = $this->getController()->getServiceLocator()->get('Logger');
            $logger->err('Error getting remote image :' . $e->getMessage());
        }
        
        return false;
    }
}
