<?php 

namespace Application\Controller\Plugin;
 
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Session\Container;
use MaidoCommon\Model\Media;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class MediaViewport extends AbstractPlugin implements ServiceLocatorAwareInterface
{
    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * @var MaidoCommon\Media\Service
     */
    protected $mediaService;

    /**
     * @var MaidoCommon\Controller\Plugin\Media
     */
    protected $mediaUrl;

  	/** 
     * Return viewport URL
     * 
     * @param Media|int $media
     * @return string
     */
    public function __invoke($media, $name)
    {
        $mediaId = $media instanceof Media ? $media->id : (int) $media;

        $viewport = $this->getMediaService()->getViewport($mediaId, $name);
        
        if ($viewport !== null) {
            $mediaUrl = $this->getMediaPlugin();
            return $mediaUrl->url($viewport);
        }

        return false;
    }

    protected function getMediaService()
    {
        if (null === $this->mediaService) {
            $this->mediaService = $this->getServiceLocator()->getServiceLocator()->get('MaidoCommon\Service\Media');
        }

        return $this->mediaService;
    }


    protected function getMediaPlugin()
    {
        if (null === $this->mediaUrl) {
            $this->mediaUrl = $this->getServiceLocator()->get('media');
        }

        return $this->mediaUrl;
    }

    public function setServiceLocator(ServiceLocatorInterface $sm)
    {
        $this->serviceLocator = $sm;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}