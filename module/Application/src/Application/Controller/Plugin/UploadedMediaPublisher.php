<?php

namespace Application\Controller\Plugin;


use Application\Entity\Project;
use Application\Entity\ProjectImage;
use MaidoCommon\Model\Media;
use MaidoCommon\Service\AbstractService;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class UploadedMediaPublisher extends AbstractPlugin
{
    /**
     * Upload an image, publish viewports and return a Doctrine entity
     * @param  array  $uploadedFile
     * @param  string $imageType
     * @return Application\Entity\Media
     */
    public function __invoke(array $uploadedFile, $viewportPath) 
    {   
        $serviceLocator = $this->getController()->getServiceLocator();
        $config = $serviceLocator->get('config');

        if (!isset($config['viewports'][$viewportPath])) {
            throw new \InvalidArgumentException(
                sprintf('Viewport configuration not found: %s', $imageType)
            );
        }

        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $mediaService = $serviceLocator->get('MaidoCommon\Service\Media');
        $filename = sprintf('%s%s', uniqid(), $mediaService->getExtensionByMimetype($uploadedFile['type']));
        
        $media = new Media(array(
            'is_local' => 1,
            'type' => 'Image',
            'url' => $viewportPath . '/' . $filename,
        ));

        
        $media->id = $mediaService->storeMedia($media, $uploadedFile['tmp_name']);
        $mediaService->publishMedia($media, $viewportPath);
        $mediaEntity = $entityManager->find('Application\Entity\Media', $media->id);

        return $mediaEntity;
    }
}
