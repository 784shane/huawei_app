<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


class InstagramController extends AbstractActionController
{
    public function handleLoginAction()
    {

        $code = $this->params()->fromQuery('code');
        $config = $this->getServiceLocator()->get('config');
        $appSession = new \Zend\Session\Container('application');
        $error = array();
        $vars = array();

        $extraVars = $this->doMobileTest($appSession);

        if ($code === null) {
            
            // Client-side authentication process, or server side with existing token.
            $error = $this->params()->fromQuery('error');
            
            if ($error !== null) {
                $error = array(
                    'errorReason' => $this->params()->fromQuery('error_reason'),
                    'errorDescription' => $this->params()->fromQuery('error_description'),
                );
            } 

        } else {

            // Server-side authentication process, request a token with a code.
            $result = $this->getAccessToken($code);

            if ($result) {
                
                if (is_array($result)) {
                    // It's an error
                    $error = array(
                        'errorReason' => $result['type'],
                        'errorDescription' => $result['message'],
                    );

                } else {
                    $vars['accessToken'] = $appSession->instagramAccessToken = $result;

                }

            } else {
                // It's an error
                $error = array(
                    'errorReason' => 'error',
                    'errorDescription' => 'Could not authenticate to Instagram',
                );

            }
        }

        

        $vars = array_merge($vars, $extraVars);
        
        $viewModel = new ViewModel($vars);
        $viewModel->setTerminal(true);
        
        return $viewModel;
        
    }

    private function doMobileTest($appSession)
    {
        $detect = $this->getServiceLocator()->get('MobileDetect');
        if ($detect->isMobile()) {

            $_SESSION['instagram_logged_in'] = true;
            $vars = array();
            $vars['isMob'] = true;
            $vars['instagram_logged_in'] = true;

            return $vars;

        }else{return array();}

    }

    protected function getAccessToken($code = '')
    {
        $config = $this->getServiceLocator()->get('config');
        $igConfig = $config['instagram'];

        $client = new \Zend\Http\Client($igConfig['access_token_url']);

        $client->setAdapter(new \Zend\Http\Client\Adapter\Curl())
            ->setMethod('post')
            ->setParameterPost(array(
                'client_id' => $igConfig['client_id'],
                'client_secret' => $igConfig['client_secret'],
                'redirect_uri' => $igConfig['redirect_uri'],
                'grant_type' => 'authorization_code',
                'code' => $code,
            ));

        $response = $client->send();

        $igResponse = json_decode($response->getBody());

        // Check for errors
        if (isset($igResponse->code) && $igResponse->code == '400') {
            return array(
                'type' => $igResponse->error_type,
                'message' => $igResponse->error_message,
            );
        }

        // Check for access_token
        if (isset($igResponse->access_token)) {
           return $igResponse->access_token;
        }

        return false;
    }
    
}
