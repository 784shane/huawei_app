<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
	protected $experienceService;


    public function indexAction()
    {
        $config = $this->getServiceLocator()->get('config');

        $mobileDetect = $this->mobileDetect();
        $ismob = $mobileDetect->isMobile();
        $istab = $mobileDetect->isTablet();
        $isiOS = $mobileDetect->isiOS();
        
    	$experienceId = $this->params()->fromRoute('id');
        $ogTags = array();

        if ($experienceId) {

            if($this->isUserAgentBot())
            {
                // Render OG tags only if experience exists and the user agent is a bot
                $experienceService = $this->getExperienceService();
                $experience = $experienceService->getExperienceById($experienceId);
                
                if ($experience) {

                    $fbConfig = $config['facebook'];

                    $url = $this->url()->fromRoute(
                        'experience-open-graph', 
                        array('id' => $experience->getId()), 
                        array('force_canonical' => true)
                    );

                    $openGraphImage = $this->viewportUrl($experience->getRenderedImage()->getId(), 'experience/open-graph');

                    if (!$openGraphImage) {
                        $openGraphImage = $this->viewportUrl($experience->getRenderedImage()->getId(), 'experience/fullsize');
                    }

                    $ogTags = array( 
                        'appId' => $fbConfig['app']['id'],
                        'type' => sprintf('%s:%s', $fbConfig['app']['namespace'], $fbConfig['open_graph']['type']),
                        'title' => $fbConfig['app']['title'],
                        'card_title' => $config['twitter']['card']['title'],
                        'url' => $url,
                        'text' => $fbConfig['open_graph']['description'],
                        'image' => $openGraphImage,
                    );    
                } 
            }
            else
            {
                return $this->redirect()->toRoute('redirect');
            }

        }

        $this->deal_with_mobile_instagram();


        $instagram_logged_in = 0;
        if(isset($_SESSION['instagram_logged_in'])){
            $instagram_logged_in = 1;
            //do not unset this here.

        }

        return new ViewModel(array(
            'ogTags' => $ogTags,
            'ismob' => $ismob,
            'istab' => $istab,
            'instagram_logged_in' => $instagram_logged_in
        ));

    }

    private function deal_with_mobile_instagram(){

        $instagram_access_token = null;
        $instagram_access_token = 
        $this->getRequest()->getPost('instagram_access_token');

        if($instagram_access_token!=null&&$instagram_access_token!=""){
            $_SESSION['instagram_access_token'] = $instagram_access_token;
            $_SESSION['instagram_logged_in'] = true;
        }
        
    }
    /**
     * Redirect action.
     * 
     * @return \Zend\Http\Response
     * @throws \Exception
     */
    public function redirectAction()
    {
        /**
         * @var \Mobile_Detect $detect
         */
        $detect = $this->getServiceLocator()->get('MobileDetect');

        if ($detect->isMobile() || $this->isUserAgentBot()) {
            return $this->redirect()->toRoute('home');
        }

        $config = $this->getServiceLocator()->get('config');

        $url = sprintf($config['facebook']['page']['url'], $config['facebook']['app']['id']);

        return $this->redirect()->toUrl($url);
    }

    /**
     * Redirect action.
     * 
     * @return \Zend\Http\Response
     * @throws \Exception
     */
    public function canvasRedirectAction()
    {
        /**
         * @var \Mobile_Detect $detect
         */
        $detect = $this->getServiceLocator()->get('MobileDetect');

        $config = $this->getServiceLocator()->get('config');

        $url = sprintf($config['facebook']['page']['url'], $config['facebook']['app']['id']);

        return new ViewModel(array(
            'url' => $url,
        ));
    }

    protected function isUserAgentBot()
    {   

        $bots = array('facebookexternalhit', 'twitterbot', 'googlebot');
        $userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? strtolower($_SERVER['HTTP_USER_AGENT']) : '';
        
        foreach ($bots as $bot) {
            if (strpos($userAgent, strtolower($bot)) !== false){
                return true;
            }
        }

        return false;
    }

    /**
     * Get source service
     * 
     * @return \Application\Service\SourceImage
     */
    protected function getExperienceService()
    {
        if (null === $this->experienceService) {
            $this->experienceService = $this->getServiceLocator()->get('Application\Service\Experience');
        }
        return $this->experienceService;
    }
}
