<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Entity\Experience;
use Application\Entity\User;
use Application\Controller\Request;

class AppController extends AbstractActionController
{


    public function appAction($id=2)
    {

        $mobileDetect = $this->mobileDetect();
        $ismob = $mobileDetect->isMobile();
        $istab = $mobileDetect->isTablet();
        $isiOS = $mobileDetect->isiOS();


        return new ViewModel();
    }

    public function testAction () 
    {
        $renderer = $this->getServiceLocator()->get('Application\Renderer\Experience');
        $user = $this->getServiceLocator()->get('Application\Auth\Facebook')->authenticate();
        
        $experience = new Experience();

        $experience->setText('Testing');
        $experience->setCreatedDate(date('Y-m-d H:i:s'));
        $experience->setUpdatedDate(date('Y-m-d H:i:s'));
        $experience->setX('20');
        $experience->setX('30');
        $experience->setWidth('50');
        $experience->setHeight('50');
        $experience->setUser($user);
        $experience->setExperienceImage();
        

        $media = $renderer->render($experience);

        var_dump($media);
        exit;
    }


    public function realdimensionsAction($id=1)
    {
        $photo_url = 
        $this->getRequest()->getPost('photo_url');

        list($width, $height, $type, $attr) = 
        getimagesize($photo_url);

        die(json_encode(
            array(
                'width' => $width,
                'height'=> $height
            )
        ));

    }


    public function generatephotoAction()
    {

        ini_set('memory_limit', '-1');

        
        $source_image_provider = 
        $this->getRequest()->getPost('source_image_provider');

        $photo_url = 
        $this->getRequest()->getPost('image_url');

        $crop_x = 
        $this->getRequest()->getPost('x');

        $crop_y = 
        $this->getRequest()->getPost('y');

        $crop_width = 
        $this->getRequest()->getPost('width');

        $crop_height = 
        $this->getRequest()->getPost('height');

        if(isset($_REQUEST['get'])):

            $testurl = "http://huawei.maido/app/generatephoto?get=1&image_url=https://scontent-b.xx.fbcdn.net/hphotos-frc3/t1.0-9/s720x720/420136_10151266195345360_1322045133_n.jpg&x=272.8&y=138.54&width=475.64&height=341.7";

            $photo_url = $_REQUEST['image_url'];

            $crop_x = $_REQUEST['x'];

            $crop_y = $_REQUEST['y'];

            $crop_width = $_REQUEST['width'];

            $crop_height = $_REQUEST['height'];
            
            $a = array(
                'crop_x'=>$crop_x,
                'crop_y'=>$crop_y,
                'crop_width'=>$crop_width,
                'crop_height'=>$crop_height
                );

        endif;



        $httpHost = 'huawei.maido';
        $httpHost = $_SERVER['HTTP_HOST'];
        //$source_image_provider = "local";
        //$photo_url = "http://huawei.maido/assets/graphics/trash/LARGE_elevation.jpg";

        if($source_image_provider=="stock")
        {
            $host_pos = stripos($photo_url,$httpHost);
            $local_url = substr($photo_url,$host_pos+strlen($httpHost));
            $photo_url = "public".$local_url;
        }


        //$src = imagecreatefromjpeg($photo_url);
        $src = imagecreatefromjpeg($photo_url);

        //$dest = imagecreatetruecolor($crop_width, $crop_height);
        $dest = imagecreatetruecolor(698, 698);

        // Copy
        //imagecopy($dest, $src, 0, 0, $crop_x, $crop_y, $crop_width, $crop_height);
        imagecopyresized($dest, $src, 0, 0, $crop_x, $crop_y, 698, 698, $crop_width, $crop_height);

        // Output and free from memory
        if(isset($_REQUEST['get'])){
            header('Content-Type: image/jpg');
        }
        //header('Content-Type: image/jpg');
        //imagegif($dest);

        $filename = 'generated'.rand().'.jpg';
        $file = 'public/assets/graphics/trash/'.$filename;
        //imagejpeg($dest, $file);
        if(isset($_REQUEST['get'])){imagejpeg($dest);}
        else{imagejpeg($dest, $file);}

        imagedestroy($dest);
        imagedestroy($src);

        if(!isset($_REQUEST['get'])):
        echo json_encode(
            array(
                'src' => 'http://'.$_SERVER['HTTP_HOST'].
                '/assets/graphics/trash/'.$filename
            )
        );
        endif;


        die();
    }


    public function imagereturnAction()
    {

        $host = $_SERVER['HTTP_HOST'];

        $pic_array = array();

        for($i = 0; $i<12; $i++)
        {
            $pa[] = array(
                //file:///C:/xampp/htdocs/maido/huawei/public/assets/graphics/trash/test1.jpg
                        'id' => $i,
                        'url' => 'http://'.$host.'/assets/graphics/trash/test1.jpg',
                        'thumb' => 'http://'.$host.'/assets/graphics/trash/test1.jpg',
                    );

            $i++;

            $pa[] = array(
                //file:///C:/xampp/htdocs/maido/huawei/public/assets/graphics/trash/test1.jpg
                        'id' => $i,
                        'url' => 'http://'.$host.'/assets/graphics/trash/test2.jpg',
                        'thumb' => 'http://'.$host.'/assets/graphics/trash/test2.jpg',
                    );

            
        }

        $pic_array = array_merge($pic_array, $pa);

        echo json_encode($pic_array);

        die();
        //die('imagereturn');
    }


    public function instagramAction()
    {

        die('got it');
        //die('imagereturn');
    }


    public function fbloginAction()
    {
            
            $code = $this->params()->fromQuery('code');

            $id = '663612927026428';
            $secret = '28c138ba04d33ee8c30737de24ee2f0b';
            $redirect_uri = 'http://huawei.maido/app/fblogin';

            $url = 'https://www.facebook.com/dialog/oauth?client_id=663612927026428&redirect_uri=http://huawei.maido/app/fblogin';


            $theUrl = 'https://graph.facebook.com/oauth/access_token?client_id=663612927026428&redirect_uri=http://huawei.maido/app/fblogin&client_secret=28c138ba04d33ee8c30737de24ee2f0b&code='.$code;


            /*$curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $theUrl);
            curl_setopt($curl, CURLOPT_VERBOSE, 1);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            /*if($theArray){
            curl_setopt($curl, CURLOPT_POSTFIELDS, $theArray);
            }*//*
            //curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

            $theCurlRes = curl_exec($curl);
            // Look at the returned header
            $resultArray = curl_getinfo($curl);*/




            /*$ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $theUrl);
            // Set so curl_exec returns the result instead of outputting it.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // Get the response and close the channel.
            $response = curl_exec($ch);
            curl_close($ch);*/


            /*$client = new Client();
            $client->setUri($theUrl);
            $client->setOptions(array(
                'maxredirects' => 0,
                'timeout'      => 30
            ));
            $response = $client->send();



            $request = new Request();
            $request->setMethod(Request::METHOD_POST);
            $request->setUri('https://graph.facebook.com/oauth/access_token');
            $request->getHeaders()->addHeaders(array(
                'client_secret' => 'header-field-value1',
                'client_id' => 'header-field-value2',
                'redirect_uri' => 'header-field-value2',
                'code' => 'header-field-value2',
            ));*/

$url = 'https://www.facebook.com/dialog/oauth?client_id=663612927026428&redirect_uri=http://huawei.maido/app/fblogin';

            $client = new \Zend\Http\Client('https://graph.facebook.com/oauth/access_token');
            $client->setAdapter(new \Zend\Http\Client\Adapter\Curl())
            ->setMethod('post')
            ->setParameterPost(array(
                'client_id' => $id,
                'client_secret' => $secret,
                'redirect_uri' => $redirect_uri,
                'code' => $code,
            ));

            $response = $client->send();

            ///$request->getPost()->set('foo', 'bar');



            print_r($response);




            
            //curl_close($curl);
            //print($theCurlRes);
        


        exit;
    }


}
