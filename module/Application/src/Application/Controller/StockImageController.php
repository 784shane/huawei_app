<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class StockImageController extends AbstractRestfulController
{

    protected $sourceImageService;


    public function getList()
    {
        $sourceImageService = $this->getSourceImageService();

        $stockImages = $sourceImageService->getStockImages();

        $images = array();

        foreach ($stockImages as $stockImage) {
            
            $mediaId = $stockImage->getMedia()->getId();

            $images[] = array(
                'id' => $stockImage->getId(),
                'url' => $this->viewportUrl($mediaId, 'source_image/fullsize'),
                'thumb' => $this->viewportUrl($mediaId, 'source_image/thumbnail'),
                'provider' => 'stock'
            );
        }

        return new JsonModel($images);
    }

    /**
     * Get source service
     * 
     * @return \Application\Service\SourceImage
     */
    protected function getSourceImageService()
    {
        if (null === $this->sourceImageService) {
            $this->sourceImageService = $this->getServiceLocator()->get('Application\Service\SourceImage');
        }
        return $this->sourceImageService;
    }

}
