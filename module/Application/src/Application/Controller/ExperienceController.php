<?php


namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Application\Entity\Experience;
use Application\Entity\ExperienceTag;
use Application\Entity\User;
use Application\Entity\SourceImage;

class ExperienceController extends AbstractRestfulController
{
    protected $authService;

    protected $mediaService;

    protected $sourceImageService;

    protected $experienceService;

    protected $experienceRenderer;

    /**
     * @param $data [url string, text string, provider string, tags array]
     *
     */
    public function create($data)
    {
        // var_dump($data);exit;
        $fbAccessToken = $data['acc'];
        $user = $this->getAuthService()->authenticate($fbAccessToken);

        if (!$user instanceof User) {
            return new JsonModel(array(
                'errors' =>  $user, 
            ));
        }

        $experienceForm = $this->getServiceLocator()->get('Application\Form\Experience');
        
        $experience = new Experience();
        
        $experienceForm
            ->setData($data)
            ->bind($experience);

        if ($experienceForm->isValid()) {
            
            try {

                $sourceImage = $this->getSourceImage($data);

                if ($sourceImage instanceof SourceImage) {

                    $experience->setSourceImage($sourceImage);

                    $renderedMedia = $this->getExperienceRenderer()->render($experience);

                    $experience->setRenderedMedia($renderedMedia);

                    $experience->setUser($user);
                    
                    $this->getExperienceService()->saveExperience($experience);
                    
                    $taggedUser = 0;
                    
                    if (isset($data['tags'])) {
                        $experienceTags = $this->getFacebookExperienceTags($user, $experience, $data['tags']);    
                        
                        foreach ($experienceTags as $experienceTag) {
                            $experience->getExperienceTags()->add($experienceTag);
                            $taggedUser = $experienceTag->getFacebookId();
                        }

                        $this->getExperienceService()->saveExperience($experience);
                    }
                    
                    $url = $this->url()->fromRoute('experience-open-graph', array('id' => $experience->getId()), array('force_canonical' => true));

                    $this->publishOpenGraphStory($user, $experience, $taggedUser, $fbAccessToken);


                    return new JsonModel(array(
                        'id' => $experience->getId(),
                        'url' => $url,
                        'shortUrl' => $this->shortUrl($url),
                        'src' => $this->viewportUrl($renderedMedia->getId(), 'experience/fullsize'),
                    ));

                } else {
                    $errors = $sourceImage;
                }

            } catch (\Exception $e) {

                $errors = array($e->getMessage());
                $logger = $this->getServiceLocator()->get('Logger');
                $logger->err('UserID: ' . $user->getId(). ' | Error creating experience :' . $e->getMessage());

            }
        } else {
            $errors = array();

            foreach ($experienceForm->getMessages() as $field => $messages) {
                foreach ($messages as $error) {
                    $errors[] = $field . ': ' . $error;
                }
            }

        }

        $this->getResponse()->setStatusCode(400);

        return new JsonModel(array(
            'errors' =>  $errors, 
        ));
    }

    protected function getFacebookExperienceTags(User $user, Experience $experience, $tags)
    {

        $sdk = $this->getServiceLocator()->get('Facebook');
        $experienceService = $this->getExperienceService();

        $tags = explode(',', $tags);
        $experienceTags = array();

        foreach ($tags as $facebookId) {
            try {
                $id = (int) trim($facebookId);

                if ($id) {
                    $endpoint = sprintf('/%s', $id);
                    $result = $sdk->api($endpoint);

                    $data = (object) $result;

                    $experienceTag = new ExperienceTag();
                    $experienceTag->setFacebookId($data->id);
                    $experienceTag->setFirstName($data->first_name);
                    $experienceTag->setLastName($data->last_name);
                    $experienceTag->setGender($data->gender);
                    $experienceTag->setExperience($experience);

                    $experienceTags[] = $experienceTag;
                }
                
                
            } catch (\Exception $e) {
                $this->getServiceLocator()->get('Logger')->err('UserID: ' . $user->getId(). ' | Error getting Facebook Experience Tags:' . $endpoint . ' ' . $e->getMessage());
            }
        }

        return $experienceTags;

    }

    protected function publishOpenGraphStory(User $user, Experience $experience, $taggedUser, $fbAccessToken)
    {
        $sdk = $this->getServiceLocator()->get('Facebook');
       
        try {
            if ($sdk->getUser($fbAccessToken)) {

                $params = array(
                    //'huawei_competition' => 'https://huawei.maido.com/experience/' . $experience->getId(),
                    'huawei_competition' => $this->url()->fromRoute(
                        'experience-open-graph', 
                        array('id' => $experience->getId()), 
                        array('force_canonical' => true)
                    ),
                    'access_token' => $sdk->getAccessToken(),
                    'fb:explicitly_shared' => 'true'
                );

                if ($taggedUser) {
                    $params['tags'] = $taggedUser;
                } 

                $path = '/me/huawei_experience:enter';
                
                $sdk->api($path, 'post', $params);
                
            }
        } catch (\Exception $e) {
            $this->getServiceLocator()->get('Logger')->err('UserID: ' . $user->getId(). ' | Error publishing open graph story: ' . $e->getMessage());

        }
    }

    /**
     * @return Application\Entity\SourceImage|array
     */
    protected function getSourceImage (array $data) {

        // See if provder is stock or facebook or instagram
        $sourceImageService = $this->getSourceImageService();

        if ($data['provider'] === 'stock') {
            $sourceImage = $sourceImageService->getSourceImageById($data['image_id']);

            if (!$sourceImage) {
                return array('Source image not found');
            }

        } else if ($data['provider'] === 'facebook' || $data['provider'] === 'instagram') {

            $sourceImage = $this->getRemoteSourceImage($data);
        }

        return $sourceImage;
    }

    /**
     * @return SourceImage|array
     */
    protected function getRemoteSourceImage (array $data) {

        $imageForm = $this->getServiceLocator()->get('Application\Form\RemoteSourceImage');
        
        $remoteImage = $this->fetchRemoteImage($data['image_url']);
        
        if ($remoteImage) {

            $imageForm->setData(array('image' => $remoteImage));
        
            if ($imageForm->isValid()) {
            
                $media = $this->publishMedia($remoteImage, 'source_image');

                $sourceImage = new SourceImage();
                $sourceImage->setMedia($media);
                $sourceImage->setProvider($data['provider']);
                                
                return $this->getSourceImageService()->saveSourceImage($sourceImage);

            } else {

                $errors = array();
                foreach ($imageForm->getMessages() as $field => $messages) {
                    foreach ($messages as $error) {
                        $errors[] = $field . ': ' . $error;
                    }
                }
            }
        } else {
            $errors = array('Impossible to get remote image'); 

        }

        return $errors;
        
    }

    /**
     * Get auth service
     * 
     * @return \Application\Auth\Facebook
     */
    protected function getAuthService()
    {
        if (null === $this->authService) {
            $this->authService = $this->getServiceLocator()->get('Application\Auth\Facebook');
        }
        return $this->authService;
    }

    /**
     * Get media service
     * 
     * @return \MaidoCommon\Service\Media
     */
    protected function getMediaService()
    {
        if (null === $this->mediaService) {
            $this->mediaService = $this->getServiceLocator()->get('MaidoCommon\Service\Media');
        }
        return $this->mediaService;
    }

    /**
     * Get source service
     * 
     * @return \Application\Service\SourceImage
     */
    protected function getSourceImageService()
    {
        if (null === $this->sourceImageService) {
            $this->sourceImageService = $this->getServiceLocator()->get('Application\Service\SourceImage');
        }
        return $this->sourceImageService;
    }

    /**
     * Get experience service
     * 
     * @return \Application\Service\Experience
     */
    protected function getExperienceService()
    {
        if (null === $this->experienceService) {
            $this->experienceService = $this->getServiceLocator()->get('Application\Service\Experience');
        }
        return $this->experienceService;
    }

    /**
     * Get experience renderer
     * 
     * @return \Application\Service\Experience
     */
    protected function getExperienceRenderer()
    {
        if (null === $this->experienceRenderer) {
            $this->experienceRenderer = $this->getServiceLocator()->get('Application\Renderer\Experience');
        }
        return $this->experienceRenderer;
    }   

}
