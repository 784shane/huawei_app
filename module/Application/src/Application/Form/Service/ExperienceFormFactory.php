<?php

namespace Application\Form\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class ExperienceFormFactory implements FactoryInterface
{

    /**
     * Create answer form
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return \Zend\Form\Form
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $form = $this->getForm();
        $form->setInputFilter($this->getInputFilter($serviceLocator));

        $form->setHydrator(
            new DoctrineHydrator(
                $serviceLocator->get('Doctrine\ORM\EntityManager'), 
                'Experience'
            )
        );
        
        return $form;
    }

    /**
     * Create new form instance.
     * 
     * @return \Zend\Form\Form
     */
    protected function getForm()
    {
        // create form object with name
        $form = new Form('experience');
        
        // [url string, text string, provider string, tags array]
        $inputs = array(
            'provider' => array(
                'name' => 'provider',
                'type' => 'Text',
                'attributes' => array(
                    'required' => true
                ),
            ),
            'image_url' => array(
                'name' => 'image_url',
                'type' => 'Text',
                'attributes' => array(
                    'required' => false // Is not required if im
                ),
            ),
            'image_id' => array(
                'name' => 'image_url',
                'type' => 'Text',
                'attributes' => array(
                    'required' => false // Is not required if im
                ),
            ),
            'text' => array(
                'name' => 'text',
                'type' => 'Text',
                'attributes' => array(
                    'required' => true
                ),
            ),
            'tags' => array(
                'name' => 'tags',
                'type' => 'Text',
            ),
            'user_id' => array(
                'name' => 'user_id',
                'type' => 'Text',
                'attributes' => array(
                    'required' => true
                ),
            ),
            'x' => array(
                'name' => 'x',
                'type' => 'Text',
                'attributes' => array(
                    'required' => true
                ),
            ),
            'y' => array(
                'name' => 'y',
                'type' => 'Text',
                'attributes' => array(
                    'required' => true
                ),
            ),
            'width' => array(
                'name' => 'width',
                'type' => 'Text',
                'attributes' => array(
                    'required' => true
                ),
            ),
            'height' => array(
                'name' => 'height',
                'type' => 'Text',
                'attributes' => array(
                    'required' => true
                ),
            ),
    
        );

        // add elements to the form
        foreach ($inputs as $input) {
            $form->add($input);
        }

        return $form;
    }

    /**
     * Get input filter.
     * 
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     * @return \Zend\InputFilter\InputFilter
     */
    protected function getInputFilter(ServiceLocatorInterface $serviceLocator)
    {
        // create input filter object
        $inputFilter = new InputFilter();

        $config = $serviceLocator->get('config');
        
        $badWords = require(__DIR__ . '/../../../../../../data/banned-words.php');

        // compose array of input filters
        $inputFilters = array(
            'provider' => array(
                'name' => 'provider',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'Zend\Validator\InArray',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'haystack' => array(
                                'facebook', 'instagram', 'stock',
                            ),
                        ),
                    ),
                ),
            ),
            'image_url' => array(
                'name' => 'image_url',
                'required' => false,
                
                // @todo, validate URL 

                /*'validators' => array(
                    array(
                        'name' => 'Zend\I18n\Validator\Int',
                    ),
                ),*/
            ),
            'image_id' => array(
                'name' => 'image_id',
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Zend\I18n\Validator\Int',
                    ),
                ),
            ),
            'text' => array(
                'name' => 'text',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => $config['experience']['minlength'],
                            'max' => $config['experience']['maxlength'],
                        ),
                    ),
                     array(
                        'name' => 'Application\Validator\BadLanguageValidator',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'bannedWords' => $badWords,
                        ),
                    ),
                ),
            ),
            'x' => array(
                'name' => 'x',
                'required' => true,
                // 'validators' => array(
                //     array(
                //         'name' => 'Zend\I18n\Validator\Float',
                //     ),
                // ),
            ),
            'y' => array(
                'name' => 'y',
                'required' => true,
                // 'validators' => array(
                //     array(
                //         'name' => 'Zend\I18n\Validator\Float',
                //     ),
                // ),
            ),
            'width' => array(
                'name' => 'width',
                'required' => true,
                // 'validators' => array(
                //     array(
                //         'name' => 'Zend\I18n\Validator\Float',
                //     ),
                // ),
            ),
            'height' => array(
                'name' => 'height',
                'required' => true,
                // 'validators' => array(
                //     array(
                //         'name' => 'Zend\I18n\Validator\Float',
                //     ),
                // ),
            ),
        );
        
        // add filters to input filter
        foreach ($inputFilters as $input) {
            $inputFilter->add($input);
        }

        return $inputFilter;
    }
}