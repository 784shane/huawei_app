<?php

namespace Application\Form\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;


class RemoteSourceImageFormFactory implements FactoryInterface
{

    /**
     * Create answer form
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return \Zend\Form\Form
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $form = $this->getForm();
        $form->setInputFilter($this->getInputFilter($serviceLocator));

        return $form;
    }

    /**
     * Create new form instance.
     * 
     * @return \Zend\Form\Form
     */
    protected function getForm()
    {
        // create form object with name
        $form = new Form('img');
        
        // compose array of elements
        $inputs = array(
            'image' => array(
                'name' => 'image',
                'type' => 'Text',
                'attributes' => array(
                    'required' => true
                ),
            ),
        );

        // add elements to the form
        foreach ($inputs as $input) {
            $form->add($input);
        }

        return $form;
    }

    /**
     * Get input filter.
     * 
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     * @return \Zend\InputFilter\InputFilter
     */
    protected function getInputFilter(ServiceLocatorInterface $serviceLocator)
    {
        // create input filter object
        $inputFilter = new InputFilter();

        $config = $serviceLocator->get('config');
        $sourceImageConfig = $config['experience']['image'];
        $maxResample = $sourceImageConfig['max_resample'];

        // instantiate entities to extract configuration

        // compose array of input filters
        $inputFilters = array(
            'image' => array(
                'name' => 'image',
                'validators' => array(
                    array(
                        'name' => 'Zend\Validator\File\IsImage',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'mimeType' => array('image/jpeg', 'image/jpg', 'image/gif', 'image/png'),
                        ),
                    ),
                    array(
                        'name' => 'Zend\Validator\File\MimeType',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'mimeType' => array('image/jpeg', 'image/jpg', 'image/gif', 'image/png'),
                        ),
                    ),
                    array(
                        'name' => 'Zend\Validator\File\ImageSize',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'minWidth' => $sourceImageConfig['final_width'] / $maxResample,
                            'minHeight' => $sourceImageConfig['final_height'] / $maxResample,
                            'messages' => array(
                                \Zend\Validator\File\ImageSize::WIDTH_TOO_BIG => "Maximum width is %maxwidth% pixels (%width% detected)",
                                \Zend\Validator\File\ImageSize::WIDTH_TOO_SMALL => "Minimum width is %minwidth% pixels (%width% detected)",
                                \Zend\Validator\File\ImageSize::HEIGHT_TOO_BIG => "Maximum height is %maxheight%' pixels (%height% detected)",
                                \Zend\Validator\File\ImageSize::HEIGHT_TOO_SMALL => "Minimum height is %minheight% pixels (%height% detected)",
                                \Zend\Validator\File\ImageSize::NOT_DETECTED => "The size of image could not be detected",
                                \Zend\Validator\File\ImageSize::NOT_READABLE => "File is not readable or does not exist",
                            )
                        ),
                    ),
                ),
            ),
        );
        
        // add filters to input filter
        foreach ($inputFilters as $input) {
            $inputFilter->add($input);
        }

        return $inputFilter;
    }
}