<?php

namespace Application\Auth;

use Application\Entity\User;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use MaidoCommon\Service\AbstractService;

class Facebook extends AbstractService
{

    protected $facebookAccessToken;

    /**
     * Facebook SDK
     * @var \Facebook
     */
    protected $facebookSdk;

    /**
     * User service instance.
     * @var Application\Service\UserService
     */
    protected $userService;

    /**
     * Try to authenticate or signup a user
     * @param  array  $params [description]
     * @return Application\Model\User|array
     */
    public function authenticate($fbAccessToken) 
    {
        $this->facebookAccessToken = $fbAccessToken;

        $result = $this->getUser();

        if ($result instanceof User) {
            // Save in to session, only for logger
            $appSession = new \Zend\Session\Container('application');
            $appSession->userId = $result->getId();
        }

        return $result;
    }


    protected function getUserService()
    {
        if (null === $this->userService) {
            $this->userService = $this->getServiceLocator()->get('Application\Service\User');
        }
        return $this->userService;
    }

    /**
     * Checks if the authenticated Facebook user exists as a User, otherwise tries to create a new one
     * 
     * @return Application\Model\User|array
     */
    protected function getUser(array $data = array())
    {   
        $userService = $this->getUserService();
        $facebook = $this->getFacebook();
        $signedRequest = $facebook->getSignedRequest();
        
        // $accessToken = $facebook->getAccessToken();
        $accessToken = $facebook->setAccessToken($this->facebookAccessToken);

        $facebookUserId = $this->getFacebookUserId();
        // check if user is already registered
        $user = $userService->getUserByFacebookId($facebookUserId);
       
        if (!$user) {

            // create a new user and sign them up  
            $facebookUserData = $this->getFacebookUserData();

            if ($facebookUserData !== false) {

                try {
                    
                    $user = $this->createUser($facebookUserData);

                } catch (\Exception $e) {
                    $this->getServiceLocator()->get('Logger')->err('Impossible save user details :' . $e->getMessage());
                    return array('Impossible save user details');
                }

            } else {
                return array('Impossible to get user from Facebook');
            }
        } 

        return $user;        
    }

    /**
     * Create a user
     * 
     * @param array $data [first_name, last_name, social_id]
     * @return MaidoUser\Model\User
     */ 
    protected function createUser(array $data) {
        
        $user = new User();
        $user->setFacebookId($this->getFacebookUserId($this->facebookAccessToken));
        $user->setFirstName($data['first_name']);
        $user->setLastName($data['last_name']);
        
        if ($data['birthday_date']) {
            $user->setBirthday(new \DateTime($data['birthday_date']));    
        }
        

        $user->setEmail($data['email']);
        $user->setGender($data['sex']);

        if (isset($data['current_location'])) {
            
            $location = implode(', ', $data['current_location']);
            
        } else {

            $location = '';

        }

        $user->setLocation($location);
    
        $this->getUserService()->saveUser($user);

        return $user;
    }

    protected function getFacebookUserId()
    {

        try {
            $facebook = $this->getFacebook();
            
            $me = $facebook->api('/me?access_token=' . $this->facebookAccessToken);
            
            if ($me) {
                return $facebook->getUser();
            }
        } catch (\Exception $e) {
            $this->getServiceLocator()->get('Logger')
                ->err('Failed getting facebook user id: ' . $e->getMessage());    
        }

        return false;
    }
    /**
     * Get facebook user data through a FQL query
     * 
     * @param string $facebookUserId
     * @param string $accessToken
     * @return array|false
     * 
     */ 
    protected function getFacebookUserData()
    {
        $facebook = $this->getFacebook();

        $fields = array();
        $fql = 'SELECT uid, name, email, first_name, last_name, sex, birthday_date,
                current_location.city, current_location.country  
                FROM user WHERE uid=me()';

        try {
            $data = $facebook->api(array(
                'method' => 'fql.query',
                'query' => $fql,
            ));

            $data = (array) $data;

            if (empty($data['email'])) {
                $this->getServiceLocator()->get('Logger')
                    ->err('Facebook user data: '. json_encode($data));    
            }

            return $data[0];

        } catch (\Exception $e) {
            
            $this->getServiceLocator()->get('Logger')
                ->err('Failed getting facebook user data: '. $e->getMessage());
        }

        return false;
        
    }

    protected function getUserFromSession()
    {
        $appSession = new \Zend\Session\Container('application');

        if (isset($appSession->userId)) {
            $sessionUser = $this->getUserService()->getUserById($appSession->userId);
            
            if ($sessionUser) {
                return $sessionUser;
            }
        }

        return false;
    }

    protected function getUserFromSignedRequest()
    {
        $userService = $this->getUserService();
        $facebook = $this->getFacebook();
        $signedRequest = $facebook->getSignedRequest();
        $facebookUserId = isset($signedRequest['user_id']) ? $signedRequest['user_id'] : $facebook->getUser();

        $logger = $this->getServiceLocator()->get('Logger');

        if ($facebookUserId) {
            $logger->log(\Zend\Log\Logger::INFO, 'We do have a user with the id ' . $facebookUserId);
            return $userService->getUserByFacebookId($facebookUserId);
        }
        
        $logger->err('Failed getting facebook user by facebook id ' . $facebookUserId);
        return false;

    }

    protected function getFacebook () {
        if (null === $this->facebookSdk) {
            $this->facebookSdk = $this->getServiceLocator()->get('Facebook');
        }
        return $this->facebookSdk;
    }
}
