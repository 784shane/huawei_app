<?php

namespace Application\Auth;

use Application\Model\User;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;


class Instagram
{
    /**
     * Get access token from code
     */ 
    public function getAccessToken($code = '')
    {
        $config = $this->getServiceLocator()->get('config');
        $igConfig = $config['instagram'];

        $adapter = new \Zend\Http\Client\Adapter\Curl();
        $client = new \Zend\Http\Client($igConfig['access_token_url']);

        $client->setAdapter($adapter)
            ->setMethod('post')
            ->setParameterPost(
                array(
                    'client_id' => $igConfig['client_id'],
                    'client_secret' => $igConfig['client_secret'],
                    'grant_type' => 'authorization_code',
                    'redirect_uri' => $igConfig['redirect_uri'],
                    'code' => $code,
                )
            );

        $response = $client->send();

        $igResponse = json_decode($response->getBody());

        // Check for errors
        if (isset($igResponse->code) && $igResponse->code == '400') {
            return array(
                'type' => $igResponse->error_type,
                'message' => $igResponse->error_message,
            );
        }

        // Check for access_token
        if (isset($igResponse->access_token)) {
           return $igResponse->access_token;
        }

        return false;
    }

    /**
     * Get paginated images
     * 
     * @param string $accessToken
     * @param string $nextMaxId
     * @return array|false
     */
    public function getPaginatedImages($accessToken = '', $nextMaxId = 0) {
        
        $config = $this->getServiceLocator()->get('config');
        $count = $config['user_photos']['items_per_page'];
        $igConfig = $config['instagram_api'];
        $ep = sprintf($igConfig['recent_media_endpoint'], $count, $accessToken);

        if ($nextMaxId != 0) {
            $ep .= '&max_id=' . $nextMaxId;
        }
        
        $adapter = new \Zend\Http\Client\Adapter\Curl();
        
        $client = new \Zend\Http\Client($ep);
        $client->setMethod('get')
            ->setAdapter($adapter);
        
        $response = $client->send();

        $igResponse = json_decode($response->getBody());

        if (!isset($igResponse->data)) {
            return false;
        }

        $result['max_id'] = 0;

        if (isset($igResponse->pagination->next_max_id)) {
            $result['max_id'] = $igResponse->pagination->next_max_id;
        }

        $igImages = array(); 

        foreach($igResponse->data as $media) {
            if ($media->type == 'image'){
                $igImages[] = array(
                    'thumb' => $media->images->low_resolution->url,
                    'fullsize' => $media->images->standard_resolution->url,
                    'source' => $media->images->standard_resolution->url
                );
            }
        }

        $result['images'] = $igImages;

        return $result;
        
    }
}
