<?php

namespace Application\Renderer;

use Application\Entity\Experience;
use MaidoCommon\Service\AbstractService;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Image\ImageInterface;
use Imagine\Imagick\Font;
use Imagine\Image\Color;

class ExperienceText extends AbstractService
{
    const SCHEME_LIGHT = 'light';
    const SCHEME_DARK = 'dark';
    
    /**
     * Render an experience and save a media
     *
     * @return Application\Entity\Media
     */
    protected $config;

    /**
     * @var Imagine\ImageInterface
     */
    protected $canvas;

    /**
     * @var Imagine
     */
    protected $imagine;

    /**
     * @var MaidoCommon\Service\Media
     */
    protected $mediaService;


    public function render (Experience $experience, Box $size)
    {
        $imagine = $this->getImagine();

        $canvas = $this->createCanvas($size);

        // var_dump($canvas->getSize()); exit;

        $lines = $this->getTextLines($experience->getText());
        
        $origin = $this->getParagraphOrigin($size->getWidth(), $lines);

        $isLight = $this->getServiceLocator()->get('Application\Renderer\Image')
                        ->isImageLight($experience);
        
        // And add 'My ultimate experience is'
        $canvas = $this->drawLead($canvas, $origin, $isLight);
        
        // Draw each line
        foreach ($lines as $line) {
            $canvas->draw()
                ->text(
                    $line['text'], 
                    $this->getParagraphFont($isLight), 
                    new Point(
                        $line['position']['x'] + $origin->getX(), 
                        $line['position']['y'] + $origin->getY()
                    )
                );    
        }


        return $canvas;
    }

    
    protected function drawLead (ImageInterface $canvas, Point $origin, $isLight)
    {
        $config = $this->getConfig();
        $leadFont = $this->getLeadFont($isLight);
        $text = $config['text']['template']['lead'];
        $width = $this->getStringWidth($text, $leadFont);

        $finalWidth = $config['image']['final_width'];
        $finalHeight = $config['image']['final_height'];

        $y = $origin->getY() - $config['text']['font']['line_height'];
        $x = $this->getCenteredOriginX($width) + $origin->getX();

        $gradient = $this->drawTextGradient($y);
        $canvas->paste($gradient, new Point(0, $y));

        $canvas->draw()->text(
            $text, 
            $leadFont, 
            new Point($x, $y));

        return $canvas;
    }

    protected function drawTextGradient($y)
    { 
        $imagine = $this->getImagine();
        $config = $this->getConfig();

        $gradient = $imagine->open( $this->getBaseDir() . 'public/assets/graphics/gradient-block.png' );
        
        $finalWidth = $config['image']['final_width'];
        $finalHeight = $config['image']['final_height'];

        $gradient
            ->crop(new Point(0, 0), new Box($finalWidth , $finalHeight - $y))
            ->resize(new Box($finalWidth, $finalHeight - $y));  
        
        return $gradient;
    }

    protected function getParagraphOrigin ($totalWidth, array $lines)
    {
        // Calculate total width minus box width, image height minus paragraph height
        $config = $this->getConfig();
        $lineHeight = $config['text']['font']['line_height'];
        $boxWidth = $config['text']['box_width'];
        $boxHeight = $config['text']['box_height'];

        return new Point(
            ($totalWidth - $boxWidth) / 2,
            $boxHeight - (count($lines) + 1) * $lineHeight
        ); 
    }

    protected function getTextLines ($text)
    {
        $config = $this->getConfig();
        $maxWidth = $config['text']['box_width'];

        $spaceWidth = $this->getStringWidth(' ', $this->getParagraphFont()); 
        
        $words = explode(' ', trim($text));
        $widths = $this->getWordsWidths($words);

        $lines = array();
        $currentLine = array();
        $currentLineWidth = 0;
        
        for ($i = 0; $i < count($words); $i++) {
            
            if ($currentLineWidth + $widths[$i] <= $maxWidth) {
                
                // Add word to line
                $currentLine[] = $words[$i];
            
                $currentLineWidth += $spaceWidth + $widths[$i];
            } else {
                // Save line
                $lines[] = $this->getCenteredLine(implode(' ', $currentLine), $currentLineWidth, count($lines));

                // Create a new line
                $currentLine = array($words[$i]);
                $currentLineWidth = $widths[$i];
            }
        }

        // Save whatever is left from the loop
        $lines[] = $this->getCenteredLine(implode(' ', $currentLine), $currentLineWidth, count($lines));

        return $lines;

    }

    protected function getCenteredLine($text, $width, $lineNumber) 
    {
        $config = $this->getConfig();
        
        $lineHeight = $config['text']['font']['line_height'];

        $x = $this->getCenteredOriginX($width);
        $y = $lineNumber * $lineHeight;
        
        return array(
            'text' => $text,
            'position' => array(
                'x' => $x, 
                'y' => $y,
            ),
        );

    }

    protected function getCenteredOriginX($width) 
    {
        $config = $this->getConfig();
        $maxWidth = $config['text']['box_width'];

        return ($maxWidth - $width) / 2;

    }

    protected function getWordsWidths(array $words)
    {
        $widths = array();
        $font = $this->getParagraphFont();

        foreach ($words as $i => $word) {
            $widths[] = $this->getStringWidth($word, $font);
        }

        return $widths;
    }

    protected function getStringWidth($string, $font) 
    {
        $box = imagettfbbox($font->getSize(), 0, $font->getFile(), $string);
        return $box[4] - $box[0];
    }

    protected function getLeadFont($isLight = false)
    {
        return $this->getFont('regular', 'lead', $isLight);
    }


    protected function getParagraphFont($isLight = false)
    {
        return $this->getFont('bold', 'paragraph', $isLight);
    }

    protected function createCanvas (Box $size)
    {
        if ($this->canvas === null) {

            $color = new Color('FFFFFF', 100);
            $imagine = $this->getImagine();

            $this->canvas = $imagine->create($size, $color);
        }

        return $this->canvas;
        
    }

    protected function getFont ($family, $size, $isLight)
    {
        $config = $this->getConfig();
        $imagine = $this->getImagine();

        $fontConfig = $config['text']['font'];
        $scheme = $isLight ? self::SCHEME_DARK: self::SCHEME_LIGHT;
        $color = $fontConfig['color'][$scheme];

        return new Font(
            $this->canvas->getImagick(),
            $fontConfig['family'][$family], 
            $fontConfig['size'][$size], 
            new \Imagine\Image\Color($color)
        );
    }

    protected function getConfig () 
    {
        if ($this->config === null) {
            $config = $this->getServiceLocator()->get('config');
            $this->config = $config['experience'];
        }
        return $this->config;
    }

    protected function getImagine () {

        if (null === $this->imagine) {
            $this->imagine = $this->getServiceLocator()->get('Imagine');
        }
        return $this->imagine;

    }

    protected function getBaseDir () 
    {
        return __DIR__ . '/../../../../../';
    }

}
