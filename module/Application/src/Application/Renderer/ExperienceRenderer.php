<?php

namespace Application\Renderer;

use Application\Entity\Experience;
use Application\Renderer\ExperienceText;
use Application\Renderer\ExperienceImage;
use MaidoCommon\Service\AbstractService;
use Imagine\Image\Box;
use Imagine\Image\Point;

class ExperienceRenderer extends AbstractService
{
    /**
     * Render an experience and save a media
     *
     * @return Application\Entity\Media
     */

    protected $config;

    /**
     * @var Imagine
     */
    protected $imagine;

    /**
     * @var MaidoCommon\Service\Media
     */
    protected $mediaService;


    public function render (Experience $experience)
    {

        $config = $this->getConfig();
        $imagine = $this->getImagine();

        // Create a new image with imagine
        $canvas = $this->getCanvas();

        $finalHeight = $config['image']['final_height'];

        // Get cropped version of sourceImage
        $imageRenderer = $this->getServiceLocator()->get('Application\Renderer\Image');
        $cropped = $imageRenderer->render($experience);

        // Render text
        $textRenderer = $this->getServiceLocator()->get('Application\Renderer\Text');
        $text = $textRenderer->render($experience, $canvas->getSize());

        $banner = $this->getBanner();

        $canvas
            ->paste($cropped, new Point(0, 0))
            ->paste($text, new Point(0, 0))
            ->paste($banner, new Point(0, $finalHeight));
        
        $config = $this->getConfig();
        $imagine = $this->getImagine();
        $finalWidth = $config['image']['final_width'];
        $finalHeight = $config['image']['final_height'] + $config['banner']['height'];

        $media = $this->publishMedia($canvas);


        // Return media
        return $media;
    }

    protected function publishMedia ($canvas) 
    {   
        $config = $this->getConfig();
        $mediaService = $this->getMediaService();

        // Create temporal file to store the rendered image
        $dir = $this->getBaseDir() . 'data/uploads/experience';
        $filename = sprintf('%s.%s', uniqid(), 'jpg');
        $path = sprintf('%s/%s', $dir, $filename);

        // Store rendered
        $canvas->save($path, array('quality' => $config['image']['jpeg_quality']));

        // Create empty media object
        $media = new \MaidoCommon\Model\Media(array(
            'is_local' => 1,
            'type' => 'Image',
            'caption' => '',
            'url' => 'experience/' . $filename,
        ));

        // Store media and create viewports
        $media->id = $mediaService->storeMedia($media, $path);
        $mediaService->publishMedia($media, 'experience');

        $entityManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $mediaEntity = $entityManager->find('Application\Entity\Media', $media->id);

        return $mediaEntity;
    }

    /**
     * @return Imagine\ImageInterface
     */
    protected function getCanvas()
    {
        $config = $this->getConfig();
        $imagine = $this->getImagine();
        $finalWidth = $config['image']['final_width'];
        $finalHeight = $config['image']['final_height'] + $config['banner']['height'];
        $canvas = $imagine->create(new Box($finalWidth, $finalHeight));
        
        return $canvas;
    }

    /**
     * @return Imagine\ImageInterface
     */
    protected function getBanner()
    {   
        $imagine = $this->getImagine();

        $config = $this->getConfig();
        $banner = $imagine->open($config['banner']['src']);

        return $banner;
    }

    protected function getConfig () 
    {
        if ($this->config === null) {
            $config = $this->getServiceLocator()->get('config');
            $this->config = $config['experience'];
        }
        return $this->config;
    }

    protected function getImagine () {

        if (null === $this->imagine) {
            $this->imagine = $this->getServiceLocator()->get('Imagine');
        }
        return $this->imagine;

    }

    protected function getMediaService () {

        if (null === $this->mediaService) {
            $this->mediaService = $this->getServiceLocator()->get('MaidoCommon\Service\Media');
        }
        return $this->mediaService;

    }

    protected function getBaseDir () 
    {
        return __DIR__ . '/../../../../../';
    }

}
