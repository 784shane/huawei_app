<?php

namespace Application\Renderer;

use Application\Entity\Experience;
use MaidoCommon\Service\AbstractService;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Image\ImageInterface;

class ExperienceImage extends AbstractService
{
    /**
     * @var array
     */
    protected $config;

    /**
     * @var Imagine
     */
    protected $imagine;

    /**
     * @var MaidoCommon\Service\Media
     */
    protected $mediaService;


    /** 
     * Crop and resize source image, return an image interface
     * @param  Experience $experience [description]
     * @return Imagine\ImageInterface                 [description]
     */
    public function render (Experience $experience)
    {

        $imagine = $this->getImagine();

        $sourceMedia = $experience->getSourceImage()->getMedia();

        $sourceImagePath = $this->getMediaImagePath($sourceMedia->getId());

        $sourceImage = $imagine->open($sourceImagePath);

        $cropped = $this->cropResizeImage($sourceImage, $experience);

        return $cropped;

    }

    protected function cropResizeImage(ImageInterface $image, Experience $experience) 
    {
        $imagine = $this->getImagine();
        $config = $this->getConfig();

        $finalWidth = $config['final_width'];
        $finalHeight = $config['final_height'];

        $cropX = min((int)$experience->getX(), 0);
        $cropY = min((int)$experience->getY(), 0);
        $cropWidth = $experience->getWidth() - min($experience->getX(), 0);
        $cropHeight = $experience->getHeight() - min($experience->getY(), 0); 


        $cropX = $experience->getX();
        $cropY = $experience->getY();
        $cropWidth = $experience->getWidth();
        $cropHeight = $experience->getHeight();
        // $finalWidth = 320;
        // $finalHeight = 320;

        error_log($experience->getX());
        error_log($experience->getY());
        error_log($cropX);
        error_log($cropY);
        error_log($cropWidth);
        error_log($cropHeight);


        $image
            ->crop(new Point($cropX, $cropY), new Box($cropWidth, $cropHeight))
            ->resize(new Box($finalWidth, $finalHeight));  

        $canvas = $imagine->create(new Box($finalWidth, $finalHeight));

        $x = max($experience->getX() * -1, 0);
        $y = max($experience->getY() * -1, 0);

        if ($x <= $config['final_width'] || $y <= $config['final_height']) {
            $canvas->paste($image, new Point($x, $y));
        }

        return $canvas;
    }

    public function isImageLight (Experience $experience) 
    {
        $sourceMedia = $experience->getSourceImage()->getMedia();
        $path = $this->getMediaImagePath($sourceMedia->getId());
        $image = $this->getImagine()->open($path);

        $color = $image->copy()->resize(new Box(1,1))->getColorAt(new Point(0,0));
        $r = $color->getRed();
        $g = $color->getGreen();
        $b = $color->getBlue();

        return ((($r * 299) + ($g * 587) + ($b * 114)) / 1000) > 210;

    }

    protected function getMediaImagePath ($mediaId)
    {
        $mediaService = $this->getMediaService();

        $viewport = $mediaService->getViewport($mediaId, 'source_image/fullsize');
        
        if ($viewport->isLocal) {
            return sprintf($this->getBaseDir() . 'public/media/%s', $viewport->url);    
        } else {
            return $viewport->url;
        }
    }



    protected function getConfig () 
    {
        if ($this->config === null) {
            $config = $this->getServiceLocator()->get('config');
            $this->config = $config['experience']['image'];
        }
        return $this->config;
    }

    protected function getImagine () {

        if (null === $this->imagine) {
            $this->imagine = $this->getServiceLocator()->get('Imagine');
        }
        return $this->imagine;

    }

    protected function getMediaService () {

        if (null === $this->mediaService) {
            $this->mediaService = $this->getServiceLocator()->get('MaidoCommon\Service\Media');
        }
        return $this->mediaService;

    }

    protected function getBaseDir () 
    {
        return __DIR__ . '/../../../../../';
    }

}
