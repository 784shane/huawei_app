<?php

namespace Application\Service;

use MaidoCommon\Service\AbstractService;
use MaidoCommon\Model\Media; 
use Application\Entity\SourceImage;

class SourceImageService extends AbstractService
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * Project repository
     */
    protected $sourceImageRepository;

    protected function getEntityManager()
    {
        if (null === $this->entityManager) {
            $this->entityManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }

        return $this->entityManager;
        
    }

    protected function getSourceImageRepository()
    {
        if (null === $this->sourceImageRepository) {
            $this->sourceImageRepository = $this->getEntityManager()->getRepository('Application\Entity\SourceImage');
        }

        return $this->sourceImageRepository;
    }

    public function getStockImages()
    {
        return $this->getSourceImageRepository()->findBy(
                array('provider' =>'stock')
            );
    }

    /**
     * @param int $id
     * @return \Application\Model\SourceImage|null
     */
    public function getSourceImageById($id)
    {
        $repository = $this->getSourceImageRepository();
        return $repository->findOneBy(array(
            'id' => $id
        ));
    }

    /**
     * Save a Source Image
     * @param SourceImage $sourceImage
     * @return null
     */
    public function saveSourceImage(SourceImage $sourceImage)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($sourceImage);
        $entityManager->flush();

        return $sourceImage;
    }

    public function deleteSourceImage(SourceImage $sourceImage)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->remove($sourceImage);
        $entityManager->flush();
    }
}
