<?php

namespace Application\Service;

use MaidoCommon\Service\AbstractService;
use Application\Entity\Experience;
use Application\Entity\ExperienceTag;
use Doctrine\ORM\Tools\Pagination\Paginator;


class ExperienceService extends AbstractService
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * Project repository
     */
    protected $experienceRepository;

    protected function getEntityManager()
    {
        if (null === $this->entityManager) {
            $this->entityManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }

        return $this->entityManager;
        
    }

    protected function getExperienceRepository()
    {
        if (null === $this->experienceRepository) {
            $this->experienceRepository = $this->getEntityManager()->getRepository('Application\Entity\Experience');
        }

        return $this->experienceRepository;
    }

    public function getExperiences()
    {
        return $this->getExperiencesRepository()->findAll();
    }

    public function getPaginatedExperiences($pageId = 1)
    {   
        $resultsPerPage = 50;
        $start = ((int) $pageId - 1) * $resultsPerPage;
        $dql = "SELECT e, t FROM Application\Entity\Experience e LEFT JOIN e.experienceTags t";

        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQuery($dql)
                               ->setFirstResult($start)
                               ->setMaxResults($resultsPerPage);

        return new Paginator($query, $fetchJoinCollection = true);
    }

    /**
     * @param int $id
     * @return \Application\Model\Experience|null
     */
    public function getExperienceById($id)
    {
        $repository = $this->getExperienceRepository();
        return $repository->findOneBy(array(
            'id' => $id
        ));
    }

    public function saveExperience(Experience $experience)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($experience);
        $entityManager->flush();
    }

    public function saveExperienceTag(ExperienceTag $tag)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($tag);
        $entityManager->flush();
    }

}
