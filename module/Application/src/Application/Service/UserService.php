<?php

namespace Application\Service;

use MaidoCommon\Service\AbstractService;
use Application\Entity\User;

class UserService extends AbstractService
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * Project repository
     */
    protected $userRepository;

    protected function getEntityManager()
    {
        if (null === $this->entityManager) {
            $this->entityManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }

        return $this->entityManager;
        
    }

    protected function getUserRepository()
    {
        if (null === $this->userRepository) {
            $this->userRepository = $this->getEntityManager()->getRepository('Application\Entity\User');
        }

        return $this->userRepository;
    }

    public function getUsers()
    {
        return $this->getUserRepository()->findAll();
    }

    /**
     * @param int $id
     * @return \Application\Model\User|null
     */
    public function getUserById($id)
    {
        $repository = $this->getUserRepository();
        return $repository->findOneBy(array(
            'id' => $id
        ));
    }

    public function saveUser(User $user)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($user);
        $entityManager->flush();
    }

    /**
     * @param int $id
     * @return \Application\Model\User|null
     */
    public function getUserByFacebookId($facebookId)
    {
        $repository = $this->getUserRepository();
        return $repository->findOneBy(array(
            'facebookId' => $facebookId
        ));
    }

}
