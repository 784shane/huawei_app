<?php 

namespace Application\Validator;

use Zend\Validator\AbstractValidator;

class BadLanguageValidator extends AbstractValidator
{

    const BAD_LANGUAGE = 'badLanguageDetected';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $messageTemplates = array(
        self::BAD_LANGUAGE  => "Bad words detected",
    );

    /**
     * Options for this validator
     *
     * @var array
     */

    protected $options = array(
        'banned_words' 	=> array(),
    );

    /**
     * Returns true if with the provided scale a crop area can be created inside the image size
     *
     * @param  string $value
     * @return bool
     */

    public function isValid($value, $context = null)
    {
        if (count($this->options['banned_words'])) {
            if (preg_match("/\b(" . implode('|', $this->options['banned_words']) . ")\b/i", $value)) {
                $this->error(self::BAD_LANGUAGE);
                return false;
            }
        }    
        return true;
    }
}

