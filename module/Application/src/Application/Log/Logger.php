<?php

namespace Application\Log;

use Zend\Session\Container;
use Zend\Log\Logger as ZendLogger;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Logger extends ZendLogger implements ServiceLocatorAwareInterface
{

    protected $serviceLocator;

    /**
     * 
     * Add a message as a log entry
     *
     * @param  int $priority
     * @param  mixed $message
     * @param  array|Traversable $extra
     * @return Logger
     * @throws Exception\InvalidArgumentException if message can't be cast to string
     * @throws Exception\InvalidArgumentException if extra can't be iterated over
     * @throws Exception\RuntimeException if no log writer specified
     */
    public function log($priority = \Zend\Log\Logger::INFO, $message, $extra = array())
    {  
        if (!isset($extra['user_agent'])) {
            $extra['user_agent'] = $this->getUserAgent();
        }

        parent::log($priority, $message, $extra);
    }

    /**
     * Override Logger emerg
     * @param string $message
     * @param array|Traversable $extra
     * @return Logger
     */
    public function emerg($message, $extra = array())
    {   
        $config = $this->getServiceLocator()->get('config');

        try{
            $addresses = $config['logger']['emergency']['contact_addresses'];
            $template = $config['logger']['emergency']['template'];
            $subject = $config['logger']['emergency']['subject'];

            $mailer = $this->getServiceLocator()->get('MaidoCommon\Service\Mailer');

            $view = new \Zend\View\Model\ViewModel(array(
                'message' => $message,
                'date' => date('Y-m-d H:i:s'),
            ));

            $view->setTemplate($template);

            foreach ($addresses as $address) {
                $htmlMessage = $mailer->createHtmlMessage($address, $subject, $view);   
                $mailer->send($htmlMessage);  
            }

        } catch (\Exception $e) {
            $this->err('Impossible to send emergency message.');
        }

        return $this->log(self::EMERG, $message, $extra);
    }

    /**
     * Return user agent string if present
     * @return  string
     */

    protected function getUserAgent()
    {
        $request = $this->getServiceLocator()->get('Request');

        if ($request instanceof \Zend\Http\Request){
            $headers = $request->getHeaders();

            if ($headers->has('User-Agent')) {
                return $headers->get('User-Agent')->getFieldValue();
            }
        }
        

        return '';
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator(){
        return $this->serviceLocator;
    }
}