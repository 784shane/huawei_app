<?php

namespace Application\Log\Service;

use Application\Log\Logger;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LoggerFactory implements FactoryInterface
{

    /**
     * Create Zend\Log\Logger instance
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return \Zend\Log\Logger
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');
        $adapter = $serviceLocator->get('Zend\Db\Adapter\Adapter');
        $mapping = array(
            'timestamp' => 'timestamp',
            'priority' => 'priority',
            'message' => 'message',
            'extra' => array(
                'user_agent' => 'user_agent',
                'user_id' => 'user_id',
            ),
        );

        $writer = new \Zend\Log\Writer\Db($adapter, 'log', $mapping);
        $logger = new Logger();
        $logger->addWriter($writer);
        return $logger;
    }
}