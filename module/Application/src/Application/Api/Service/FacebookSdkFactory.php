<?php

namespace Application\Api\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class FacebookSdkFactory implements FactoryInterface
{

    /**
     * Create Facebook SDK instance
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return \Facebook
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');
        return new \Facebook(array(
            'appId' => $config['facebook']['app']['id'],
            'secret' => $config['facebook']['app']['secret']
        ));
    }
}