<?php

namespace Application\View\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Application\View\Helper\ViewConfiguration;

class ViewConfigurationFactory implements FactoryInterface
{

    /**
     * Create view helper
     *
     * @param ServiceLocatorInterface $viewManager
     * @return \Application\View\Helper\ViewConfiguration
     */
    public function createService(ServiceLocatorInterface $viewManager)
    {
        $serviceLocator = $viewManager->getServiceLocator();
        $config = $serviceLocator->get('config');

        $viewConfiguration = new ViewConfiguration();
        $viewConfiguration->setConfig($config);
        
        return $viewConfiguration;
    }

}
