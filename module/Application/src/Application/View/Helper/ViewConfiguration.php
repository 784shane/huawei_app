<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Class ViewConfiguration
 * 
 * @package Application\View\Helper
 */
class ViewConfiguration extends AbstractHelper {

    /**
     * 
     * @var array
     */ 
	protected $config;

    public function setConfig($config = array())
    {
        $this->config = $config;
    }

    public function getAnalyticsAccountId()
    {
        return $this->config['analytics']['account_id'];
    }

    public function getFacebookAppId()
    {
        return $this->config['facebook']['app']['id'];
    }

    public function getFacebookAppUrl()
    {
        return $this->config['facebook']['app']['url'];
    }

    public function getFacebookAppNamespace()
    {
        return $this->config['facebook']['app']['namespace'];
    }

    public function getFacebookAppBasicScope()
    {
        return $this->config['facebook']['app']['scope'];
    }

    public function getFacebookShareDescription()
    {
        return $this->config['facebook']['app']['description'];
    }

    public function getFacebookShareTitle()
    {
        return $this->config['facebook']['app']['title'];
    }

    public function getFacebookShareImage()
    {
        return $this->config['facebook']['app']['image'];
    }

    public function getFacebookOpenGraphAction()
    {
        return $this->config['facebook']['open_graph']['action'];
    }

    public function getFacebookOpenGraphType()
    {
        return $this->config['facebook']['open_graph']['type'];
    }

    public function getTwitterShare()
    {
        return $this->config['twitter']['share'];
    }
    
    public function getInstagramClientId()
    {
        return $this->config['instagram']['client_id'];
    }

    public function getInstagramRedirectUrl()
    {
        return $this->config['instagram']['redirect_uri'];
    }

    public function getBannedWords()
    {
        return $this->config['banned_words'];
    }

    public function getSourceImageMinWidth()
    {
        return $this->config['experience']['image']['final_width'];
    }

    public function getSourceImageMinHeight()
    {
        return $this->config['experience']['image']['final_height'];
    }

    public function getSourceImageMaxResample()
    {
        return $this->config['experience']['image']['max_resample'];
    }

    public function shouldPreventJavascriptCaching()
    {
        return $this->config['javascript']['prevent_caching'];
    }

    public function shouldUseMinifiedJavascript()
    {
        return $this->config['javascript']['use_minified'];
    }

}