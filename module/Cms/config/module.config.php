<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'acl' => array(
        'roles' => array(
            'administrator' => null,
        ),
        'resources' => array(
            'cms' => 'administrator',
            'cms/experiences' => 'administrator',
            'cms/stockimages' => 'administrator',
            'cms/stockimages/upload' => 'administrator',
            'cms/stockimages/delete' => 'administrator',
        ),
    ),
    'router' => array(
        'routes' => array(
            'cms' => array(
                'type' => 'literal',
                'options' => array(
                    'route'    => '/cms',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Experience',
                        'action'     => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'experiences' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route'    => '/experiences',
                            'defaults' => array(
                                'controller' => 'Cms\Controller\Experience',
                                'action'     => 'index',
                            ),
                        ),
                    ),
                    'csv' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route'    => '/csv',
                            'defaults' => array(
                                'controller' => 'Cms\Controller\Experience',
                                'action'     => 'csv',
                            ),
                        ),
                    ),
                    'stockimages' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route'    => '/stockimages',
                            'defaults' => array(
                                'controller' => 'Cms\Controller\StockImage',
                                'action'     => 'index',
                            ),
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'upload' => array(
                                'type' => 'literal',
                                'options' => array(
                                    'route'    => '/upload',
                                    'defaults' => array(
                                        'controller' => 'Cms\Controller\StockImage',
                                        'action'     => 'upload',
                                    ),
                                ),
                            ),
                            'delete' => array(
                                'type' => 'segment',
                                'options' => array(
                                    'route'    => '/delete/:id',
                                    'defaults' => array(
                                        'controller' => 'Cms\Controller\StockImage',
                                        'action'     => 'delete',
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Cms\Controller\Experience' => 'Cms\Controller\ExperienceController',
            'Cms\Controller\StockImage' => 'Cms\Controller\StockImageController'
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Cms\Auth\Http\Adapter' => 'Cms\Auth\Http\Service\AuthenticationAdapterFactory',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'layout/cms' => __DIR__ . '/../view/layout/layout.phtml',
            'cms/index/index' => __DIR__ . '/../view/cms/index/index.phtml',
            'cms/experience/experience' => __DIR__ . '/../view/cms/experience/partials/experience.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'module_layouts' => array(
        'Cms' => 'layout/cms',
   ),
    'cms_auth' => array(
        'config' => array(
            'accept_schemes' => 'digest',
            'realm'          => 'Huawei Experiences CMS',
            'digest_domains' => '/cms',
            'nonce_timeout'  => 3600,
        ),
    ),
);
