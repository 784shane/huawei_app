<?php

namespace Cms;

use Zend\Mvc\ModuleRouteListener;
use Zend\Session\Container;
use Zend\Mvc\MvcEvent;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;

class Module
{   
    /**
     * Main bootstrap
     * 
     * @param \Zend\EventManager\Event $e
     */
    public function onBootstrap($e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $serviceManager = $e->getApplication()->getServiceManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $eventManager->attach('route', array($this, 'acl'));
   }

    /**
     * Get this module configuration.
     * 
     * @return array
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Get autoloader configuration.
     * 
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function acl(MvcEvent $e)
    {
        $acl = new Acl();
        $application = $e->getApplication();
        $sm = $application->getServiceManager();
        $request = $e->getRequest();
        $response = $e->getResponse();
        $config = $sm->get('Config');
        
        $route = $e->getRouteMatch()->getMatchedRouteName();

        if (isset($config['acl']['roles']) && is_array($config['acl']['roles'])) {
            foreach ($config['acl']['roles'] as $role => $parents) {
                $acl->addRole(new Role($role), $parents);
            }
        }

        if (isset($config['acl']['resources']) && is_array($config['acl']['resources'])) {
            foreach ($config['acl']['resources'] as $resource => $roles) {
                $r = new Resource($resource);

                $acl->addResource($r);
                $acl->allow($roles, $r);
            }
        }

        if ($acl->hasResource($route)) {
            
            $authAdapter = $sm->get('Cms\Auth\Http\Adapter');

            $authAdapter->setRequest($request);
            $authAdapter->setResponse($response);

            $result = $authAdapter->authenticate();
            $role = null;

            if ($result->isValid()) {
                $role = 'administrator';
            }

            if (!$acl->hasRole($role) || !$acl->isAllowed($role, $route)) {  
                $response->setContent('Access denied');
                $response->setStatusCode(401);
                return $response;
            }
        }
    }
}