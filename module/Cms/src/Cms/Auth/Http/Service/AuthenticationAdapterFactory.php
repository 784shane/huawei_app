<?php

namespace Cms\Auth\Http\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Authentication\Adapter\Http as HttpAdapter;
use Zend\Authentication\Adapter\Http\FileResolver;

class AuthenticationAdapterFactory implements FactoryInterface
{

    /**
     * Create view helper
     *
     * @param ServiceLocatorInterface $viewManager
     * @return \Application\View\Helper\OpenGraphTags
     */
    public function createService(ServiceLocatorInterface $sm)
    {
        
        $config = $sm->get('config');
        $authConfig = $config['cms_auth'];
        $authAdapter = new HttpAdapter($authConfig['config']);
        $digestResolver = new FileResolver();
        $digestResolver->setFile($authConfig['passwd_file']);
        $authAdapter->setDigestResolver($digestResolver);

        return $authAdapter;
    }

}
