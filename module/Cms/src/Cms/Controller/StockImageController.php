<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Cms\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Entity\SourceImage;

class StockImageController extends AbstractActionController
{

    protected $sourceImageService;

    public function indexAction () 
    {
        $sourceImageService = $this->getSourceImageService();

        $stockImages = $sourceImageService->getStockImages();

        $result = array();
        
        foreach ($stockImages as $stockImage) {
            $result[] = (object) array(
                'id' => $stockImage->getId(),
                'src' => $this->viewportUrl($stockImage->getMedia()->getId(), 'source_image/thumbnail'),
            );
        }

        return new ViewModel(array(
            'images' => $result,
            'errors' => $this->flashObject()->get()
        ));
    }

    public function deleteAction ()
    {
        $id = $this->params()->fromRoute('id');

        $sourceImageService = $this->getSourceImageService();
        $sourceImage = $sourceImageService->getSourceImageById($id);

        if ($sourceImage) {        
            $sourceImageService->deleteSourceImage($sourceImage);
            $mediaService = $this->getServiceLocator()->get('MaidoCommon\Service\Media');
            $mediaService->deleteMedia($sourceImage->getMedia()->getId());
        }

        $this->redirect()->toRoute('cms/stockimages');

    }

    public function uploadAction () 
    {

        $imageForm = $this->getServiceLocator()->get('Application\Form\RemoteSourceImage');

        $imageForm->setData($this->getRequest()->getFiles()->toArray());

        if ($imageForm->isValid()) {
            
            $media = $this->publishMedia($imageForm->get('image')->getValue(), 'source_image');

            $sourceImage = new SourceImage();
            $sourceImage->setMedia($media);
            $sourceImage->setProvider('stock');
                            
            $this->getSourceImageService()->saveSourceImage($sourceImage);

        } else {

            $errors = array();
            foreach ($imageForm->getMessages() as $field => $messages) {
                foreach ($messages as $error) {
                    $errors[] = $field . ': ' . $error;
                }
            }

            $this->flashObject()->set((object) $errors);
        }

        $this->redirect()->toRoute('cms/stockimages');
        
    }

    protected function getSourceImageService ()
    {
        if ($this->sourceImageService === null) {
            $this->sourceImageService = $this->getServiceLocator()->get('Application\Service\SourceImage');
        }

        return $this->sourceImageService;
    }
}
