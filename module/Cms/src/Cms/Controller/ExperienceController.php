<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Cms\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class ExperienceController extends AbstractActionController
{
    /** 
     * @var Application\Service\Experience
     */
    protected $experienceService;

    public function indexAction () 
    {

        $pageId = $this->params()->fromQuery('page') ?: 1;

        $experienceService = $this->getExperienceService();

        $result = $experienceService->getPaginatedExperiences($pageId);

        $experiences = array();

        foreach ($result as $experience) {
            $user = $experience->getUser();

            $experiences[] = array(
                'id' => $experience->getId(),
                'thumb' => $this->viewportUrl($experience->getRenderedImage()->getId(), 'experience/thumbnail'),
                'fullsize' => $this->viewportUrl($experience->getRenderedImage()->getId(), 'experience/fullsize'),
                'text' => $experience->getText(),
                'name' => sprintf('%s %s', $user->getFirstName(), $user->getLastName()),
                'date' => date_format($user->getCreatedDate(), 'd-m-Y H:i:s'),
            );
        }

        $variables = array(
            'experiences' => $experiences
        );

        if ($this->getRequest()->isXmlHttpRequest()) {
            return new JsonModel($variables);
        } else {
            return new ViewModel($variables);
        }
        
    }
    

    public function csvAction () 
    {

        $adapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');

        $sql = 'SELECT 
                e.id, 
                COALESCE(date_format(e.created_date, "%d-%m-%Y %H:%i:%s"), "") as created_date,
                e.text, 
                COALESCE(u.first_name, "") as first_name,
                COALESCE(u.last_name, "") as last_name,
                COALESCE(u.email, CONCAT(u.facebook_id, "@facebook.com")) as email,
                COALESCE(u.gender, "n/a") as gender,
                COALESCE(u.birthday, "n/a") as birthday,
                COALESCE(u.location, "n/a") as location,
                u.facebook_id,
                si.provider as image_provider
                FROM experience e, user u, source_image si
                WHERE u.id = e.user_id AND si.id = e.source_image_id
                ORDER BY e.id DESC';

        $result = $adapter->query($sql, \Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);
        
        $tmpCsvFilename = tempnam( __DIR__ . '../../../../data/uploads', 'csv');
        $csvFile = fopen($tmpCsvFilename, 'w+');
        $titles = array(
            'Id',
            'Created date',
            'Text',
            'Name',
            'Email',
            'Gender',
            'Birthday',
            'Location',
            'Facebook id',
            'Image provider',
        );

        fputcsv($csvFile, $titles);

        foreach ($result as $row) {
            fputcsv($csvFile, array(
                $row->id,
                $row->created_date,
                $row->text,
                sprintf('%s %s', $row->first_name, $row->last_name),
                $row->email,
                $row->gender,
                $row->birthday,
                $row->location,
                $row->facebook_id,
                ucfirst($row->image_provider),
            ));
        }

        $size = filesize($tmpCsvFilename);
        $date = date('dmY-his');

        $response = $this->getResponse();
        $headers = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'text/csv')
                ->addHeaderLine('Content-Disposition', sprintf("attachment; filename=\"experiences_%s.csv\"", $date))
                ->addHeaderLine('Accept-Ranges', 'bytes')
                ->addHeaderLine('Content-Length', $size);

        $response->setContent(file_get_contents($tmpCsvFilename));

        fclose($csvFile);
        unlink($tmpCsvFilename);

        return $response;
    }

    protected function getExperienceService()
    {
        if ($this->experienceService === null) {
            $this->experienceService = $this->getServiceLocator()->get('Application\Service\Experience');
        }
        return $this->experienceService;
    }
}
