define(
    ['module'], function (module) {
        var PixelTracker = function () {
            this.track = function (url) {
                var i = document.createElement('img');
                $(i).css('display', 'none').attr('src', url);
                $('body').append(i);
            };
        };

        return new PixelTracker();
    }
);