require.config({
    urlArgs: window.config.preventJavascriptCaching ? 'bust=' + (new Date()).getTime() : '',
    waitSeconds: 240,
    shim: {
        'jquery': {
            exports: 'jQuery'
        },
        'underscore': {
            exports: '_'
        },
        'backbone': {
            deps: ['underscore', 'jquery', 'easing'],
            exports: 'Backbone'
        },
        'easing': {
            deps: ['jquery']
        },
        'masonry': {
            deps: ['jquery']
        },
        'mobilecheck': {
            exports: 'isMobile'
        },
        'jquery-ui': {
            deps: ['jquery']
        },
        'jquery-cookie': {
            deps: ['jquery']
        }
    },
    paths: {
        'jquery': '/assets/js/vendor/jquery-min',
        'jquery-ui': '/assets/js/vendor/jquery-ui-1.10.3.custom.min',
        'hammer': '/assets/js/vendor/jquery.hammer.min',
        'easing': '/assets/js/vendor/jquery.easing',
        'underscore': '/assets/js/vendor/underscore-min',
        'backbone': '/assets/js/vendor/backbone-min',
        'spin': '/assets/js/vendor/spin.min'
    },
    config: {
        'util/analytics' : {
            analyticsId : window.config.analyticsId
        },
        'app/view/main': {
            shareDescription: window.config.facebook.share.description,
            shareTitle: window.config.facebook.share.title,
            redirectUrl: window.config.facebook.share.url
        },
        'app/service/facebook': {
            appId: window.config.facebook.app.id,
            scope: window.config.facebook.app.scope
        },
        'app/service/facebook-photos': {
            finalWidth: window.config.sourceImage.finalWidth / window.config.sourceImage.maxResample,
            finalHeight: window.config.sourceImage.finalHeight / window.config.sourceImage.maxResample,
            paginationLimit: 100,
            maxId: 1000
        },
        'app/view/page/edit/crop-tool': {
            finalWidth: window.config.sourceImage.finalWidth,
            finalHeight: window.config.sourceImage.finalHeight,
            maxResample: window.config.sourceImage.maxResample
        },
        'app/control/app-control': {
            picEndWidth: window.config.sourceImage.finalWidth,
            picEndHeight: window.config.sourceImage.finalHeight,
            friendListLimit: 8
        },
        'app/service/instagram': {
            accessToken: window.bootstrap.instagramAccessToken,
            clientId: window.config.instagram.clientId,
            redirectUrl: window.config.instagram.redirectUrl,
            paginationLimit: 100
        },
        'app/view/page/start': {
            scope: window.config.facebook.app.scope
        },
        'app/view/page/example': {
            scope: window.config.facebook.app.scope
        }
    }
});

require(["app/app"], function () {});