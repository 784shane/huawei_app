define(['backbone'], function (Backbone) {

        var AppRouter = Backbone.Router.extend({
            history: [],
            initialize: function (options) {
                if (options.routes) {
                    this.routes = options.routes;
                }
                this.on('route', this.onRoute, this);
            },
            back: function () {
                var h = this.history,
                    r = h.length > 1 ? h[(h.length - 2)] : '/'; // Go home if no previous route

                Backbone.history.navigate(r, true);
            },
            onRoute: function () {
                var u = Backbone.history.fragment,
                    page = !/^\//.test(u) && u !== "" ? u : '/' + u;

                Backbone.trigger('app:pageview', page);
                this.history.push(u);
            }
        });

        return new AppRouter({
            routes: {
                '/': 'home',
                'app/:id' : 'getPost',
                'terms-conditions': 'terms-conditions',
                'edit': 'edit',
                'edit/select-source': 'select-source',
                'edit/select-image': 'select-image',
                'edit/crop-image': 'crop-image',
                'edit/write': 'write',
                'edit/preview': 'preview',
                'thanks': 'thanks',
                '*path': 'home'
            }
        });
    }
);