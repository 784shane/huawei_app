var defs = [
    'module',
    'backbone',
    'app/view/main',
    'util/analytics',
    'util/hide-addressbar',
    'util/ios-detection'
];

define(defs, function (module, Backbone, mainView, Analytics) {
    
    /*
     * App events
     * ---
     * app:error [message]
     * app:load:start [key]
     * app:load:complete [key]
     * app:pageview [page]
     * app:back []
     * app:resize []
     */

    Backbone.on({

        'app:pageview': function (page) {
            // Track with analytics
            if (window._gaq) {
                _gaq.push(['_trackPageview', page]);
            }
        },
        'app:error': function (message) {
            Backbone.trigger('app:log', message, 'Error');
        },
        'app:log': function (message, type) {
            // Track as an analytics event
            if (window._gaq) {
                _gaq.push(['_trackEvent', (type || 'Log'), message, navigator.userAgent, 1]);
            }
        }
    });

    Backbone.trigger('app:load:start', 'start');

    // Load complete
    //Backbone.history.start({pushState: true, root: '/'});

    //Backbone.trigger('app:load:complete', 'start');

});
