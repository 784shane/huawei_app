define(
    [
        'module',
        'backbone',
        'app/model/remote-image'
    ],
    function (module, Backbone, RemoteImage) {

        return Backbone.Collection.extend({
            model: RemoteImage,
            url: function () {
                return 'app/imagereturn';
            }
        });
    }
);