define(
    [
        'module',
        'backbone'
    ],
    function (module, Backbone) {

        return Backbone.Model.extend({
            defaults: {
                'id': null,
                'text': '',
                'source_image_provider': null,
                'source_image_id': null,
                'source_image_url': null,
                'crop_x': null,
                'crop_y': null,
                'crop_width': null,
                'crop_height': null,
                'url': null,
                'shortUrl': null
            },
            urlRoot: '/experience'
        });
    }
);

