define(
    [
        'module',
        'backbone'
    ],
    function (module, Backbone) {

        return Backbone.Model.extend({
            defaults: {
                'id': null,
                'url': null,
                'thumb': ''
            }
        });
    }
);