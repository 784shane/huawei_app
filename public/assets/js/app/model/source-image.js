define(
    [
        'module',
        'backbone'
    ],
    function (module, Backbone) {

        return Backbone.Model.extend({
            defaults: {
                'id': null,
                'provider': '',
                'source_url': '', // This should be validated to only allow instagram and facebook domains
                'url': null
            },
            urlRoot: '/source-image'
        });
    }
);