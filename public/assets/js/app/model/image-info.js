define(
    [
        'module',
        'backbone'
    ],
    function (module, Backbone) {

        return Backbone.Model.extend({
            defaults: {
                'image_url': null,
                'realdimensionsAction': null
            },
            urlRoot: '/app/realdimensions'
        });
    }
);