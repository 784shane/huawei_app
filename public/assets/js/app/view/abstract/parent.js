define(
    [
        'backbone',
        'app/view/abstract/child'
    ],
    function (Backbone, AbstractChildView) {

        return AbstractChildView.extend({
            current: null,
            transitioning: false,
            // Elements enumerated in this object mantain a relation of exclusion between themselves
            children: {},
            // Components can be simultaneously be displayed
            components: {},
            hideCurrent: function () {
                var self = this;
                return this.current.doTransitionOut().done(function () {
                    self.current = null;
                });
            },
            showChild: function (child) {
                var self = this,
                    transition = new $.Deferred();

                if (this.current === this.children[child]) {
                    return;
                }

                transition.done(function () {
                    self.current = self.children[child];
                    self.current.doTransitionIn();
                });
                
                if (this.current) {
                    this.hideCurrent().done(function () {
                        transition.resolve();
                    });
                } else {
                    transition.resolve();
                }
            }
        });
    }
);