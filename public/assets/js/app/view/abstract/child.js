define([
        'backbone'
    ], function (Backbone) {

        function resolved () {
            var def = new $.Deferred();
            return def.resolve();
        }
        return Backbone.View.extend({
            transitioning: false,
            _transitionInPromise: null,
            _transitionOutPromise: null,
            /** 
             * @final 
             * Whatever the implementation of transitionIn is, always set the member variable
             * transitioning to true before starting the transition.
             * If the transition is currently running return the promise.
             * 
             * @return Promise
             */
            doTransitionIn: function () {
                var self = this;

                if (!this._transitionInPromise ) {
                    this.transitioning = true;
                    this._transitionInPromise = this.transitionIn();
                    this._transitionInPromise.done(function () {
                        self.transitioning = false;
                        self._transitionInPromise = null;
                    });
                }
                
                return this._transitionInPromise || resolved();
            },
            /** 
             * @see doTransitionOut
             * 
             * @return Promise
             */
            doTransitionOut: function () {
                var self = this;

                if (!this._transitionOutPromise) {
                    this.transitioning = true;
                    this._transitionOutPromise = this.transitionOut();
                    this._transitionOutPromise.done(function () {
                        self.transitioning = false;
                        self._transitionOutPromise = null;
                    });
                }

                return this._transitionOutPromise || resolved();
                
            },
            /**
             * Default transition in, just a fade in
             * @return Promise
             */
            transitionIn: function () {
                var transition = new $.Deferred();
                
                this.$el.clearQueue().fadeIn(400, function () {
                    transition.resolve();
                });

                return transition.promise();
            },
            /**
             * Default transition out, just a fade out
             * @return Promise
             */
            transitionOut: function () {
                var transition = new $.Deferred();
                this.$el.fadeOut(400, function () {
                    transition.resolve();
                });

                return transition.promise();
            }
        });
    }
);