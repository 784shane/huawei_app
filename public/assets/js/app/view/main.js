define(
    [
        'module',
        'backbone',
        'app/router/router',
        'app/transitions/transitions',
        'app/view/page/edit/crop-tool',
        'app/service/facebook',
        'app/control/app-control',
        'app/service/local-photos',
        'app/service/instagram',
        'app/view/page/edit/mobile_pinching'
    ],

    function (module, Backbone,  router, 
        transitions, cropTool, Facebook, 
        Control, LocalPhotos, Instagram, Pinching) {

        /*console.log({
            name: module.config().shareTitle,
            link: module.config().redirectUrl,
            description: module.config().shareDescription
        });*/

        var textReady = false;

        var share = function(device)
        {
            Backbone.trigger('app:pageview', 'share/facebook');

            var pictureURL = $('#cloned_img').attr('src');

            var attrs = {
                name: module.config().shareTitle,
                link: module.config().redirectUrl,
                picture: pictureURL,
                description: module.config().shareDescription

            };

            Facebook.share(attrs);
        };

        var getInstagramPhotos = function()
        {
            Instagram.LoadMobilePhotos('mobile')
                        .done(function(){
                            //Now we will open our photos
                            transitions.open_friends();
                        });
        };


        
        //var MainView = AbstractParentView.extend({
        var MainView = Backbone.View.extend({
            events: {

                'click .thank_you_button': function () {
                    Backbone.trigger('app:pageview', 'action/learnmore');
                },

                'click .m_thank_2nd_lmore_button': function () {
                    Backbone.trigger('app:pageview', 'action/learnmore');
                },
                
                'click .m_thank_share_area_icons_left': function () {
                    var device = "mobile";
                    share(device);
                },

                'click #share_fb': function () {

                    var device = "web";
                    share(device);
                },

                'click #share_tw': function () {

                    var imageShortUrl = Control.get_image_short_url();
                    var textToTweet = 'I entered the Huawei Ultimate Experience Competition ' + imageShortUrl;
                    var url = 'https://twitter.com/intent/tweet?text=' + textToTweet;
                    var windowname = '_blank';
                    var settings = 'width=405,height=255,top=500,left=500';

                    window.open(url, windowname,  settings);

                    Backbone.trigger('app:pageview', 'share/twitter');
                },

                'click #share_tw_m': function () {

                    var imageShortUrl = Control.get_image_short_url();
                    var textToTweet = 'I entered the Huawei Ultimate Experience Competition ' + imageShortUrl;
                    var url = 'https://twitter.com/intent/tweet?text=' + textToTweet;
                    var windowname = '_blank';
                    var settings = 'width=405,height=255,top=500,left=500';

                    window.open(url, windowname,  settings);
                    Backbone.trigger('app:pageview', 'share/twitter');
                },

                'click #receive_emails_check': function () {
                    Control.toggle_recieve_email_updates();
                },

                'click #create_yours_button': function () {
                        
                        Facebook.allow(['email','publish_actions']).done(function () {
                        
                            transitions.theStop();

                            Facebook.getMainPhoto([],'mainPhoto');
                            //we will stop and reset the 
                            //photo scolling here
                            Control.stop_front_scroller();
                            transitions.fade_out_page_0();
                            
                            Backbone.trigger('app:pageview', 'action/start');

                            $('.page1_pocket img').attr('src', '/assets/graphics/start_bg.jpg');

                        }).fail(function () {
                            
                        });
                },


                'click #create_yours_button_m': function () {

                        transitions.theStop();

                        //getLoginStatus
                        FB.getLoginStatus(function (response) {
                            if (response.authResponse) {

                                Facebook.getMainPhoto([],'mainPhoto');

                                Control.actions_upon_start();
                                Backbone.trigger('app:pageview', 'action/start');


                            } else {
                                //defer.reject();

                                var scope = module.config().scope;
                                Facebook.login([scope]).done(function () 
                                {

                                        Facebook.getMainPhoto([],'mainPhoto');

                                        Control.actions_upon_start();
                                        Backbone.trigger('app:pageview', 'action/start');
                                });


                            }
                        });

                },


                'click #testbutton': function () {
                    //cropTool.load('assets/graphics/trash/friend_square.png');
                    cropTool.load('http://images5.alphacoders.com/335/335544.jpg');
                    //transitions.fade_out_page_0();
                },

                'click #front_page_check': function () {

                    //Mark the checkbox as accepting terms error off
                    Control.toggle_error_mark('tacs',true);
                    
                    transitions.toggle_tac_check();

                },

                'click #friends_close':function() {
                    transitions.close_friends(false);
                },

                // Login with instagram and get images
                'click #ist_source_button': function () {

                    Control.set_source_image_provider('instagram');

                    $('#friends_box .friend_title_box #friendbox_title').html('Instagram Images');
                    Backbone.trigger('app:pageview', 'sourceimage/instagram');
                    
                    Instagram.loadPhotos().done(function (obj) 
                    {    
                    });
                },

                // Login with instagram and get images
                'click #m_insta_source': function () {

                    Control.set_source_image_provider('instagram');

                    Backbone.trigger('app:pageview', 'sourceimage/instagram');

                    if(instagram_access_token==""){
                        Instagram.mobileLogin();
                    }else{
                        getInstagramPhotos();
                    }

                },

                // Login with instagram and get images
                'click #instagram': function () {

                    Control.set_source_image_provider('instagram');

                    $('#friendbox_title').html('Instagram Images');

                    Backbone.trigger('app:pageview', 'sourceimage/instagram');
                    
                    Instagram.loadPhotos().done(function () {
                        // After successfully fetching instagram images, go to select image route
                        Backbone.history.navigate('edit/select-image', true);
                    });
                },

                'click #hw_source_button': function () {

                    Control.set_source_image_provider('stock');

                    LocalPhotos.get_local_photos();

                    Backbone.trigger('app:pageview', 'sourceimage/ourlibrary');

                },

                'click #fb_source_button': function () {

                    Control.set_source_image_provider('facebook');

                    Backbone.trigger('app:pageview', 'sourceimage/facebook');

                    FB.login(function(response) {
                        if (response.authResponse) {
                            Facebook.loadPhotos('web');
                        }
                        else {
                             
                        }
                    }, {scope: 'user_photos'});

                },

                'click #m_fb_source': function () {

                    Control.set_source_image_provider('facebook');

                    Backbone.trigger('app:pageview', 'sourceimage/facebook');

                    FB.login(function(response) {
                        if (response.authResponse) {
                            Facebook.loadPhotos('mobile');
                        }
                        else {
                            
                        }
                    }, {scope: 'user_photos'});

                },

                'click #m_lib_source': function () {

                    Control.set_source_image_provider('stock');

                    Backbone.trigger('app:pageview', 'sourceimage/ourlibrary');

                    LocalPhotos.get_local_shots().done(function(data){
                        Control.load_home_pictures(data,'mobile');
                    });
                },

                'click #ctas_pic':function() {

                    //We have a very im[portant thing to do here
                    //because this was animating, we must first stop
                    //the animation
                    Control.stop_cta_blinking();

                    var me = Facebook.getMeObject();

                    if(me && me.email)
                    {
                        $('#user_email_address').val(me.email);
                    }
                    //alert(Control.has_user_accepted_terms());
                    transitions.show_pic_buttons(0);
                    //$('.pic_source_block').show();
                },


                // Login with facebook and request default permissions
                'click #login': function () {

                    // Default scope (comes from the config file)
                    var scope = module.config().scope;
                    
                    Facebook.login([scope]).done(function () {
                        // After successful login, go to edit route
                        //Backbone.history.navigate('edit', true);
                    });
                },


                'click #facebook-photos': function () {
                    Facebook.loadPhotos().done(function () {
                       // After successfully fetching facebook images, go to select image route
                        //Backbone.history.navigate('edit/select-image', true);
                    });
                },

                // Login with instagram and get images
                'click .selected_image_web': function (ev) {

                    var imageProvider = Control.get_source_image_provider();


                    if(imageProvider == 'stock')
                    {
                        Control.set_source_image_id($(ev.currentTarget).attr('data-id'));
                    }

                    var bigImageUrl = $(ev.currentTarget).data('big');
                    cropTool.load(bigImageUrl);
                    transitions.close_friends();

                    //Load Appropriate Top Bar
                    Control.display_top_bar('move_and_scale_bar');

                    transitions.toggle_crop_tool('on', 'web', 'forward');
                    
                },

                // Login with instagram and get images
                'click .selected_image_mob': function (ev) {

                    var imageProvider = Control.get_source_image_provider();

                    if(imageProvider == 'stock')
                    {
                        Control.set_source_image_id($(ev.currentTarget).attr('data-id'));
                    }

                    var bigImageUrl = $(ev.currentTarget).data('big');

                    //Load Appropriate Top Bar
                    Control.display_top_bar('mobile_move_and_scale_bar');
                    
                    Control.display_tag_friend_area('finish_cropping');
                    
                    Pinching.load(bigImageUrl);

                    Control.adjust_viewport();
                    
                    transitions.close_friends();

                    $('.m_source_buttons').hide();

                    //Load Appropriate Top Bar
                    //Control.display_top_bar('move_and_scale_bar');

                    //transitions.toggle_crop_tool('on','mobile', 'forward');

                    $('.pinching_absolute').css('display','block');
                    
                },

                // Login with instagram and get images
                'click #strip_finish_button_m': function () {

                    //remove the glow area
                    $('#cta_action_glow_area_m').css('display','none');
                    //make the write_here_block visible again
                    //so that the text can blink.
                    $('#write_here_block').css('display','block');
                    $('.photo_cta_area').css('display','block');

                    $('#turning_back_block').css('display','none');
                    $('#finish_typing_button_m').css('display','block');
                    //close photo chooser and display photo
                    Control.apply_chosen_photo('mobile');
                    Backbone.trigger('app:pageview', 'experience/cropandresize');
                },

                // Login with instagram and get images
                'click #strip_finish_button': function () {
                    //close photo chooser and display photo
                    Control.apply_chosen_photo('web');
                    Backbone.trigger('app:pageview', 'experience/cropandresize');
                },

                // Login with instagram and get images
                'click #finish_typing_button': function () 
                {
                    var text = $('#entry_text').val();
                    
                    var textWithoutSpaces = text.replace(/[^A-Z]/gi, "").length;
                    if(textWithoutSpaces > 5){

                        data = {text: text};

                        $.post( "user/validatetext",data,
                        function( response ) 
                        {

                            if(response.status == 'ok')
                            {
                                Control.gettextarea_ready('web');
                                transitions.prepare_friend_tagging('web');
                                $('#entry_text').attr('readonly','readonly');
                                $('.textWordCount').fadeOut();
                                Backbone.trigger('app:pageview', 'experience/write');
                            }
                            else
                            {
                                $('#entry_text').val(response.status);
                                Backbone.trigger('app:pageview', 'experience/badword');   
                            }
                        });

                    }else{

                        textReady = false;
                        transitions.toggle_typeNowPopup('web', 'on');

                    }


                },

                // Login with instagram and get images
                'click #finish_typing_button_m': function () {

                    var text = $('#cta_exp_extry').val();
                    var textWithoutSpaces = text.replace(/[^A-Z]/gi, "").length;
                    if(textWithoutSpaces > 5){

                    data = {text: text};

                    $.post( "user/validatetext",data,
                    function( response ) 
                    {

                        if(response.status == 'ok')
                        {
                            //$('#strip_finish_button_m').css('display','none');
                            $('#finish_typing_button_m').css('display','none');
                            $('.tag_button_area').css('display','none');

                            //just in case the test box was still blinking, 
                            //we should stop it and reset
                            Control.gettextarea_ready('mobile');

                            transitions.prepare_friend_tagging('mobile');
                            $('#cta_exp_extry').attr('readonly','readonly');
                            $('.textWordCount_m').fadeOut();
                            Backbone.trigger('app:pageview', 'experience/write');
                        }
                        else
                        {
                            $('#cta_exp_extry').val(response.status);
                            Backbone.trigger('app:pageview', 'experience/badword');
                            
                        }
                    });

                    }else{

                        textReady = false;
                        transitions.toggle_typeNowPopup('mobile', 'on');

                    }
                    
                },

                // Login with instagram and get images
                'click #bring_no_friend_button': function () {
                    $('.no_thanks_button_area').hide();
                    $('#add_f_button').stop();
                    $('#add_f_button').animate({opacity:1});
                    //Load Appropriate Top Bar
                    Control.display_top_bar('finish_entry_bar');

                    Backbone.trigger('app:pageview', 'friends/nothanks');
                    //send stuff to the server
                    //Control.save_photo_details();
                },

                // Login with instagram and get images
                'click #bring_no_friend_button_2nd': function () {

                    $('.tag_button_area').show();
                    $('#finish_creating_button_m').show();
                    $('.no_thanks_button_area').hide();
                    $('.tag_friend_area_block').hide();
                    $('#add_f_button').stop();
                    $('#add_f_button').animate({opacity:1});

                    //Load Appropriate Top Bar
                    Control.display_top_bar('mobile_bar_finish_creating');

                    Backbone.trigger('app:pageview', 'friends/nothanks');
                    //send stuff to the server
                    //Control.save_photo_details();
                },

                // Login with instagram and get images
                'click #finish_creating_button': function () 
                {
                    //show loader
                    if(Control.has_user_accepted_terms())
                    {
                        var me = Facebook.getMeObject();
                        
                        Control.startLoader();

                        $('.add_picture_strip').css('display','none');
                        
                        //send stuff to the server
                        Control.save_photo_details('web',me.accessToken);

                        $('body').css('overflow', 'hidden');
                        $('body').css('height', '770px');
                        $('.page_background_tint').css('opacity', '0.7');
                       Backbone.trigger('app:pageview', 'experience/finish');
                    }
                    else
                    {
                        Control.toggle_error_mark('tacs',false);
                    }
                },

                // Login with instagram and get images
                'click #finish_creating_button_m': function () {

                    //show loader
                    if(Control.has_user_accepted_terms())
                    {
                        var me = Facebook.getMeObject();
                        
                        Control.startLoader();

                        $('#finish_creating_button_m').css('display','none')

                        $('.add_picture_strip').css('display','none');
                        //send stuff to the server

                        Control.save_photo_details('mobile',me.accessToken);

                        Backbone.trigger('app:pageview', 'experience/finish');
                    }
                    else
                    {
                        Control.toggle_error_mark('tacs',false);
                    }
                },

                // Login with instagram and get images
                'click #tag_friend_button': function () {

                    device = "web";

                    $('#add_f_button').stop();
                    $('#add_f_button').animate({opacity:1});

                    //transitions.move_phone_for_thanks();
                    Facebook.get_friends(device);
                    //get_friends

                },

                // Login with instagram and get images
                'click #tag_friend_button_m': function () {

                    device = 'mobile';

                    $('#add_f_button').stop();
                    $('#add_f_button').animate({opacity:1});
                    
                    //transitions.move_phone_for_thanks();
                    Facebook.get_friends(device);
                    //get_friends

                },

                // Login with instagram and get images
                'click .friend_block_m': function (ev) {

                    $('.no_thanks_button_area').css('display','none');

                    //keep user to be tagged
                    var chosenTagId = $(ev.currentTarget).data('id');
                    Control.placeTaggedFriendPhoto(chosenTagId);

                    $('.tag_button_area').show();
                    $('#finish_creating_button_m').show();

                    //Load Appropriate Top Bar
                    Control.display_top_bar('mobile_bar_finish_creating');

                    //close friend list
                    transitions.toggle_friend_list_box('off');

                },

                // Login with instagram and get images
                'click .friend_block': function (ev) {

                    $('.tag_button_area').hide();
                    $('.no_thanks_button_area').hide();
                    $('#add_f_button').stop();
                    $('#add_f_button').animate({opacity:1});

                    //Load Appropriate Top Bar
                    Control.display_top_bar('finish_entry_bar');

                    //keep user to be tagged
                    var chosenTagId = $(ev.currentTarget).data('id');
                    Control.placeTaggedFriendPhoto(chosenTagId);

                    //close friend list
                    transitions.toggle_friend_list_box('off');

                },

                // Login with instagram and get images
                'click .friend_list_head_top i': function (ev) {

                    //close friend list
                    transitions.toggle_friend_list_box('off');

                },

                // Login with instagram and get images
                'keyup #friend_finder_input': function (ev) {
                    Control.check_friendlist_input(device);

                },

                // Login with instagram and get images
                /*'focus .pic_display_ctas_textarea_input': function () {
                    Control.gettextarea_ready('web');
                },*/

                'click .pic_display_ctas_textarea_input': function () {

                    Control.gettextarea_ready('web');
                    transitions.toggle_typeNowPopup('web','off');

                },

                'keyup .pic_display_ctas_textarea_input': function () {

                    var count = 100 - $('#entry_text').val().length;
                    $('.textWordCount').html(count);
                    if(!textReady){

                        textReady = true;
                        Control.gettextarea_ready('web');
                        transitions.toggle_typeNowPopup('web','off');
                        
                    }

                },

                'keyup #cta_exp_extry': function () {
                    var count = 100 - $('#cta_exp_extry').val().length;
                    $('.textWordCount_m').html(count);
                },
                // Login with instagram and get images
                'click #cta_exp_extry': function () {
                    Control.gettextarea_ready('mobile');
                    transitions.toggle_typeNowPopup('mobile','off');
                },

                // Login with instagram and get images
                'focus #cta_exp_extry': function () {
                    Control.gettextarea_ready('mobile');
                    transitions.toggle_typeNowPopup('mobile','off');
                },

                // Login with instagram and get images
                'click #terms_link': function () {
                    $('.popups').show();
                    $('.tacs_box').show();
                },

                'click #tacs_close': function () {
                    $('.popups').hide();
                    $('.tacs_box').hide();
                },

                'click .m_thank_email_right': function () {
                    Control.toggle_recieve_email_updates_mobile();
                },

                'click #friend_finder_input': function () {
                    $('#friend_finder_input').val(''); 
                },

                'click .pic_display_ctas_textarea_input_ie': function () 
                {
                    $('.pic_display_ctas_textarea_input').val('');
                },

                'click #m_cta_pic_glow': function () {

                    transitions.stop_blinking();
                    var me = Facebook.getMeObject();

                    if(me && me.email)
                    {
                        $('#email_entry').val(me.email);
                    }

                    Control.show_source_button('mobile');

                },

                'click .pic_display_ctas_textarea_input_ie': function () 
                {   
                    if(Control.getIEclickedOnTextArea() == 0)
                    {
                        $('.pic_display_ctas_textarea_input').val('');
                        Control.setIEclickedOnTextArea(1);
                    }
                },

                'blur .pic_display_ctas_textarea_input_ie': function () 
                {
                    if($('.pic_display_ctas_textarea_input_ie').val().length > 90) 
                    { 
                        var text = $('.pic_display_ctas_textarea_input_ie').val().substr(0, 90);
                        $('.pic_display_ctas_textarea_input_ie').val(text);
                        return false; 
                    } 
                },

                'keyup .pic_display_ctas_textarea_input_ie': function () 
                {
                    var count = 100 - $('#entry_text').val().length;
                    $('.textWordCount').html(count);

                    if($('.pic_display_ctas_textarea_input_ie').val().length > 90) 
                    { 
                        var text = $('.pic_display_ctas_textarea_input_ie').val().substr(0, 90);
                        $('.pic_display_ctas_textarea_input_ie').val(text); 
                        return false; 
                    }
                },

                'click #return_button': function () {

                    $('#turning_back_block').css('display','none');
                    
                    Control.prepare_back_to_source('mobile');
                    Control.show_source_button('mobile');

                },

                'click #return_w': function () {

                    transitions.toggle_crop_tool('off','web', 'back');

                    //remove the images
                    $('.image').attr('src','');
                    $('#backdrop_mask_img').attr('src','');

                    Control.stop_cta_blinking();

                    //alert(Control.has_user_accepted_terms());
                    transitions.show_pic_buttons(0);



                }

                // 'focus #email_entry': function () {
                //     if($('#email_entry').val()=="Enter a valid email"){
                //         $('#email_entry').val('');
                //         $('#email_entry').removeClass('error');
                //     }
                // },

                // 'focus #user_email_address': function () {
                //     if($('#user_email_address').val()=="Enter a valid email"){
                //         $('#user_email_address').val('');
                //         $('#user_email_address').removeClass('error');
                //     }
                // }










            },

            
            children: {
               /*'start': startView,
               'edit': editView,
               'thanks': thanksView*/
            },

            initialize: function () {
                var self = this;
                

                router.on({
                    /*'route:start route:terms-conditions': function () {
                        self.showChild('start');
                    },
                    'route:select-source route:select-image route:crop-image route:write route:preview': function () {
                        self.showChild('edit');
                    },
                    'route:thanks': function () {
                        self.showChild('thanks');
                    }*/

                    'route:app getPost': function () {

                    },

                    'route:defaultRoute': function () {

                    }

                });


                Backbone.on({
                    'app:load:start': function (name) {

                    },

                    'attemptGettingFbPhotos': function (name) {

                        Facebook.loadPhotos();
                    },

                    'getPhotosNow': function (device) {
                        
                        Facebook.loadPhotosAfterPerms().done(function(res)
                        {
                            Control.load_facebook_pictures(res,device);
                        });
                    },

                    'showPopup': function (name) {
                        switch(name){
                            case "photos":{
                                transitions.open_friends();
                                break;
                            }

                            case "":{
                                break;
                            }
                        }
                    },

                    'app:load:error': function (name, message) {
                        //self.unlisten(name);
                        //Backbone.trigger('app:error', 'Error loading ' + (message || name));
                    },

                    'closeCropping': function (device) {
                        transitions.toggle_crop_tool('off',device, 'forward');
                    },

                    'showAppropriateBar': function (name) {
                        //Load Appropriate Top Bar
                        Control.display_top_bar(name);
                    },

                    'mainPhoto': function () {

                        Control.loadMainPhoto(Facebook.getMeObject().userID);
                        
                    },

                    'friends_retrieved': function (friends,device) {
                        Control.showFriendBox(friends,device);
                        $('#friend_finder_input').val('Search Facebook Friends');
                        transitions.toggle_friend_list_box('on');
                    },

                    'view_final_screen': function () {

                        Control.stopLoader();
                        //whether user email is gotten or not we need to show
                        //the user the next screen.
                        transitions.move_phone_for_thanks();

                    },

                    'send_email_updates': function (device) {

                        Control.send_email_updates(device, {recieveemail: 1});

                    },

                    'blink_m_photo_call_to_action': function () {
                        //transitions.pulsate('pulsing_m','up');
                        //transitions.grow_elem('down',1);
                        //transitions.get_ready_to_grow();
                    }
                    

                });


                
                Facebook.init();

                /*$(window).on('resize', function () {
                    Backbone.trigger('app:resize');
                }).trigger('resize');*/

                /*$(window).on('resize', function () {
                    Backbone.trigger('app:resize');
                }).trigger('resize');*/

                window.fbAsyncInit = function() {
                    
                };

                window.onresize = function() {

                    $('#mn_hdr').css('width','100%');
                };


                if(instagram_logged_in==1){
                    //this means that we have instagram, we will
                    //now load so we can now get our instagram pictures.
                    getInstagramPhotos();
                }else{
                    transitions.test();
                }

            }
        });


        return new MainView({
            el: '#mainApp'
        });
        
    }
);







/*define(
    [
        'module',
        'backbone',
        'app/router/router',
        'app/service/facebook',
        'app/view/abstract/parent',
        'app/view/component/loading',
        'app/view/page/start',
        'app/view/page/edit',
        'app/view/page/thanks',
        'app/view/page/example'
    ],

    function (module, Backbone, router, AbstractParentView, loadingView, startView, editView, thanksView, exampleView) {


        var MainView = AbstractParentView.extend({
            events: {},

            
            children: {
               'start': startView,
               'edit': editView,
               'thanks': thanksView
            },
            initialize: function () {
                var self = this;

                router.on({
                    'route:start route:terms-conditions': function () {
                        self.showChild('start');
                    },
                    'route:select-source route:select-image route:crop-image route:write route:preview': function () {
                        self.showChild('edit');
                    },
                    'route:thanks': function () {
                        self.showChild('thanks');
                    }
                });

                $(window).on('resize', function () {
                    Backbone.trigger('app:resize');
                }).trigger('resize');
            }
        });

        return new MainView({
            el: 'body'
        });
        
    }
);







define(
    [
        'module',
        'backbone',
        'app/model/experience'
    ],
    function (module, Backbone, Ex) {

      var experience = new Experience();
      experience.set({
                'id': null,
                'text': '',
                'source_image_provider': null,
                'source_image_id': null,
                'source_image_url': null,
                'crop_x': null,
                'crop_y': null,
                'crop_width': null,
                'crop_height': null,
                'url': null,
                'shortUrl': null
            });
      
      experience.save().done(function(){

        

      })
    }
);




*/