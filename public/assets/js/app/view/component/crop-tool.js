define(
    [
        'module',
        'backbone',
        'jquery',
        'jquery-ui'
    ],
    function (module, Backbone, jQuery, ui) {
    
        var $cached,
            $box,
            $img;

        // $mask
        var getResponsiveScale = function () {
            // This value should come from CSS
            return 1;
        };
        
        var constrain = function () {
            // Constarin image to maximum size
        };

        var center = function () {
            // Center image inside mask
        };

        var preload = function (src) {
            var defer = new $.Deferred();

            $cached.on('load', function () {
                defer.resolve();
            }).attr('src', src);

            if ($cached[0].loaded) {
                defer.resolve();
            }

            return defer.promise().done(function () {
                $img.attr('src', $cached.attr('src'));
            });
        };

        var setup = function () {
            var iw = $cached.width(),
                ih = $cached.height(),
                ratio = iw / ih;

            $box.resizable({
                aspectRatio: ratio,
                handles: 'ne, se, sw, nw',
                maxWidth: iw * module.config().maxScale,
                maxHeight: ih * module.config().maxScale,
                minWidth: module.config().minWidth,
                minHeight: module.config().minHeight
            });

            $box.draggable();
        };

        var CropTool = Backbone.View.extend({
            events: {},
            initialize: function () {
                $cached = this.$('#cache-img');
                $img = this.$('img.image');
                $box = this.$('.box');

                $box.on({
                    'drag': function (ev, ui) {
                        if (self.$el.is('.active')) {
                            $img.css(ui.position);
                        }
                    },
                    'resize': function (ev, ui) {
                        if (self.$el.is('.active')) {
                            $img.css(ui.size);
                        }
                    },
                    'create': function (ev, ui) {
                        $img.css(ui.size).css(ui.position);
                    }
                });
            },
            load: function (src) {
                var self = this;

                preload(src).done(function () {
                    constrain();
                    center();
                    setup();
                    self.focus();
                });
            },
            focus: function () {
                // Show resize UI
                this.$el.addClass('active');
            },
            blur: function () {
                // Hide resize UI
                this.$el.removeClass('active');
            },
            val: function () {
                var position = $img.position(),
                    rs = getResponsiveScale();
                    
                // scale = original.width / $img.width();

                return {
                    'x': position.left * rs,
                    'y': position.top * rs,
                    'width': $img.width() * rs,
                    'height': $img.height() * rs
                };
            }
        });
});