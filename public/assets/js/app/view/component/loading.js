define(['backbone'],
    function (Backbone) {
        
        var timeout,
            events = [];

        var LoadingView = Backbone.View.extend({
            initialize: function () {
                var self = this;

                Backbone.on({
                    'app:load:start': function (name) {
                        self.listen(name);
                    },
                    'app:load:complete': function (name) {
                        self.unlisten(name);
                    },
                    'app:load:error': function (name, message) {
                        self.unlisten(name);
                        Backbone.trigger('app:error', 'Error loading ' + (message || name));
                    }
                });
            },
            listen: function (event) {
                if (events.length === 0) {
                    this.show();
                }
                events.push(event);
            },
            unlisten: function (event) {
                events.splice(_.indexOf(events, event), 1);
                if (events.length === 0) {
                    this.hide();
                }
            },
            show: function () {
                var self = this;
                clearTimeout(timeout);
                
                timeout = setTimeout(function () {
                    self.$el.clearQueue().fadeIn(1000, 'easeInOutExpo');
                }, 1600);
            },
            hide: function () {
                var self = this;
                clearTimeout(timeout);

                this.$el.clearQueue().fadeOut(400);
            }
        });

        return new LoadingView({
            el: '#loader'
        });
    }
);