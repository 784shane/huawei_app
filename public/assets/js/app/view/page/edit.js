define([
        'backbone',
        'app/router/router',
        'app/view/abstract/parent',
        'app/view/page/edit/friends-tagger',
        'app/view/page/edit/step-info',
        'app/view/page/edit/start',
        'app/view/page/edit/image-sources',
        'app/view/page/edit/experience',
        'app/view/page/terms-conditions'
    ], function (Backbone, router, AbstractParentView, friendsTaggerView, stepInfoView, startView, imageSourcesView, experienceView) {

        var EditView = AbstractParentView.extend({
            events: {},
            children: {
                'start': startView,
                'sources': imageSourcesView,
                'experience': experienceView
            },
            initialize: function () {
                var self = this;

                router.on({
                    'route:edit': function () {
                        // Show centered button
                        self.showChild('start');
                    },
                    'route:select-source': function () {
                        // Show sources buttons
                        self.showChild('sources');
                    },
                    'route:crop-image': function () {
                        // Show experience
                        self.showChild('experience');
                    }
                });
            }
        });

        return new EditView({
            el: '#edit'
        });
    }
);