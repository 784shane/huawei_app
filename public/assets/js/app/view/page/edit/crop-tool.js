define(
    [
        'module',
        'backbone',
        'jquery',
        'jquery-ui',
        'hammer'//,
        //'app/view/page/edit/jquery.ui.touch-punch.min'
    ],
    function (module, Backbone, jQuery, ui, hammer) {
    
        var $cached,
            $box,
            $image,
            $mask,
            initialized = false,
            isTouch = $('html').hasClass('touch');


        var mask_offLeft = 0,
            mask_offTop = 0,
            mask_offLeftDir = "left",
            mask_offTopDir = "up";

        var left_constrain = 0,
            upper_leftConstrain = 0,
            topConstrain = 0,
            lower_topConstrain = 0;


        var touchHandler = function(event)
        {

            var touch = event.changedTouches[0];

            var simulatedEvent = document.createEvent("MouseEvent");
                simulatedEvent.initMouseEvent({
                touchstart: "mousedown",
                touchmove: "mousemove",
                touchend: "mouseup"
            }[event.type], true, true, window, 1,
                touch.screenX, touch.screenY,
                touch.clientX, touch.clientY, false,
                false, false, false, 0, null);

            touch.target.dispatchEvent(simulatedEvent);
            event.preventDefault();
        }

        touch_init = function()
        {
            var self = this;
            var box = $box;
            //touchHandler();


            document.addEventListener("touchstart", touchHandler, true);
            document.addEventListener("touchmove", touchHandler, true);
            document.addEventListener("touchend", touchHandler, true);
            document.addEventListener("touchcancel", touchHandler, true);

            /*box.addEventListener("touchstart", self.touchHandler, true);
            box.addEventListener("touchmove", self.touchHandler, true);
            box.addEventListener("touchend", self.touchHandler, true);
            box.addEventListener("touchcancel", self.touchHandler, true);*/
        }
        
        var constrain = function () {
            // Constrain image to fill crop area and go outside if possible
            var css, width, height, ratio,
                maxResample = module.config().maxResample,
                initialScale = (module.config().maxResample - 1) / 2 + 1,
                initialWidth = responsive(module.config().finalWidth * initialScale),
                initialHeight = responsive(module.config().finalHeight * initialScale);
            
            $image.css({
                'width': '',
                'height': ''
            });

            width = responsive($image.width());
            height = responsive($image.height());
            ratio = $image.width() / $image.height();

            if (width * maxResample < initialWidth || height * maxResample < initialHeight) {
                // Image is not big enough to be scaled that much
                initialWidth = width * maxResample;
                initialHeight = height * maxResample;
            }

            if (ratio >= initialWidth / initialHeight) {
                // Image is wider, we use the hieght because we want to cover all
                css = {
                    'width': initialHeight * ratio,
                    'height': initialHeight
                };
            } else {
                css = {
                    'width': initialWidth,
                    'height': initialWidth / ratio
                };
            }

            $box.css(css);
            $image.css(css);
        };

        var center = function () {
            // Center image inside mask
            var css = {
                left: ($mask.width() - $image.width()) / 2,
                top: ($mask.height() - $image.height()) / 2
            };

            $box.css(css);
            $image.css(css);
        };

        var showCurrent = function () {
            var defer = new $.Deferred();
            $image.fadeIn(200, function () {
                defer.resolve();
            });
            return defer.promise();
        };

        var hideCurrent = function () {
            var defer = new $.Deferred();
            if (initialized) {
                $image.fadeOut(200, function () {
                    $image.css({
                        'width': '',
                        'height': ''
                    });

                    defer.resolve();
                });
            } else {
                defer.resolve();
            }
            return defer.promise();
        };

        var preload = function (src) {
            var defer = new $.Deferred();

            $cached.on('load', function () {
                defer.resolve();
            }).attr('src', src);

            if ($cached[0].loaded) {
                defer.resolve();
            }

            return defer.promise().then(function () {
                $image.attr('src', $cached.attr('src')).css('visibility', 'hidden');
                $cached.attr('src', '');
            });
        };

        var getResizableConfig = function () {
            var iw, ih, ratio,
                maxResample = module.config().maxResample,
                finalWidth = module.config().finalWidth,
                finalHeight = module.config().finalHeight;
            
            $image.css({
                'width': 'auto',
                'height': 'auto'
            });

            iw = $image.width();
            ih = $image.height();
            ratio = iw / ih;

            
            // @bug, resizable fails with ne nw handles if both limits (width and height) are set
            var conf = {
                aspectRatio: ratio,
                handles: 'ne, se, sw, nw',
                maxWidth: 6000,//responsive(iw) * maxResample,
                minHeight: $('#backdrop_mask').height(),
                minWidth: $('#backdrop_mask').width()//responsive(finalWidth), 
            };

            return conf;
        };

        var check_box_position = function(boxOffset)
        {
            /*this.left_constrain = offset.left;
            this.upper_leftConstrain = left_constrain + bmWidth;
            this.topConstrain = offset.top;
            this.lower_topConstrain = topConstrain + bmWidth;*/

            var boxOffset = $('#box_containement').offset();


            if(boxOffset.left==left_constrain||
                boxOffset.left>left_constrain)
            {

                //$('.box').css('left',left_constrain+'px');
                //return false;
            }
        };

        var setupMouse = function () {
            var self = this,
                config = getResizableConfig();

            if (initialized) {
                $box.resizable('destroy');
            }

            // Draggable and resizable
            $box.resizable(config).draggable(/*{ containment: '.image_container'//[0, 0, 200, 0]
                //opacity:1, 
                //distance:2000,
                //cursor: "crosshair" 
            }*/).on({
                'create resize drag': function (ev, ui) {
                    //if (tool.$el.is('.active')) {

                        
                    
                        if (typeof(ui)=="object"&&ui.position != null) {

                            if(check_box_position())
                            {

                            }

                        

                        var l = $image.position().left;
                        var t = $image.position().top;


                            $image.css(ui.position);


                        var bgIPos = $('#backdrop_mask_img').position();
                        var l1 = $image.position().left;
                        var t1 = $image.position().top;


                        if(l1>l){

                            ldiff = l1-l;
                            $('#backdrop_mask_img')
                                    .css({left:(
                                        (bgIPos.left)+ldiff
                                    )+"px"}); 

                        }else{

                            ldiff = l-l1;
                            $('#backdrop_mask_img')
                                    .css({left:(
                                        (bgIPos.left)-ldiff
                                    )+"px"}); 

                        }


                        if(t1>t){

                            tdiff = t1-t;
                            $('#backdrop_mask_img')
                                    .css({top:(
                                        (bgIPos.top)+tdiff
                                    )+"px"}); 

                        }else{

                            tdiff = t-t1;
                            $('#backdrop_mask_img')
                                    .css({top:(
                                        (bgIPos.top)-tdiff
                                    )+"px"}); 

                        }



                            $('#image_container_backdrop')
                            .css(ui.position);

                        

                            
                        }

                        if (typeof(ui)=="object" && ui.size != null) {
                            $image.css(ui.size);
                            $('#image_container_backdrop')
                            .css(ui.size);

                            $('#backdrop_mask_img')
                            .css(ui.size);

                            //$('#box_containement')
                            //.css(ui.size);
                        }

                        //tool.trigger('change', tool.val());
                    //}

                    

                    
                }
            });

            initialized = true;
        };

        var setupTouch = function () {

            
            var pinching = false,
                config = getResizableConfig(),
                previous = {
                    scale: 1,
                    x: 0,
                    y: 0
                };
            
            // @todo this is still in progress
            // @todo resize only within limits provided in config
            // @todo set pointer-events to none
            // @todo container should take full width and height
            $box.hammer({
                'prevent_defaults': true
            }).on({
                'touchstart': function (ev) {
                    ev.gesture.preventDefault();
                },
                'pinchin pinchout': function (ev) {
                    var s = ev.gesture.scale / previous.scale;
                    pinching = true;
                    previous.scale = ev.gesture.scale;

                    var css = {
                        'width': $image.width() * s,
                        'height': $image.height() * s
                    };
                
                    $image.css({
                        'width': $image.width() * s,
                        'height': $image.height() * s
                    });


                    //$image.css(ui.size);
                    $('#image_container_backdrop')
                    .css(css);

                    $('#backdrop_mask_img')
                    .css(css);

                    /*$('.box')
                    .css(css);*/
                },
                'release': function (ev) {
                    pinching = false;

                    $box.css({
                        'width': $image.width(),
                        'height': $image.height()
                    });


                },
                'dragstart': function (ev) {
                    ev.gesture.preventDefault();
                    //$('#logo').html('hahah');
                    //previous.x = ev.gesture.deltaX;
                    //previous.y = ev.deltaY;


                    previous.x = ev.gesture.deltaX;
                    previous.y = ev.gesture.deltaY;
                },
                'drag': function (ev) {

                    ev.gesture.preventDefault();
                    
                    //var difx = ev.deltaX - previous.x,
                    //dify = ev.deltaY - previous.y;

                    var difx = ev.gesture.deltaX - previous.x,
                        dify = ev.gesture.deltaY - previous.y;

                    //previous.x = ev.deltaX;
                    //previous.y = ev.deltaY;

                    previous.x = ev.gesture.deltaX;
                    previous.y = ev.gesture.deltaY;

                    var l = $image.position().left;
                    var t = $image.position().top;

                    $image.css({
                        'left': $image.position().left + difx,
                        'top': $image.position().top + dify
                    });

                    var css = {
                        'left': $image.position().left,
                        'top': $image.position().top
                    };

                    //$image.css(ui.size);
                    /*
                    $('#image_container_backdrop')
                    .css(css);

                    $('#backdrop_mask_img')
                    .css(css);

                    $('.box')
                    .css(css);*/



                    var bgIPos = $('#backdrop_mask_img').position();
                        var l1 = $image.position().left;
                        var t1 = $image.position().top;


                        if(l1>l){

                            ldiff = l1-l;
                            $('#backdrop_mask_img')
                                    .css({left:(
                                        (bgIPos.left)+ldiff
                                    )+"px"});

                        }else{

                            ldiff = l-l1;
                            $('#backdrop_mask_img')
                                    .css({left:(
                                        (bgIPos.left)-ldiff
                                    )+"px"});

                        }


                        if(t1>t){

                            tdiff = t1-t;
                            $('#backdrop_mask_img')
                                    .css({top:(
                                        (bgIPos.top)+tdiff
                                    )+"px"});

                        }else{

                            tdiff = t-t1;
                            $('#backdrop_mask_img')
                                    .css({top:(
                                        (bgIPos.top)-tdiff
                                    )+"px"});

                        }



                            $('#image_container_backdrop')
                            .css(css); 



                    $('.box')
                    .css(css);




                    
                }/*,
                'pinchin pinchout drag': function () {
                    //tool.trigger('change', tool.val());
                }*/
            });
            
        };

        var setup = function () {
            if (isTouch) {
                setupTouch();
            } else {
                setupMouse();
            }
        };

        var getResponsiveScale = function () {
            // Relation between the final (config file) and the visible (CSS) dimentions 
            return Math.min(module.config().finalWidth / $mask.width(), module.config().finalHeight / $mask.height());
        };

        var real = function (value) {
            return (value * getResponsiveScale()).toFixed(2);
        };
        
        var responsive = function (value) {
            return (value / getResponsiveScale()).toFixed(2);
        };

        var CropTool = Backbone.View.extend({
            initialize: function () {
                var self = this;

                //this.$el.prepend(_.template('<img id="cached_img">'));

                $cached = this.$('#cached_img');
                $image = this.$('img.image');
                $box = this.$('.box');
                $mask = this.$('.mask');

            },

            getDiff:function(val1, val2){
                return val1>val2?val1-val2:val2-val1;
            },


            load: function (src) {
                var self = this;

                $('#backdrop_mask_img').css({
                    'top':0,
                    'left':0
                });
                
                hideCurrent().done(function () {
                    preload(src).then(function () {
                        showCurrent().done(function () {


                        setTimeout(function(){
                            setup();
                            // constrain();
                            // center();
                            self.focus();

                            self.adjustBackdropMask();

                            /*$('.image').on('load',function(){
                                
                            });*/



                            //$image.css('width','100%');
                            //$image.css('height','auto');

                            var imgWidth = $image.width();
                            var imgHeight = $image.height();
                            
                            // // // // // // // // // // // // // // // // // // // // // // // // //
                            // THIS IS A TEMPORARY FIX SO THAT WE ACTUALLY GET CROPPED IMAGES
                            // YES, THIS FIX IS LITERALLY JUST COMMENTING OUT THE BELOW IF STATEMENT
                            // // // // // // // // // // // // // // // // // // // // // // // // // 
                            // 
                            // if(imgHeight>imgWidth){
                            //     $image.css('width',
                            //         $('#backdrop_mask').width()+"px");
                            //         $image.css('height','auto');
                            // }else{
                            //     $image.css('height',
                            //         $('#backdrop_mask').height()+"px");
                            //     $image.css('width','auto');
                            // }



                            //position the image centered 
                            //in relation to backdrop
                            //$image.offset().left




                            //1. Adjust image so it fits the viewport
                            ///////////////////////////////////////////////

                            var newImgLeft=0, newImgTop=0, 
                            bmPos = $('#backdrop_mask').offset(),
                            imgPos = $image.offset();

                            mask_offLeft = 
                            self.getDiff(imgPos.left,bmPos.left),
                            mask_offTop = 
                            self.getDiff(imgPos.top,bmPos.top);



                            if(imgPos.left>bmPos.left){
                                mask_offLeftDir="left";
                                newImgLeft = $image.position().left-mask_offLeft;
                            }else{
                                mask_offLeftDir="right";
                                newImgLeft = $image.position().left+mask_offLeft;
                            }
                            

                            if(imgPos.top>bmPos.top){
                                mask_offTopDir="up";
                                newImgTop = $image.position().top-mask_offTop;
                            }else{
                                mask_offTopDir="down";
                                newImgTop = $image.position().top+mask_offTop;
                            }

                            

                            $image.css({
                                'left':newImgLeft+"px",
                                'top':newImgTop+"px"
                            });




                            //2. Assign same image to backdrop
                            ///////////////////////////////////////////////
                            $('#backdrop_mask_img').attr('src',
                                $image.attr('src'));

                            

                            //3. Apply the dimensions to this image as well
                            ///////////////////////////////////////////////
                            $('#backdrop_mask_img').css({
                                'width':$image.width()+"px",
                                'height':'auto'
                            });

                            


                            //4. Position mask to match the image
                            //var k = bmPos;
                            var newMskLeft, newMskTop,
                            maskOffset = $('.mask').offset(),
                            mskOffsetLeft = 
                            self.getDiff(maskOffset.left,bmPos.left),
                            mskOffsetTop = 
                            self.getDiff(maskOffset.top,bmPos.top);

                            if(maskOffset.left>bmPos.left){
                                newMskLeft = $('.mask').position().left-mskOffsetLeft;
                            }else{
                                newMskLeft = $('.mask').position().left+mskOffsetLeft;
                            }

                            if(maskOffset.top>bmPos.top){
                                newMskTop = $('.mask').position().top-mskOffsetTop;
                            }else{
                                newMskTop = $('.mask').position().top+mskOffsetTop;
                            }


                            $('.mask').css({
                                'left':newMskLeft+"px",
                                'top':newMskTop+"px"
                            });

                            $('.mask').css('width',
                                $('#backdrop_mask').width()+'px');
                            $('.mask').css('height',
                                $('#backdrop_mask').height()+'px');



                            //5.
                            //image_container_backdrop
                            var newIcbTop, newIcbTop,
                            icbOffset = $('#image_container_backdrop').offset(),
                            icbOffsetLeftDiff = 
                            self.getDiff(icbOffset.left,$image.offset().left),
                            icbOffsetTopDiff = 
                            self.getDiff(icbOffset.top,$image.offset().top);


                            if(icbOffset.left>$image.offset().left){
                                newIcbLeft = 
                                $('#image_container_backdrop')
                                .position().left-icbOffsetLeftDiff;
                            }else{
                                newIcbLeft = 
                                $('#image_container_backdrop')
                                .position().left+icbOffsetLeftDiff;
                            }

                            if(icbOffset.top>$image.offset().top){
                                newIcbTop = 
                                $('#image_container_backdrop')
                                .position().top-icbOffsetTopDiff;
                            }else{
                                newIcbTop = 
                                $('#image_container_backdrop')
                                .position().top+icbOffsetTopDiff;
                            }



                            $('#image_container_backdrop').css({
                                'top':newIcbTop+'px',
                                'left':newIcbLeft+'px',
                                'width':$('#backdrop_mask_img').width()+'px',
                                'height':$('#backdrop_mask_img').height()+'px'
                            });


                            //alert($image.width());

                            //$image.css('width','100%');

                            //5.
                            //Adjust the box's position
                            var newBoxTop, newBoxTop,
                            boxOffset = $('.box').offset(),
                            boxOffsetLeftDiff = 
                            self.getDiff(boxOffset.left,$image.offset().left),
                            boxOffsetTopDiff = 
                            self.getDiff(boxOffset.top,$image.offset().top);


                            if(boxOffset.left>$image.offset().left){
                                newBoxLeft = 
                                $('.box')
                                .position().left-boxOffsetLeftDiff;
                            }else{
                                newBoxLeft = 
                                $('.box')
                                .position().left+boxOffsetLeftDiff;
                            }

                            if(boxOffset.top>bmPos.top){
                                newBoxTop = 
                                $('.box')
                                .position().top-boxOffsetTopDiff;
                            }else{
                                newBoxTop = 
                                $('.box')
                                .position().top+boxOffsetTopDiff;
                            }

                            $('.box').css({
                                'top':newBoxTop+'px',
                                'left':newBoxLeft+'px',
                                'width':$image.width()+'px',
                                'height':$image.height()+'px'
                            });
                            



                            

                            $image.css('visibility', 'visible');
                            //self.trigger('change', self.val());

                        },1000);


                        });
                    });
                });

                this.blur();
            },

            centerImg:function(){
                /*
                if($image.width()>$('#backdrop_mask').width()){
                            lll = $image.position().left - (self.getDiff(
                                $('#backdrop_mask').width(),
                                $image.width())/2);

                            alert(lll)

                            $image.css('left',lll+'px');

                            }
                */
            },

            adjustBackdropMask:function(){

                var bmWidth = $('#backdrop_mask').width();
                var offset = $('#backdrop_mask').offset();

                left_constrain = offset.left;
                upper_leftConstrain = left_constrain + bmWidth;
                topConstrain = offset.top;
                lower_topConstrain = topConstrain + bmWidth;
                
            },

            focus: function () {
                // Show borders and handles
                this.$el.addClass('active');
            },
            blur: function () {
                // Hide borders and handles
                this.$el.removeClass('active');
            },

            val: function () {
                var position = $image.position();

                /*return {
                    'x': real(position.left),
                    'y': real(position.top),
                    'width': real($image.width()),
                    'height': real($image.height())
                };*/

                return {
                    'x': position.left,
                    'y': position.top,
                    'width': $image.width(),
                    'height': $image.height()
                };
            }
        });

        _.extend(CropTool, Backbone.Events);

        var tool = new CropTool({
            el: '#crop_tool'
        });

        return tool;

});