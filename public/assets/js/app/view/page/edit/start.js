define([
        'backbone',
        'app/view/abstract/child'
    ], function (Backbone, AbstractChildView) {

        var StartView = AbstractChildView.extend({
            events: {}
        });

        return new StartView({
            el: '#edit_start'
        });
    }
);