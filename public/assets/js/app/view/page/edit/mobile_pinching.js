define(
    [
        'module',
        'backbone',
        'jquery',
        'hammer'
        //'jquery-ui',
    ],
    function (module, Backbone, jQuery, hammer){//, ui, ) {

        var ham;

        var MobilePinching = Backbone.View.extend({

            pinching:false,

            pinchingOffset:{
                'top':{ 'num':0, 'dir':'down' },
                'left':{ 'num':0, 'dir':'left' }
            },

            pinchingStops:{
                'stopAt':0, 'axis':'y', 'calcPerc':0
            },

            pinchingIn:true,
            pinchingOut:true,

            handleOffset:{},
            maskOffset:{},

            pinchingInterval:null,

            theHammerLoader:null,

            pinching_handle:null,
            viewPortWidth:null,
            viewPortHeight:null,

            events: {

                'load #pinching_handle': function () {
                    alert('load');
                }
            },

            up:function(){

                if(pinching == true){alert('pinching');}

                    var css = {
                        'top': '0px',
                        'left': '0px',
                        'width': ($('#pinching_holder')
                            .width() + Math.ceil(s))+"px",
                        'height': ($('#pinching_holder')
                            .height() + Math.ceil(s))+"px"
                    };
                
                    $('#pinching_handle').css(css);
            },

            initialize:function(){

                var ieVersion = this.getBrowserVersion();
                if(ieVersion<0||ieVersion>8){

                    var self = this;
                    //$(document).ready(function(){

                    this.viewPortWidth = $('.pinching_viewport').width();
                    this.viewPortHeight = $('.pinching_viewport').height();

                    this.pinching_handle = $('#pinching_handle');

                    var element = document.getElementById('pinching_holder');

                    ham = hammer(element, {
                    'prevent_defaults': true,
                    'drag':true,
                    //'transform':false,
                    'gesture':true
                    });

                    this.theHammerLoader = 
                    setTimeout(function(){self.pinchingEvents();}, 1000);

                }

            },

            loadHammer:function(){

                ham = $('#pinching_holder').hammer({
                'prevent_defaults': true,
                'drag':true,
                //'transform':false,
                'gesture':true
                });

                this.pinchingEvents();

            },

            getBrowserVersion:function(){



                var rv = -1; // Return value assumes failure.
                if (navigator.appName == 'Microsoft Internet Explorer') {
                    var ua = navigator.userAgent;
                    var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
                    if (re.exec(ua) != null)
                        rv = parseFloat(RegExp.$1);
                }

                return rv;


            },

            resetOffset:function(){

                var self = this,
                    sOff, MOff, mPos;

                        sOff = $(self.pinching_handle).offset();
                        mOff = $('#pinching_mask_img').offset();
                        mPos = $('#pinching_mask_img').position();

                        if(sOff.left>mOff.left){
                            //right
                            self.pinchingOffset.left.num = (sOff.left - mOff.left);
                            self.pinchingOffset.left.dir = 'right';

                        }else{
                            //left
                            self.pinchingOffset.left.num = (mOff.left - sOff.left);
                            self.pinchingOffset.left.dir = 'left';
                        }



                        if(sOff.top>mOff.top){
                            //down
                            self.pinchingOffset.top.num = (sOff.top - mOff.top);
                            self.pinchingOffset.top.dir = 'down';

                        }else{
                            //up
                            self.pinchingOffset.top.num = (mOff.top - sOff.top);
                            self.pinchingOffset.top.dir = 'up';

                        }


            },

            constrain:function(dir){


                //if(dir == 'in'){

                    var self = this;

                    if(self.pinchingStops.axis == "x"){



                        var width = document
                            .getElementById('pinching_handle')
                            .clientWidth;

                        if(width < self.pinchingStops.stopAt){

                            self.pinchingIn=false;

                            

                            /*document
                            .getElementById('pinching_handle')
                            .style.width = self.pinchingStops.stopAt+"px";*/

                        }
                    }


                    if(this.pinchingStops.axis == "y"){

                        var height = document
                            .getElementById('pinching_handle')
                            .clientHeight;

                            //$('#disp').html(height+" --- "+self.pinchingStops.stopAt);

                        if(height < self.pinchingStops.stopAt){

                            self.pinchingIn=false;

                            document
                            .getElementById('pinching_handle')
                            .style.width = 
                            (((parseFloat(self.pinchingStops.calcPerc)/100)*
                                parseFloat(self.pinchingStops.stopAt))+2)+"px";

                            document
                            .getElementById('pinching_handle')
                            .style.top = "0px";

                        }
                    }


                
                //}else{
                    //self.pinchingIn=true;
                //}

                //self.scaleElement(dir);

            },

            load:function(img){
                try{
                    var self = this;
                    $(this.pinching_handle).attr('src',img);

                    $(this.pinching_handle).on('load',function(){

                        var sOff, MOff, mPos, 
                        phW = $(self.pinching_handle).width(),
                        phH = $(self.pinching_handle).height();

                        $('#pinching_mask_img').attr('src',img);
                        $('#pinching_mask_img').css({
                            'width':$(self.pinching_handle).width()
                        });

                        sOff = $(self.pinching_handle).offset();
                        mOff = $('#pinching_mask_img').offset();
                        mPos = $('#pinching_mask_img').position();

                        if(sOff.left>mOff.left){
                            //right
                            self.pinchingOffset.left.num = (sOff.left - mOff.left);
                            self.pinchingOffset.left.dir = 'right';

                            //Move mask left position to match handle
                            $('#pinching_mask_img')
                            .css('left',
                            (mPos.left+self.pinchingOffset.left.num)+'px');

                        }else{
                            //left
                            self.pinchingOffset.left.num = (mOff.left - sOff.left);
                            self.pinchingOffset.left.dir = 'left';

                            //Move mask left position to match handle
                            $('#pinching_mask_img')
                            .css('left',
                            (mPos.left-self.pinchingOffset.left.num)+'px');
                        }



                        if(sOff.top>mOff.top){
                            //down
                            self.pinchingOffset.top.num = (sOff.top - mOff.top);
                            self.pinchingOffset.top.dir = 'down';

                            //Move mask top position to match handle
                            $('#pinching_mask_img')
                            .css('top',
                            (mPos.top+self.pinchingOffset.top.num)+'px');

                        }else{
                            //up
                            self.pinchingOffset.top.num = (mOff.top - sOff.top);
                            self.pinchingOffset.top.dir = 'up';

                            //Move mask top position to match handle
                            $('#pinching_mask_img')
                            .css('top',
                            (mPos.top-self.pinchingOffset.top.num)+'px');

                        }


                        //set up the stopAts

                        if(phW > phH){

                            self.pinchingStops.axis = "y";
                            self.pinchingStops.stopAt = 
                            $('.pinching_viewport').height();
                            self.pinchingStops.calcPerc = (phW/phH)*100;



                        }else{
                            self.pinchingStops.axis = "x";
                            self.pinchingStops.stopAt = 
                            $('.pinching_viewport').width();
                        }
                        

                    });

                    

                 }catch(err){

                 }   
            },

            pinchingEvents:function()
            {

                clearTimeout(this.theHammerLoader);

                var self = this,
                    pinching = false, nWidth,
                    ph = document.getElementById('pinching_holder'),
                    mImg = document.getElementById('pinching_mask_img'),
                    //config = getResizableConfig(),
                    previous = {
                        scale: 1,
                        x: 0,
                        y: 0
                    };

                ham.on("tap", function(evt) {
                        //alert('tap!');
                });

                ham.on("click", function(evt) {
                        //alert('click!');
                });

                ham.on("dragend", function(ev) {
                        previous.x = ev.gesture.deltaX;
                        previous.y = ev.gesture.deltaY;
                        self.pinchingIn = true;
                });

                ham.on("dragstart", function(ev) {
                    self.pinchingIn = true;

                        previous.x = ev.gesture.deltaX;
                        previous.y = ev.gesture.deltaY;

                        $('#pinching_mask_img')
                        .css('left',
                            $('#pinching_holder')
                            .position().left+"px");

                        $('#pinching_mask_img')
                        .css('top',
                            $('#pinching_holder')
                            .position().top+"px");


                        self.resetOffset();

                });

                ham.on("drag", function(evt) {

                        evt.gesture.preventDefault();
                        
                        var leftMove, topMove, m_leftMove = 0,
                            Xamount, Yamount, x, y,
                            pos = $('#pinching_holder').position();
                        //x = evt.gesture.deltaX;

                        x = evt.gesture.deltaX;//,
                        y = evt.gesture.deltaY;

                        if(x>previous.x){

                            //Move to the Right

                            Xamount = x - previous.x;
                            leftMove = (pos.left+Xamount);
                            if(self.pinchingOffset.left.dir=="left"){
                                m_leftMove = leftMove-parseFloat(self.pinchingOffset.left.num);
                            }else{
                                m_leftMove = leftMove+parseFloat(self.pinchingOffset.left.num);
                            }
                            $('#pinching_mask_img').css('left',m_leftMove+"px");
                            $('#pinching_holder').css('left',leftMove+"px");

        
                        }else{

                            //Move to the Left
                            
                            Xamount = previous.x - x;
                            leftMove = (pos.left-Xamount);
                            if(self.pinchingOffset.left.dir=="left"){
                                m_leftMove = leftMove-parseFloat(self.pinchingOffset.left.num);
                            }else{
                                m_leftMove = leftMove+parseFloat(self.pinchingOffset.left.num);
                            }
                            $('#pinching_mask_img').css('left',m_leftMove+"px");
                            $('#pinching_holder').css('left',leftMove+"px");

                        }



                        if(y>previous.y){

                            //Move Down
                            
                            Yamount = y - previous.y;
                            topMove = (pos.top+Yamount);
                            if(self.pinchingOffset.top.dir=="down"){
                                m_topMove = topMove+parseFloat(self.pinchingOffset.top.num);
                            }else{
                                m_topMove = topMove-parseFloat(self.pinchingOffset.top.num);
                            }

                            $('#pinching_mask_img').css('top',m_topMove+"px");
                            $('#pinching_holder').css('top',topMove+"px");
        
                        }else{

                            //Move Up
                            
                            Yamount = previous.y - y;
                            topMove = (pos.top-Yamount);
                            if(self.pinchingOffset.top.dir=="down"){
                                m_topMove = topMove+parseFloat(self.pinchingOffset.top.num);
                            }else{
                                m_topMove = topMove-parseFloat(self.pinchingOffset.top.num);
                            }
                            
                            $('#pinching_mask_img').css('top',m_topMove+"px");
                            $('#pinching_holder').css('top',topMove+"px");

                        }



                        previous.x = evt.gesture.deltaX;
                        previous.y = evt.gesture.deltaY;
                });

                ham.on("release", function(evt) {
                    self.pinchingIn = true;
                    previous.scale = evt.gesture.scale;


                    /*self.constrain();
                    self.matchPositions();
                    self.resetOffset();*/



                });

                /*ham.on("pinchin", function(evt) {

                    evt.gesture.preventDefault();

                    ph.clientWidth;
                        ph.style.width = 
                        (ph.clientWidth*evt.gesture.scale)+"px";

                    if(pinching){
                        clearInterval(self.pinchingInterval);
                        }
                        pinching = false;



                    try{
                        evt.gesture.preventDefault();

                        if(pinching==false){
                            pinching = true;
                            self.scaleElement('down');
                        }

                    }catch(err){
                        //alert(err.message);
                        
                    }
                });*/

                ham.on("pinchin", function(evt) {

                    evt.gesture.preventDefault();

                    s = evt.gesture.scale/previous.scale;

                    ph.clientWidth;
                        ph.style.width = 
                        (ph.clientWidth*s)+"px";

                    nWidth = ph.clientWidth;
                    //Assign the same dimensions to the mask.
                    //Dont worry to apply the height here as
                    //it will flow freely as auto.
                    mImg.style.width = nWidth+"px";

                    previous.scale = evt.gesture.scale;



                    /*


                var self = this, w, nWidth, nHeight,
                ph = document.getElementById('pinching_holder'),
                mImg = document.getElementById('pinching_mask_img');

                    nWidth = ph.clientWidth;
                    //Assign the same dimensions to the mask.
                    //Dont worry to apply the height here as
                    //it will flow freely as auto.
                    mImg.style.width = nWidth+"px";



                    */

                        //if(self.pinchingIn){
                    //self.constrain('in');
                    //}

                    /*if(pinching){
                        clearInterval(self.pinchingInterval);
                        }

                        pinching = false;


                    try{
                        evt.gesture.preventDefault();

                        if(pinching==false){
                            pinching = true;
                            self.scaleElement('up');

                        }
                    }catch(err){
                        //alert(err.message);
                        
                    }*/
                });

                ham.on("pinchout", function(evt) {

                    evt.gesture.preventDefault();

                    s = evt.gesture.scale/previous.scale;

                    ph.clientWidth;
                        ph.style.width = 
                        (ph.clientWidth*s)+"px";

                    nWidth = ph.clientWidth;
                    //Assign the same dimensions to the mask.
                    //Dont worry to apply the height here as
                    //it will flow freely as auto.
                    mImg.style.width = nWidth+"px";

                    previous.scale = evt.gesture.scale;

                    /*if(!self.pinchingIn){
                        self.pinchingIn = true;*/
                    //self.constrain('out');
                    //}
                    /*if(pinching){
                        clearInterval(self.pinchingInterval);
                        }

                        pinching = false;


                    try{
                        evt.gesture.preventDefault();

                        if(pinching==false){
                            pinching = true;
                            self.scaleElement('up');

                        }
                    }catch(err){
                        //alert(err.message);
                        
                    }*/
                });

            },

            stayInViewport_adjustPosition:function(){

                var self = this, css,
                    sOff = $('#pinching_holder').offset()
                    sPos = $('#pinching_holder').position(),
                    mOff = $('#pinching_mask_img').offset(),
                    mPos = $('#pinching_mask_img').position(),
                    sW = $('#pinching_holder').width(),
                    sH = $('#pinching_holder').height(),
                    vW = $('.pinching_viewport').width(),
                    vH = $('.pinching_viewport').height();


                    

                /*if(sW>sH){
                    if(sW<vW){
                        css = {
                            'width':vW+'px',
                            'height':'auto',
                        };

                        $('#pinching_holder').css(css);
                    }
                }


                if(sH>sW){
                    if(sH<vH){

                        css = {
                            'width':vW+'px',
                            'height':'auto',
                        };

                        $('#pinching_holder').css(css);
                    }
                }*/




                if((sPos.left+sW)<0){
                    //this means that the image has gone off too
                    //much to the left.

                    $('#pinching_holder').css('left','0px');
                 }


                 if(sPos.left>(vW -((1/10)*sW)) ||sPos.left==(vW -((1/10)*sW))){
                    //this means that the image has gone off too
                    //much to the left.



                    $('#pinching_holder').css('left',((sW*-1)+vW)+"px");
                 }

                //((sPos.top+sH)<0)
                 if((sPos.top+sH)<0){
                    //this means that the image has gone off too
                    //much to the top.

                    $('#pinching_holder').css('top','0px');
                 }


                 if(sPos.top>(vH -((1/10)*sH))||sPos.top==(vH -((1/10)*sH))){
                    //this means that the image has gone off too
                    //much to the left.

                    $('#pinching_holder').css('top',((sH*-1)+vH)+"px");
                 }
            },

            matchPositions:function(){


                
                var self = this,
                    sOff = $(self.pinching_handle).offset()
                    sPos = $(self.pinching_handle).position(),
                    mOff = $('#pinching_mask_img').offset(),
                    mPos = $('#pinching_mask_img').position(),
                    sW = $(self.pinching_handle).width(),
                    sH = $(self.pinching_handle).height(),
                    vW = $('.pinching_viewport').width(),
                    vH = $('.pinching_viewport').height();


                self.stayInViewport_adjustPosition();

                 


                 //Since we may have changed the position of the handle
                 //we need to reassign the correct one here.
                 sPos = $(self.pinching_handle).position();


                if(sOff.left>mOff.left){
                    //right
                    
                    //Move mask left position to match handle
                    $('#pinching_mask_img')
                    .css('left',
                    (mPos.left+(sOff.left - mOff.left))+'px');

                }else{
                    //left

                    //Move mask left position to match handle
                    $('#pinching_mask_img')
                    .css('left',
                    (mPos.left-(mOff.left - sOff.left))+'px');
                }



                if(sOff.top>mOff.top){
                    //down

                    //Move mask top position to match handle
                    $('#pinching_mask_img')
                    .css('top',
                    (mPos.top+(sOff.top - mOff.top))+'px');

                }else{
                    //up

                    //Move mask top position to match handle
                    $('#pinching_mask_img')
                    .css('top',
                    (mPos.top-(mOff.top - sOff.top))+'px');

                }


                //Match the Width and Height of the pictures.
                $('#pinching_mask_img')
                    .css({
                        'width':$(self.pinching_handle).width()+'px',
                        'height':$(self.pinching_handle).height()+'px'
                    });


                //Now check to see if the image has gone out of viewing area






            },

            scaleElement:function(dir){


                var self = this, w, nWidth, nHeight,
                ph = document.getElementById('pinching_holder'),
                mImg = document.getElementById('pinching_mask_img');

                /*this.pinchingInterval = setInterval(function(){
                    w = ph.clientWidth;
                    if(dir=="down"){
                        ph.style.width = (w-3)+"px";
                    }else{
                        ph.style.width = (w+3)+"px";
                    }*/

                    nWidth = ph.clientWidth;
                    //Assign the same dimensions to the mask.
                    //Dont worry to apply the height here as
                    //it will flow freely as auto.
                    mImg.style.width = nWidth+"px";



                //},1);

            }

        });

        _.extend(MobilePinching, Backbone.Events);

        var tool = new MobilePinching({
            el: '#mobile_pinching'
        });

        return tool;

});