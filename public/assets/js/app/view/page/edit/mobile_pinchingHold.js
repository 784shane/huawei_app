define(
    [
        'module',
        'backbone',
        'jquery',
        'hammer'
        //'jquery-ui',
    ],
    function (module, Backbone, jQuery, hammer){//, ui, ) {

        var ham;

        

        var pinchingEvents = function(){


        };

        var MobilePinching = Backbone.View.extend({

            pinching:false,

            pinchingOffset:{
                'top':{ 'num':0, 'dir':'down' },
                'left':{ 'num':0, 'dir':'left' }
            },

            handleOffset:{},
            maskOffset:{},

            pinchingInterval:null,

            pinching_handle:null,

            events: {

                'load #pinching_handle': function () {
                
                }
            },

            up:function(){

                if(pinching == true){
                    
                }

                    var css = {
                        'top': '0px',
                        'left': '0px',
                        'width': ($('#pinching_holder')
                            .width() + Math.ceil(s))+"px",
                        'height': ($('#pinching_holder')
                            .height() + Math.ceil(s))+"px"
                    };
                
                    $('#pinching_handle').css(css);
            },

            initialize:function(){

                //$(document).ready(function(){

                this.pinching_handle = $('#pinching_handle');
                //create an object to hold that image.
                imgNode = document.createElement("img");
                //var img = document.getElementById('FULL_SRC');
                
                //ham = hammer($('#pinching_holder'));

                ham = $('#pinching_holder').hammer({
                'prevent_defaults': true,
                'drag':true,
                //'transform':false,
                'gesture':true
                });

                this.pinchingEvents();
                

                /*$('#pinching_holder').hammer({
                'prevent_defaults': true,
                'drag':false
                }).on("drag", function(ev) {
                    
                });
*/              

                /*var previous={'scale':1};
                var options = {
                  preventDefault: true
                };
                var hammertime = new Hammer(document, options);
                hammertime.on("pinchin pinchout", function(evt){ 
                    $('#pinching_holder')
                    .append(evt.gesture.scale+" "+
                    Math.floor(evt.gesture.scale)+"/n");

                evt.gesture.preventDefault();

                pinching=true;



                try{
                    var s = previous.scale;
                    //pinching = true;
                    previous.scale = evt.gesture.scale;

                    //scaling the image now
                    var css = {
                        'top': '0px',
                        'left': '0px',
                        'width': ($('#pinching_holder')
                            .width() + Math.ceil(s))+"px",
                        'height': ($('#pinching_holder')
                            .height() + Math.ceil(s))+"px"
                    };
                
                    $('#pinching_handle').css(css);
                }catch(er){
                    $('#pinching_holder').append(er.message);
                }
                });*/





                //});
            },

            pinchingSetup:function(){
                
            },

            loadHandler:function(){
            
            },

            load:function(img){
                try{
                    var self = this;
                    $(this.pinching_handle).attr('src',img);

                    $(this.pinching_handle).on('load',function(){

                        var sOff, MOff, mPos;

                        $('#pinching_mask_img').attr('src',img);
                        $('#pinching_mask_img').css({
                            'width':$(self.pinching_handle).width()
                        });

                        sOff = $(self.pinching_handle).offset();
                        mOff = $('#pinching_mask_img').offset();
                        mPos = $('#pinching_mask_img').position();

                        if(sOff.left>mOff.left){
                            //right
                            self.pinchingOffset.left.num = (sOff.left - mOff.left);
                            self.pinchingOffset.left.dir = 'right';

                            //Move mask left position to match handle
                            $('#pinching_mask_img')
                            .css('left',
                            (mPos.left+self.pinchingOffset.left.num)+'px');

                        }else{
                            //left
                            self.pinchingOffset.left.num = (mOff.left - sOff.left);
                            self.pinchingOffset.left.dir = 'left';

                            //Move mask left position to match handle
                            $('#pinching_mask_img')
                            .css('left',
                            (mPos.left-self.pinchingOffset.left.num)+'px');
                        }



                        if(sOff.top>mOff.top){
                            //down
                            self.pinchingOffset.top.num = (sOff.top - mOff.top);
                            self.pinchingOffset.top.dir = 'down';

                            //Move mask top position to match handle
                            $('#pinching_mask_img')
                            .css('top',
                            (mPos.top+self.pinchingOffset.top.num)+'px');

                        }else{
                            //up
                            self.pinchingOffset.top.num = (mOff.top - sOff.top);
                            self.pinchingOffset.top.dir = 'up';

                            //Move mask top position to match handle
                            $('#pinching_mask_img')
                            .css('top',
                            (mPos.top-self.pinchingOffset.top.num)+'px');

                        }

                    });

                    

                 }catch(err){

                 }   
            },

            pinchingEvents:function()
            {

                var self = this,
                    pinching = false,
                    //config = getResizableConfig(),
                    previous = {
                        scale: 0,
                        x: 0,
                        y: 0
                    };

                ham.on("tap", function(evt) {
                        //alert('tap!');
                });

                ham.on("click", function(evt) {
                        //alert('click!');
                });

                ham.on("dragend", function(ev) {
                        previous.x = ev.gesture.deltaX;
                        previous.y = ev.gesture.deltaY;
                });

                ham.on("dragstart", function(ev) {
                        previous.x = ev.gesture.deltaX;
                        previous.y = ev.gesture.deltaY;
                });

                ham.on("drag", function(evt) {

                        evt.gesture.preventDefault();
                        
                        var leftMove, topMove, m_leftMove = 0,
                            Xamount, Yamount, x, y,
                            pos = $('#pinching_holder').position();
                        //x = evt.gesture.deltaX;

                        x = evt.gesture.deltaX;//,
                        y = evt.gesture.deltaY;

                        if(x>previous.x){

                            //Move to the Right

                            Xamount = x - previous.x;
                            leftMove = (pos.left+Xamount);
                            if(self.pinchingOffset.left.dir=="left"){
                                m_leftMove = leftMove-parseFloat(self.pinchingOffset.left.num);
                            }else{
                                m_leftMove = leftMove+parseFloat(self.pinchingOffset.left.num);
                            }
                            $('#pinching_mask_img').css('left',m_leftMove+"px");
                            $('#pinching_holder').css('left',leftMove+"px");

        
                        }else{

                            //Move to the Left
                            
                            Xamount = previous.x - x;
                            leftMove = (pos.left-Xamount);
                            if(self.pinchingOffset.left.dir=="left"){
                                m_leftMove = leftMove-parseFloat(self.pinchingOffset.left.num);
                            }else{
                                m_leftMove = leftMove+parseFloat(self.pinchingOffset.left.num);
                            }
                            $('#pinching_mask_img').css('left',m_leftMove+"px");
                            $('#pinching_holder').css('left',leftMove+"px");

                        }



                        if(y>previous.y){

                            //Move Down
                            
                            Yamount = y - previous.y;
                            topMove = (pos.top+Yamount);
                            if(self.pinchingOffset.top.dir=="down"){
                                m_topMove = topMove+parseFloat(self.pinchingOffset.top.num);
                            }else{
                                m_topMove = topMove-parseFloat(self.pinchingOffset.top.num);
                            }

                            $('#pinching_mask_img').css('top',m_topMove+"px");
                            $('#pinching_holder').css('top',topMove+"px");
        
                        }else{

                            //Move Up
                            
                            Yamount = previous.y - y;
                            topMove = (pos.top-Yamount);
                            if(self.pinchingOffset.top.dir=="down"){
                                m_topMove = topMove+parseFloat(self.pinchingOffset.top.num);
                            }else{
                                m_topMove = topMove-parseFloat(self.pinchingOffset.top.num);
                            }
                            
                            $('#pinching_mask_img').css('top',m_topMove+"px");
                            $('#pinching_holder').css('top',topMove+"px");

                        }



                        previous.x = evt.gesture.deltaX;
                        previous.y = evt.gesture.deltaY;
                });

                ham.on("release", function(evt) {
                        
                        if(pinching){
                        clearInterval(self.pinchingInterval);
                        self.matchPositions();
                        }
                        pinching = false;

                });

                ham.on("pinchin", function(evt) {

                    if(pinching){
                        clearInterval(self.pinchingInterval);
                        }
                        pinching = false;



                    try{
                        evt.gesture.preventDefault();

                        if(pinching==false){
                            pinching = true;
                            self.scaleElement('down');
                        }

                    }catch(err){
                        //alert(err.message);
                        
                    }
                });

                ham.on("pinchout", function(evt) {

                    if(pinching){
                        clearInterval(self.pinchingInterval);
                        }

                        pinching = false;


                    try{
                        evt.gesture.preventDefault();

                        if(pinching==false){
                            pinching = true;
                            self.scaleElement('up');

                        }
                    }catch(err){
                        //alert(err.message);
                        
                    }
                });

            },

            matchPositions:function(){
                
                var self = this,
                    sOff = $(self.pinching_handle).offset(),
                    mOff = $('#pinching_mask_img').offset(),
                    mPos = $('#pinching_mask_img').position();

                if(sOff.left>mOff.left){
                    //right
                    
                    //Move mask left position to match handle
                    $('#pinching_mask_img')
                    .css('left',
                    (mPos.left+(sOff.left - mOff.left))+'px');

                }else{
                    //left

                    //Move mask left position to match handle
                    $('#pinching_mask_img')
                    .css('left',
                    (mPos.left-(mOff.left - sOff.left))+'px');
                }



                if(sOff.top>mOff.top){
                    //down

                    //Move mask top position to match handle
                    $('#pinching_mask_img')
                    .css('top',
                    (mPos.top+(sOff.top - mOff.top))+'px');

                }else{
                    //up

                    //Move mask top position to match handle
                    $('#pinching_mask_img')
                    .css('top',
                    (mPos.top-(mOff.top - sOff.top))+'px');

                }





            },

            scaleElement:function(dir){


                var self = this, w, nWidth, nHeight,
                ph = document.getElementById('pinching_holder'),
                mImg = document.getElementById('pinching_mask_img');

                this.pinchingInterval = setInterval(function(){
                    w = ph.clientWidth;
                    if(dir=="down"){
                        ph.style.width = (w-20)+"px";
                    }else{
                        ph.style.width = (w+20)+"px";
                    }

                    nWidth = ph.clientWidth;
                    //Assign the same dimensions to the mask.
                    //Dont worry to apply the height here as
                    //it will flow freely as auto.
                    mImg.style.width = nWidth+"px";



                },1);

            }

        });

        _.extend(MobilePinching, Backbone.Events);

        var tool = new MobilePinching({
            el: '#mobile_pinching'
        });

        return tool;

});