define([
        'backbone',
        'app/view/abstract/child',
    ], function (Backbone, AbstractChildView, selectImageView) {

        var SelectImagePopupView = AbstractChildView.extend({
            events: {
                // listen for image select
            },
            // @todo, render images list from collection
            // @todo, lazy loading of images
            initialize: function () {
                
            }
        });

        return new SelectImagePopupView({
            el: '#select_image'
        });
    }
);