define([
        'backbone',
        'app/router/router',
        'app/view/abstract/child'
    ], function (Backbone, router, AbstractChildView) {

        var StepInfo = AbstractChildView.extend({
            events: {},
            initialize: function () {
                // @todo We should listen to all edit routes and update the bar accordingly 
                router.on();
            }
        });

        return new StepInfo({
            el: '#steps'
        });
    }
);