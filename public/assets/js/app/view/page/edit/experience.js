define([
        'backbone',
        'app/router/router',
        'app/view/abstract/child',
        'app/view/page/edit/experience/crop-tool'
    ], function (Backbone, router, AbstractChildView, cropTool) {

        var ExperienceView = AbstractChildView.extend({
            events: {},
            initialize: function () {
                var self = this;

                // @todo, get src
                router.on({
                    'route:crop-image': function () {
                        cropTool.load(src);
                    },
                    'route:write route:preview': function () {
                        cropTool.blur();
                    }
                });
            }
        });

        return new ExperienceView({
            el: '#experience'
        });
    }
);