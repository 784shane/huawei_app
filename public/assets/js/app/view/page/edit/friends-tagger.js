define([
        'backbone',
        'app/view/abstract/child'
    ], function (Backbone, AbstractChildView) {

        var FriendsTaggerView = AbstractChildView.extend({
            events: {}
        });

        return new FriendsTaggerView({
            el: '#friends_tagger'
        });
    }
);