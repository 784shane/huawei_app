define([
        'backbone',
        'app/router/router',
        'app/view/abstract/child',
        'app/view/page/edit/image-sources/select-image'
    ], function (Backbone, router, AbstractChildView, selectImagePopupView) {

        var ImageSourcesView = AbstractChildView.extend({
            events: {},
            initialize: function () {
                var self = this;
                router.on({
                    'route:select-source': function () {
                        // Show sources buttons
                        selectImagePopupView.transitionOut();
                    },
                    'route:select-image': function (provider) {
                        // Load images from provider and then...

                        // Show images popup
                        selectImagePopupView.transitionIn();
                    }
                });
            }
        });

        return new ImageSourcesView({
            el: '#image_sources'
        });
    }
);