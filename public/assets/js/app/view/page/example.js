define(
    [
        'module',
        'backbone',
        'app/view/abstract/child',
        'app/service/facebook',
        'app/service/instagram'
    ],
    function (module, Backbone, AbstractChildView, Facebook, Instagram) {
        
       
        var ExampleView = AbstractChildView.extend({

            events: {
                // Login with facebook and request default permissions
                'click #login': function () {
                    // Default scope (comes from the config file)
                    var scope = module.config().scope;
                    
                    Facebook.login([scope]).done(function () {
                        // After successful login, go to edit route
                        Backbone.history.navigate('edit', true);
                    });
                },


                'click #facebook-photos': function () {
                    Facebook.loadPhotos().done(function () {
                       // After successfully fetching facebook images, go to select image route
                        Backbone.history.navigate('edit/select-image', true);
                    });
                },

                // Login with instagram and get images
                'click #instagram': function () {
                    Instagram.loadPhotos().done(function () {
                        // After successfully fetching instagram images, go to select image route
                        Backbone.history.navigate('edit/select-image', true);
                    });
                }
            },

            initialize: function () {
                var self = this;

                Instagram.on('image:added', function (remoteImageModel) {
                    // This event listener could be in the images popup view (app/view/page/edit/image-sources/select-image), 
                    // and for each new model added it would render and append the new item
                });

                Facebook.on('image:added', function (remoteImageModel) {

                });
            }
        });

        return new ExampleView({
            el: '#example'
        });
        
    }
);