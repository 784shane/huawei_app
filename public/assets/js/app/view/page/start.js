define([
        'module',
        'backbone',
        'app/router/router',
        'app/view/abstract/child',
        'app/view/page/terms-conditions',
        'app/service/facebook',
        'app/service/instagram'
    ], function (module, Backbone, router, AbstractChildView, termsConditions, Facebook, Instagram) {

        var StartView = AbstractChildView.extend({
            initialize: function () {
                router.on({
                    'route:start': function () {
                        termsConditions.transitionOut();
                    },
                    'route:terms-conditions': function () {
                        termsConditions.transitionIn();
                    }
                });
            }
        });

        return new StartView({
            el: '.landing' // rename to match template id
        });
    }
);