define([
        'backbone',
        'app/view/abstract/child'
    ], function (Backbone, AbstractChildView) {

        var ThanksView = AbstractChildView.extend({
            events: {}
        });

        return new ThanksView({
            el: '#thanks'
        });
    }
);