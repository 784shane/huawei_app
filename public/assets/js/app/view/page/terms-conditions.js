define([
        'backbone',
        'app/view/abstract/child',
    ], function (Backbone, AbstractChildView, termsConditions) {

        var TermsConditionsView = AbstractChildView.extend({
            initialize: function () {
                var self = this;
            }
        });

        return new TermsConditionsView({
            el: '#terms-conditions'
        });
    }
);