define(
    [
        'module',
        'backbone',
        'app/router/router',
        'app/transitions/transitions',
        'app/view/page/edit/crop-tool'/*,
        'app/view/abstract/parent',
        'app/view/component/loading',
        'app/view/page/start',
        'app/view/page/edit',
        'app/view/page/thanks',
        'app/view/page/example'*/
    ],

    function (module, Backbone, router, transitions, cropTool) {


        

        //var MainView = AbstractParentView.extend({
        var MainView = Backbone.View.extend({
            events: {

                'click #create_yours_button': function () {
                    transitions.theStop();
                    transitions.fade_out_page_0();
                },

                'click #testbutton': function () {
                    //cropTool.load('assets/graphics/trash/friend_square.png');
                    cropTool.load('http://images5.alphacoders.com/335/335544.jpg');
                    //transitions.fade_out_page_0();
                },

                'click #front_page_check': function () {
                    //transitions.fade_out_page_0();
                },

                'click #fb_source_button': function () {
                    transitions.open_friends();
                },

                'click #friends_close':function() {
                    transitions.close_friends();
                },

                'click #ist_source_button': function () {
                    //transitions.fade_out_page_0();
                },

                'click #hw_source_button': function () {
                    //transitions.fade_out_page_0();
                    transitions.move_phone_for_thanks();
                },

                'click #ctas_pic':function() {
                    transitions.show_pic_buttons(0);
                    //$('.pic_source_block').show();
                }

            },

            
            children: {
               /*'start': startView,
               'edit': editView,
               'thanks': thanksView*/
            },

            initialize: function () {
                var self = this;

                transitions.test();

                router.on({
                    /*'route:start route:terms-conditions': function () {
                        self.showChild('start');
                    },
                    'route:select-source route:select-image route:crop-image route:write route:preview': function () {
                        self.showChild('edit');
                    },
                    'route:thanks': function () {
                        self.showChild('thanks');
                    }*/

                    'route:app getPost': function () {
                    },

                    'route:defaultRoute': function () {
                    }
                });

                $(window).on('resize', function () {
                    Backbone.trigger('app:resize');
                }).trigger('resize');
            }
        });


        return new MainView({
            el: '#mainApp'
        });
        
    }
);