define(
    [
        'module',
        'backbone',
        'app/service/facebook',
        'app/model/experience',
        'app/model/image-info',
        'app/transitions/transitions',
        'spin',
    ],
    function (module, Backbone, Facebook, Experience, ImageInfo, transitions, Spinner) {

        var theTimeout = null;

        var picturePostingUrl = '/app/generatephoto';
        picturePostingUrl = '/api/experience';

        var user_to_be_tagged = null;

        var tac_accepted = false;

        var Control = new Object;

        var entrySteps = {'photo_selected':false,
                          'text_chosen':false,
                          'tagging_decided':false};

        var source_image_provider = "";
        var image_short_url = "";

        var recieveEmails = 0;

        var IEclickedOnTextArea = 0;

        var friendListLimit = module.config().friendListLimit;

        initialize:
        {
            Backbone.trigger('app:pageview', 'view/landing');
            
            Backbone.on({
                'tac_toggled': function (state) {

                    //When the terms and conditions is toggled
                    //we will keep a track of the status of this.

                    switch(state){
                        case "on":
                            tac_accepted = true;
                            break;

                        default:
                            tac_accepted = false;
                    }

                }

             });
        };

        Control.toggle_error_mark=function(name, state){


            switch(name){

                case "tacs":{

                    switch(state){
                        case true:
                            $('#home .check').removeClass('error');
                            break;

                        default:
                            $('#home .check').addClass('error');
                    }

                    break;
                }

            }
        };

        Control.showFriendBox=function(friends,device){

            var template = _.template($('#friend-row-tpl').html());
            var html = "";
            //var html = _.template(tpl, {url:'some uri;', thumb:'sume thumb'});
            var x=0;
            for(var i in friends.data){

                var friend = friends.data[parseInt(i)];
                html += template({ name: friend.name, uid:friend.uid, num:x, device:device });

                x++;
                if(x==3){ x=0;}
                if(i==friendListLimit){break;}

            }

            $('#friend_list_rows').html(html);

            Backbone.trigger('app:pageview', 'friends/showfriends');

        };

        Control.placeTaggedFriendPhoto=function(id){

            //keep this tagged person handy
            user_to_be_tagged = id;

            var taggedImgSrc = 'http://graph.facebook.com/'+
            user_to_be_tagged+'/picture?type=square';

            $("#tagged_person_image").attr('src',taggedImgSrc);
            $("#tagged_person_image").attr('data-id',user_to_be_tagged);
            $("#tagged_person_image").css('display','block');
            $('#add_f_button').css('display','none');
            $('.friend_plus').fadeIn();
            $('.page1 .tag_friend_area .friends_tagged').css('width', '156px');
            $('.page1 .tag_friend_area .tag_friend_area_desc').css('margin', '30px auto 20px auto');
            Backbone.trigger('app:pageview', 'friends/tagged');

        };

        Control.check_friendlist_input=function(device){

            //we dont want to do this everytime the user
            //enters characters but when the user stops for a brief
            //period. That's why we will clearTimeout since we will be setting
            //it a lot.
            clearTimeout(theTimeout);
            theTimeout = setTimeout(function(){
            var entry = ($('#friend_finder_input').val()).toLowerCase();
            var numOfChars = entry.length;
            var friends = Facebook.getFriendsObject();

            var friendRows = "";

            $('#friend_list_rows').html("");

            var rowCount = 0;



            var template = _.template($('#friend-row-tpl').html());
            var html = "";
            //var html = _.template(tpl, {url:'some uri;', thumb:'sume thumb'});
            var x=0;
            for(var i in friends.data){

                var friend = friends.data[parseInt(i)];

                var name = (friend.name).toLowerCase();

                    if((name).substring(0,numOfChars)===entry){

                html += template({
                    name: friend.name,
                    uid:friend.uid,
                    num:x,
                    device:device
                });

                x++;
                if(x==3){ x=0;}
                if(rowCount==friendListLimit){break;}

                rowCount++;
            }

            }

            $('#friend_list_rows').html(html);

            },200);
        };

        Control.toggle_recieve_email_updates=function(){

            if(recieveEmails == 0)
            {
                recieveEmails = 1;

                $( ".thanks_email_sliding_check" ).animate({
                  left: 40
                }, 200, "linear", function() {
                  $('.thanks_email_entry_cta_right').css('background-color', '#85dc4b');
                    data = {recieveemail: recieveEmails};
                    Control.send_email_updates_now(data);
                    Backbone.trigger('app:pageview', 'action/marketingyes');
                });
            }
            else
            {
                recieveEmails = 0;
                $( ".thanks_email_sliding_check" ).animate({
                  left: 3
                }, 200, "linear", function() {
                  $('.thanks_email_entry_cta_right').css('background-color', '#c2382f');

                  data = {recieveemail: recieveEmails};
                  Control.send_email_updates_now(data);
                  Backbone.trigger('app:pageview', 'action/marketingno');

                });
            }

            
        };

        Control.toggle_recieve_email_updates_mobile =function(){

            if(recieveEmails == 0)
            {
                recieveEmails = 1;

                left = '47%';
                $( ".m_thank_email_slider" ).animate({
                  left: left
                }, 200, "linear", function() {
                  $('.m_thank_email_right').css('background-color', '#85dc4b');
                    data = {recieveemail: recieveEmails};
                    Control.send_email_updates_now(data);
                    Backbone.trigger('app:pageview', 'action/marketingyes');
                });
            }
            else
            {
                recieveEmails = 0;
                left = '4%';
                $( ".m_thank_email_slider" ).animate({
                  left: left
                }, 200, "linear", function() {
                  $('.m_thank_email_right').css('background-color', '#c2382f');

                  data = {recieveemail: recieveEmails};
                  Control.send_email_updates_now(data);
                  Backbone.trigger('app:pageview', 'action/marketingno');

                });
            }

            
        };

        Control.send_email_updates_now = function(data){
        $.post( "user/recieveemails",data,
            function( response ) {

            });

        };
    
        Control.setIEclickedOnTextArea = function(value)
        {
            IEclickedOnTextArea = value;
        };

        Control.getIEclickedOnTextArea = function(data)
        {
            return IEclickedOnTextArea;
        };

        Control.send_email_updates = function(device, data){

            $('#email_entry').removeClass('error');

            if(!this.is_email_valid('mobile')){

                $('#email_entry').addClass('error');
                $('#email_entry').val('Enter a valid email');

                return;
            }else{
                this.send_email_updates_now(data);
            }

        };

        Control.is_email_valid = function(device)
        {
            switch(device){
                case "web":{
                    return this.check_email($('#user_email_address').val());
                    break;
                }

                default:{
                    return this.check_email($('#email_entry').val());
                }
            }

            return false;
        };

        Control.check_email = function(theEmail)
        {

                var theReturn=true;
                var emptyStrExp=/(^\s*$)/;
                if(emptyStrExp.test(theEmail)){theReturn=false;}
                
                if(theReturn){
                var EmailExp=/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                if(!EmailExp.test(theEmail)){theReturn=false;}
                }

                return theReturn;

        };


        Control.has_user_accepted_terms=function(){

            return tac_accepted;
        };

        Control.set_source_image_provider=function(provider)
        {

            this.source_image_provider = provider;

        };

        Control.get_image_short_url=function()
        {

            return image_short_url;

        };

        Control.getFacebookPhotos=function()
        {
            source_image_provider = 'facebook';

            Facebook.loadPhotos().done(function () {
                       // After successfully fetching facebook images, go to select image route
                        //Backbone.history.navigate('edit/select-image', true);
                    });

        };

        Control.adjust_viewport=function()
        {


        };

        Control.startLoader=function()
        {
            $('.loading').show();
            $('.popups').fadeIn();
            var opts = {
              lines: 11, // The number of lines to draw
              length: 14, // The length of each line
              width: 7, // The line thickness
              radius: 20, // The radius of the inner circle
              corners: 1, // Corner roundness (0..1)
              rotate: 0, // The rotation offset
              direction: 1, // 1: clockwise, -1: counterclockwise
              color: '#FFF', // #rgb or #rrggbb or array of colors
              speed: 1, // Rounds per second
              trail: 60, // Afterglow percentage
              shadow: false, // Whether to render a shadow
              hwaccel: false, // Whether to use hardware acceleration
              className: 'spinner', // The CSS class to assign to the spinner
              zIndex: 2e9, // The z-index (defaults to 2000000000)
              top: '50%', // Top position relative to parent
              left: '50%' // Left position relative to parent
            };
            
            var target = document.getElementById('loading');
            var spinner = new Spinner(opts).spin(target);
            
        };

        Control.stopLoader=function()
        {
            $('.popups').fadeOut();
            $('.loading').hide();
        };

        Control.apply_chosen_photo=function(device)
        {

            //first of all, if this is a mobile device we will
            //position the photo for the full view.
            var clone;
            if(device=="mobile"){
                this.preposition_for_mobile();

                clone = $('#pinching_handle').clone( true );

                $(clone).css({
                'width':$('#pinching_mask_img').width(),
                'height':$('#pinching_mask_img').height(),
                'top':$('#pinching_mask_img').position().top,
                'left':$('#pinching_mask_img').position().left
                });

            }
            else{
                clone = $('#backdrop_mask_img').clone( true );
            }

            //save photo in place
            
            $(clone).attr('id','cloned_img');
            if(device=="web"){
                $('#pic_display').html('');
                $('#pic_display').append(clone);
            }else{
                $('#pic_display_m').html('');
                $('#pic_display_m').append(clone);
            }

            //Load Appropriate Top Bar
            if(device=="mobile"){
                this.display_top_bar('mobile_bar_start_typing');
            }else{
                this.display_top_bar('finish_typing_bar');
            }

            Backbone.trigger('closeCropping',device);

            var isIE = Control.checkIfIE();
            if(isIE != 0)
            {
                $('.pic_display_ctas_textarea_input_ie').val('Write here');
            }
        };

        Control.preposition_for_mobile=function()
        {
            //Making sure we calculate the borders.
            //This is important especially when dealing
            //with percentages.

            var vp_borderLW = 
            $('.pinching_viewport').css("border-left-width");
            var vp_borderRW = 
            $('.pinching_viewport').css("border-right-width");
            var vp_borderTW = 
            $('.pinching_viewport').css("border-top-width");
            var vp_borderBW = 
            $('.pinching_viewport').css("border-bottom-width");


            //Removing the pixel's extension letters
            vp_borderLW = 
            parseFloat(vp_borderLW
                .substring(0,vp_borderLW.length-2));
            vp_borderRW = 
            parseFloat(vp_borderRW
                .substring(0,vp_borderRW.length-2));
            vp_borderTW = 
            parseFloat(vp_borderTW
                .substring(0,vp_borderTW.length-2));
            vp_borderBW = 
            parseFloat(vp_borderBW
                .substring(0,vp_borderBW.length-2));


            //the viewport width decreases since the borders
            //are on it. So we must make an identation for a
            //fake width.
            var viewWidth = 
            $('.pinching_viewport').width()+(vp_borderLW+vp_borderRW);

            //the viewport height decreases since the borders
            //are on it. So we must make an identation for a
            //fake height.
            var viewHeight = 
            $('.pinching_viewport').height()+(vp_borderTW+vp_borderBW);

            var viewOffset = $('.pinching_viewport').offset();

            var destWidth = $('#pic_display_m').width();
            var destHeight = $('#pic_display_m').height();

            var bckDropWidth = $('#pinching_backdrop').width();
            var bckDropHeight = $('#pinching_backdrop').height();

            var picDispOffset = $('#pic_display_m').offset(); 
            var bckdropOffset = $('#pinching_backdrop').offset();

            var pos = $('#pinching_mask_img').position();
            var imgWidth = $('#pinching_mask_img').width()+(vp_borderLW+vp_borderRW);
            var imgHeight = $('#pinching_mask_img').height()+(vp_borderTW+vp_borderBW);


            var diffInLeft = ((viewOffset.left+vp_borderLW) - picDispOffset.left);
            var diffInTop = ((viewOffset.top+vp_borderTW) - picDispOffset.top);



            var widerPerc = (destWidth/viewWidth)*100;
            var newImgWidth = ((widerPerc/100)*imgWidth);
            //We must fake the left and top positions as well since we
            //have borders on this element.
            //We are going to increase the left postion with the
            //width of the left border. Same applies for the
            //top position.
            var leftPerc = ((pos.left)/imgWidth)*100;
            var newLeft = ((leftPerc/100)*newImgWidth)-((widerPerc/100)*diffInLeft);


            var higherPerc = (destHeight/viewHeight)*100;
            var newImgHeight = ((higherPerc/100)*imgHeight);
            //We must fake the left and top positions as well since we
            //have borders on this element.
            //We are going to increase the top postion with the
            //width of the top border. Same applies for the
            //left position.
            var topPerc = ((pos.top)/imgHeight)*100;
            var newTop = ((topPerc/100)*newImgHeight)-((widerPerc/100)*diffInTop);


            var css = {
                top:newTop+"px",
                left:newLeft+"px",
                width:newImgWidth+"px",
                height:'auto'
            };

            $('#pinching_mask_img').css(css);

        }

        Control.stop_cta_blinking=function()
        {
            //We have a very im[portant thing to do here
            //because this was animating, we must first stop
            //the animation
            $( ".pic_display_ctas_pic_button" ).stop();

            //once stopped, we need to reset the ctas elements.
            this.reset_ctas_elements();

            $('#ctas_pic').hide();
        };

        Control.stop_m_cta_blinking=function()
        {

        };

        Control.reset_ctas_elements=function()
        {
            //once stopped, we need to reset the ctas elements.
            $( ".pic_display_ctas_pic_button" )
            .css({width:'48px', height:'38px'});

            $( ".pic_display_ctas_pic_glow" ).css('opacity',0);
        };

        Control.stop_front_scroller=function()
        {
            //we will stop and reset the
            //photo scolling here
            $('#slides').stop();
        };

        Control.gettextarea_ready=function(device)
        {
            if(device=="mobile"){
            $('#cta_exp_extry').stop();
            $('#cta_exp_extry')
            .css('opacity',1);
            }else{
            $('.pic_display_ctas_textarea_input').stop();
            $('.pic_display_ctas_textarea_input').css('opacity',1);
            $('.pic_display_ctas_textarea_input').css('font-style','normal');
            }
        };

        Control.display_tagging_friends=function()
        {
            $('#add_f_button').stop();
            $('#add_f_button').animate({opacity:1});

            $('.no_thanks_button_area').css('display','block');
        };

        Control.load_home_pictures=function(res , device)
        {

            var photo , num = 0,
                //rsPhoto = res[0]['fql_result_set'],
                //rsPhotoSrc = res[1]['fql_result_set'],
                //depending on which device loaded called this
                //we will load the appropriate
                fb_tpl = _.template($('#fb-photo-tpl').html()),
                html = "", mob_count = 0, web_count = 0,
                friend_interior = $('.friend_interior');

            //Assign title to the popup.
            $('#friends_box #friendbox_title').text('Our Library');
            friend_interior.html('');


            for(i in res)
            {

                photo = res[i];
                    html += fb_tpl({
                        src_big: photo.url,
                        src_small:photo.thumb,
                        device:device,
                        num:num,
                        mb_last:mob_count==2?true:false,
                        wb_last:web_count==3?true:false
                    });

                    num++;
                    web_count++;
                    mob_count++;
                    if(num==4){num = 0;}
                    if(mob_count==3){mob_count = 0;}
                    if(web_count==4){web_count = 0;}
            }

            friend_interior.html(html);
            Backbone.trigger('showPopup','photos');
        };

        Control.load_instagram_pictures=function(res , device)
        {

            /*var photo , num = 0,
                //rsPhoto = res[0]['fql_result_set'],
                //rsPhotoSrc = res[1]['fql_result_set'],
                //depending on which device loaded called this
                //we will load the appropriate
                fb_tpl = _.template($('#fb-photo-tpl').html()),
                html = "", mob_count = 0, web_count = 0,
                friend_interior = $('.friend_interior');

            //Assign title to the popup.
            $('#friends_box #friendbox_title').text('Our Library');
            friend_interior.html('');


            for(i in res)
            {

                photo = res[i];
                    html += fb_tpl({
                        src_big: photo.url,
                        src_small:photo.thumb,
                        device:device,
                        num:num,
                        mb_last:mob_count==2?true:false,
                        wb_last:web_count==3?true:false
                    });

                    num++;
                    web_count++;
                    mob_count++;
                    if(num==4){num = 0;}
                    if(mob_count==3){mob_count = 0;}
                    if(web_count==4){web_count = 0;}
            }

            friend_interior.html(html);
            Backbone.trigger('showPopup','photos');*/
        };

        Control.load_facebook_pictures=function(res , device)
        {


            //ref - FacebookPhotos.addPhotos()
            var photo , num = 0,
                rsPhoto = res,//res[0]['fql_result_set'],
               // rsPhotoSrc = res[1]['fql_result_set'],
                //depending on which device loaded called this
                //we will load the appropriate
                fb_tpl = _.template($('#fb-photo-tpl').html()),
                html = "", mob_count = 0, web_count = 0,
                friend_interior = $('.friend_interior');

            //Assign title to the popup.
            $('#friends_box #friendbox_title').text('Facebook Images');
            friend_interior.html('');


                for (i in rsPhoto) {

                    photo = rsPhoto[i];
                    html += fb_tpl({
                        src_big: photo.src_big,
                        src_small:photo.src_small,
                        device:device,
                        num:num,
                        mb_last:mob_count==2?true:false,
                        wb_last:web_count==3?true:false
                    });

                    num++;
                    web_count++;
                    mob_count++;
                    if(num==4){num = 0;}
                    if(mob_count==3){mob_count = 0;}
                    if(web_count==4){web_count = 0;}

                }


            friend_interior.html(html);
            Backbone.trigger('showPopup','photos');
            

        };

        Control.display_tag_friend_area=function(button)
        {
            $('.tag_button_area').css('display','block');
            $('#turning_back_block').css('display','block');
        };

        Control.display_top_bar=function(bar)
        {

            $('#add_pic_bar').css('display','none');
            $('#finish_typing_bar').css('display','none');
            $('#bring_a_friend_bar').css('display','none');
            $('#move_and_scale_bar').css('display','none');
            $('#finish_entry_bar').css('display','none');
            $('#select_source_bar').css('display','none');
            $('#mobile_bar_bring_along').css('display','none');
            $('#mobile_bar_finish_creating').css('display','none');
            $('#mobile_bar_start_typing').css('display','none');
            $('#mobile_move_and_scale_bar').css('display','none');

            switch(bar){

                case "move_and_scale_bar":
                    $('#move_and_scale_bar').css('display','block');
                    break;

                case "bring_a_friend_bar":
                    $('#bring_a_friend_bar').css('display','block');
                    break;

                case "finish_typing_bar":
                    $('#finish_typing_bar').css('display','block');
                    break;

                case "add_pic_bar":
                    $('#add_pic_bar').css('display','block');
                    break;

                case "finish_entry_bar":
                    $('#finish_entry_bar').css('display','block');
                    break;

                case "select_source_bar":
                    $('#select_source_bar').css('display','block');
                    break;

                case "mobile_bar_bring_along":
                    $('#mobile_bar_bring_along').css('display','block');
                    break;

                case "mobile_bar_finish_creating":
                    $('#mobile_bar_finish_creating').css('display','block');
                    break;

                case "mobile_bar_start_typing":
                    $('#mobile_bar_start_typing').css('display','block');
                    break;

                case "mobile_move_and_scale_bar":
                    $('#mobile_move_and_scale_bar').css('display','block');
                    break;
            }
        };

        Control.loadMainPhoto=function(uid)
        {
            $('#fb_thumb').attr('src',
                'http://graph.facebook.com/'+uid+'/picture?type=square');
            //http://graph.facebook.com/" + facebookId + "/picture?type=square
        };

        Control.save_photo_details=function(device, acc)
        {
            var self = this;
            this.getPhotoDimensions($('#cloned_img').attr('src'))
            .done(function(picData){

                self.generatePhoto(picData,device,acc);

            });

        };

        Control.getPhotoDimensions=function(photo_url)
        {
            var defer = new $.Deferred();

            var self = this;
            var data = {
                'photo_url':photo_url
            };

            $.post( "app/realdimensions",data,
            function( data ) {
                if(typeof(data.width)=="undefined"){
                    defer.resolve($.parseJSON(data));
                }else{
                    defer.resolve(data);
                }
                
            });

            return defer.promise();
        };

        Control.actions_upon_start=function()
        {
            $('#home_content').css('display','none');
            this.display_top_bar('add_pic_bar');
            $('.page1').css('display','block');

            Backbone.trigger('blink_m_photo_call_to_action');
        };

        Control.set_source_image_id=function(imageID)
        {
            this.source_image_id = imageID;

        };

        Control.get_source_image_provider=function(imageID)
        {
            return this.source_image_provider;
        };

        Control.checkIfIE=function(imageID)
        {
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer, return version number
                return(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
            else                 // If another browser, return 0
               return 0;
        };

        Control.generatePhoto=function(picData,device, acc)
        {
            var self = this,
                entrytext,
                viewPortWidth,
                viewPortHeight;

            switch(device){
                case "web":
                    entrytext = $('#entry_text').val();
                    viewPortWidth = $('#pic_display').width();
                    viewPortHeight = $('#pic_display').height();
                    break;
                
                default:
                    entrytext = $('#cta_exp_extry').val();
                    viewPortWidth = $('#pic_display_m').width();
                    viewPortHeight = $('#pic_display_m').height();
                    break;
                }




            var upperLeft=0, lowertop=0;
            var naturalImageDimension = {};
            naturalImageDimension.width = picData.width;
            naturalImageDimension.height = picData.height;


            var vw = module.config().picEndWidth;
            var vh = module.config().picEndHeight;
            
            var cloned_img_dimensions = {};
            var cloned_img_positions = $('#cloned_img').position();

            cloned_img_dimensions.left = cloned_img_positions.left;
            cloned_img_dimensions.top = cloned_img_positions.top;

            cloned_img_dimensions.width = $('#cloned_img').width();
            cloned_img_dimensions.height = $('#cloned_img').height();

            cloned_img_dimensions.right = cloned_img_dimensions.left + cloned_img_dimensions.width;
            cloned_img_dimensions.bottom = cloned_img_dimensions.top + cloned_img_dimensions.height;


            var leftPerc = ( (cloned_img_dimensions.left*-1) / cloned_img_dimensions.width) * 100;
            var newLeft = (leftPerc/100) * naturalImageDimension.width;
            /*if(newLeft<0){newLeft=0;}
            if(newLeft>naturalImageDimension.width){newLeft=naturalImageDimension.width;}*/

            var topPerc = ( (cloned_img_dimensions.top*-1) / cloned_img_dimensions.height) * 100;
            var newTop = (topPerc/100) * naturalImageDimension.height;
            if(newTop<0){newTop=0;}
            if(newTop>naturalImageDimension.height){newTop=naturalImageDimension.height;}





            var sm_width, sm_height;
            if((cloned_img_dimensions.left+cloned_img_dimensions.width ) < 0){
                upperWidth = 0;
            }else{

                if(cloned_img_dimensions.left>viewPortWidth){
                    upperWidth = 0;
                }else{
                    sm_width = viewPortWidth;
                    if(sm_width>(cloned_img_dimensions.width-cloned_img_dimensions.left)){
                        sm_width = sm_width -(cloned_img_dimensions.width-cloned_img_dimensions.left);
                    }

                    widthPerc = (sm_width/cloned_img_dimensions.width)*100;
                    upperWidth = (widthPerc/100)*naturalImageDimension.width;

                }
            }





            if((cloned_img_dimensions.top+cloned_img_dimensions.height ) < 0){
                lowerHeight = 0;
            }else{

                if(cloned_img_dimensions.top>viewPortHeight){
                    lowerHeight = 0;
                }else{
                    sm_height = viewPortHeight;
                    if(sm_height>(cloned_img_dimensions.height-cloned_img_dimensions.top)){
                        sm_height = sm_height -(cloned_img_dimensions.height-cloned_img_dimensions.top);
                    }

                    heightPerc = (sm_height/cloned_img_dimensions.height)*100;
                    lowerHeight = (heightPerc/100)*naturalImageDimension.height;

                }
            }









            /*var percWidth = ( ( (cloned_img_dimensions.left*-1) + viewPortWidth) / cloned_img_dimensions.width) * 100;
            var upperWidth = (percWidth/100) * naturalImageDimension.width;
            if((newLeft+upperWidth)>naturalImageDimension.width){
                upperWidth = naturalImageDimension.width-newLeft;
            }

            var percHeight = ( ( (cloned_img_dimensions.top*-1) + viewPortHeight) / cloned_img_dimensions.height) * 100;
            var lowerHeight = (percHeight/100) * naturalImageDimension.height;
            if((newTop+lowerHeight)>naturalImageDimension.height){
                //lowerHeight = naturalImageDimension.height-newTop;
            }*/
             var tag = $('#tagged_person_image').attr('data-id');

            var str = $('#entry_text').val();
            if (str==""){str=".";}

            if(this.source_image_provider == 'stock')
            {
                Backbone.trigger('app:pageview', 'imageused/ourlibrary');
            }
            else if(this.source_image_provider == 'facebook')
            {
                Backbone.trigger('app:pageview', 'imageused/facebook');
            }
            else if(this.source_image_provider == 'instagram')
            {
                Backbone.trigger('app:pageview', 'imageused/instagram');
            }

            var imageId = 
            (typeof(this.source_image_id)!="undefined")?
            this.source_image_id:1;

            var data = {
                'text': str, //@todo validation
                'provider': this.source_image_provider,
                'image_id': this.source_image_id,
                'image_url': $('#cloned_img').attr('src'),
                'x': (newLeft).toFixed(2),
                'y': (newTop).toFixed(2),
                'width': upperWidth.toFixed(2),
                'height': lowerHeight.toFixed(2),
                'tags': tag,
                'acc': acc
            };


            


            if(device == "web"){


                $.post(picturePostingUrl, data).done(function (result)
                {

                    //Just incase this browser is IE, we want to make sure
                    //that this data is parsed, so we will use $.parseJSON
                    //where it is undefined.

                    if(typeof(result.src)=="undefined"){
                        result = $.parseJSON(result);
                    }


                    image_short_url = result.url;

                    Facebook.postPhoto(result.src);


                          //Set the image from the backend to show
                          //on the client side.
                          $('#cloned_img').attr('src',result.src);

                          //Apply specific style to this element as we will
                          //reposition it to fit the new view.
                          $('#cloned_img').css({
                            'width':'100%',
                            'height':'120%',
                            'top':'0',
                            'left':'0'
                            });

                          //We are done with this, so we will hide it.
                          $('.pic_display_ctas').css('display','none');

                          //Adjust the size of the cropped image container
                          $('.pic_display_case .pic_display').css({
                            'width':'341px',
                            'height':'315px'
                            });

                          //Adjust the size of the entire display case.
                          $('.pic_display_case').css({
                            'display':'block',
                            'padding':'129px 0 0 44px'
                            });

                        Backbone.trigger('view_final_screen');


                });

            }else{
                setTimeout(function(){

                    $('#tag_gray_area').css('display','none');
                        $('.add_picture_pocket_m').css('display','none');
                        $('.thanks_main_window_m').css('display','none');
                        $('.m_thank_you').css('display','block');

                        var body = $("html, body");
                        body.animate({scrollTop:0}, '500', 'swing', function() { 
                           Control.stopLoader();
                           //Backbone.trigger('app:pageview', 'view/thankyou');

                           $.post(picturePostingUrl, data).done(function (result){

                                //we dont need to do anything here since we have already
                                //shown the final screen.
                                Facebook.postPhoto(result.src);
                            
                            });

                        });

                },3333);
            }


        };

        Control.show_source_button = function(device){

            switch(device){
                case 'mobile':{

                    $('.add_picture_pocket_m_backdrop')
                    .css("background-color","#242733");

                    
                    Control.display_top_bar('select_source_bar');

                    
                    $('#cta_action_glow_area_m').hide();
                    $('.photo_cta_area').css({
                        'top':'auto', 'bottom':'0%'
                    });

                    $('.m_source_buttons').show();

                    break;
                }

                default :{

                }
            }
        };

        Control.prepare_back_to_source = function(device){

            switch(device){
                case 'mobile':{
                    //Remove all images used
                    $('#pinching_mask_img').attr('src','');
                    $('#pinching_handle').attr('src','');

                    this.resetCroppingTool(device);

                    $('#mobile_pinching').hide();

                    break;
                }

                default :{

                }
            }

        };

        Control.resetCroppingTool = function(device){

                var css, cssH;
                css = {
                            'width':'100%',
                            'height':'auto',
                            'left':0,
                            'top':0
                        };

            switch(device){
                case 'mobile':{

                    //Remove all images used
                    $('#pinching_mask_img').attr('src','');
                    $('#pinching_handle').attr('src','');

                    //Reset the handles and holders.
                    $('#pinching_backdrop').css(css);
                    $('#pinching_handle').css(css);
                    $('#pinching_mask_img').css(css);
                    $('#pinching_holder').css(css);

                    break;

                }

                default :{

                }
            }

        };

        return Control;
    });