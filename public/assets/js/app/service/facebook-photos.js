define(
    [
        'module',
        '//connect.facebook.net/en_GB/all.js',
        'backbone',
        'app/model/remote-images'
    ],
    function (module, sdk, Backbone, RemoteImagesCollection) {

        var collection = new RemoteImagesCollection(),
            nextMaxPhotoId = 0;

        var getFql = function () {

            var photoQuery;
            var minHeight = module.config().finalHeight,
                minWidth = module.config().finalWidth,
                maxId = module.config().maxId,
                paginationLimit = module.config().paginationLimit,
                maxIdFql = "",//nextMaxPhotoId !== 0 ? "AND object_id < " + maxId : '',
                srcQuery = "SELECT src, photo_id FROM photo_src WHERE width='180' AND photo_id IN (SELECT object_id FROM #query1)",
                photoQuery;



                srcQuery = "SELECT src, photo_id FROM photo_src WHERE photo_id IN (SELECT object_id FROM #query1)",


            photoQuery = "SELECT src_big, src_small, object_id FROM photo WHERE src_big_height > '" + minHeight + "' " + maxIdFql;
            photoQuery += " AND src_big_width > '" + minWidth + "' AND album_object_id IN (SELECT object_id FROM album WHERE owner=me())";
            photoQuery += " ORDER BY created DESC LIMIT " + paginationLimit;


            photoQuery = "SELECT src_big, src_small, object_id FROM photo WHERE "
            photoQuery += "album_object_id IN (SELECT object_id FROM album WHERE owner=me())"
            photoQuery += " ORDER BY created DESC";

            return {
                'query1': photoQuery,
                'query2': srcQuery
            };

        };

        var getAllPhotos = function () {

            return "SELECT src_big, src_small, object_id FROM photo WHERE album_object_id IN (SELECT object_id FROM album WHERE owner=me()) ORDER BY created DESC";

        };

        var parseResult = function (result) {
            var photos = {},
                photo,
                i, j,
                rsPhoto = result[0]['fql_result_set'],
                rsPhotoSrc = result[1]['fql_result_set'];

            for (i in rsPhoto) {
                photo = rsPhoto[i];
                photos[photo.object_id] = {
                    id: photo.object_id,
                    thumb: photo.src_small,
                    url: photo.src_big
                };
            }

            for (j in rsPhotoSrc) {
                photo = rsPhotoSrc[j];
                photos[photo.photo_id].thumb = photo.src;
            }

            nextMaxPhotoId = rsPhoto.length > 0 ? rsPhoto[i].object_id : 0;
            
            return $.map(photos, function (value, index) {
                return [value];
            });

        };

        var load = function () {
            var defer = new $.Deferred();

            /*FB.api({
                method: 'fql.multiquery',
                queries: getFql()
            }, function (result) {


                if (result[0] && result[0]['fql_result_set']) {
                    defer.resolve(result);

                } else {

                    defer.reject();
                }
            });*/




            FB.api({
                method: 'fql.query',
                query: getAllPhotos()
            }, function (result) {
                if (result[0] && result[0]['src_big']) {
                    defer.resolve(result);

                } else {

                    defer.reject();
                }
            });

            return defer.promise();
        };


        var facebookPhotos = {
            load: function () {
                return load();
            }
        };

        _.extend(facebookPhotos, Backbone.Events);

        collection.on('add', function (model) {
            facebookPhotos.trigger('image:added', model);
        });

        return facebookPhotos;

});