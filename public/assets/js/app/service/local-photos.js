define([
        'module',
        'backbone',
        'app/model/remote-images'
    ],
    function (module, Backbone, RemoteImagesCollection) {

        var collection = new RemoteImagesCollection();

        var localPhotos = {

            get_local_shots:function()
            {
                var defer = new $.Deferred();
                $.get( "/api/stockimages", function (data) {
                

                    defer.resolve(data);

                }).fail(function () {
                    defer.reject();
                });

                return defer.promise();

                /*


            FB.api({
                method: 'fql.multiquery',
                queries: getFql()
            }, function (result) {


                if (result[0] && result[0]['fql_result_set']) {
                    defer.resolve(result);

                } else {

                    defer.reject();
                }
            });

            return defer.promise();
                */
            },

            get_local_photos:function()
            {
                //var defer = new $.Deferred();
                source_image_provider = 'stock';
                
                $.get( "/api/stockimages", function (data) {

                    var friend_interior = $('.friend_interior');
                    
                    friend_interior.html('');
                    var html = '';
                    var count = 0;


                    
                    for (var i in data) 
                    {
                        
                        /*collection.add({
                            id: data[i].id,
                            url: data[i].url,
                            thumb: data[i].thumb || ''
                        });*/

                        var photos = data[parseInt(i)];

                        c="";
                        if(count==3){c="last";}
                        if(count==0){html += '<ul>';}

                        html += '<li class="selected_image_web selecting_image '+c+'" '+
                        'data-big="'+photos.url+'" data-small="'+photos.thumb+'" data-id="' + photos.id +'">'+
                        '<img src="'+photos.thumb+'" border="0" width="100%">'+
                        '</li>';
                
                        //friend_interior.append('ul');
                        //collection.add(photos[i]);
                        count++;
                        if(count>3||i==(photos.length-1))
                        {
                            html +='</ul>';
                            count=0;
                        }
                    }

            //$('#friendbox_title').text('Instagram Images');
            $('#friends_box #friendbox_title').text('Our Library');
            friend_interior.append(html);
            Backbone.trigger('showPopup','photos');
                    
                    //defer.resolve(collection);

                }).fail(function () {
                    //defer.reject();
                });



                /*var defer = new $.Deferred()

                collection.fetch().done(function () {
                    defer.resolve(collection);
                }).fail(function () {
                    defer.reject();
                }); 

                return defer.promise();*/
            }

        };

        _.extend(localPhotos, Backbone.Events);

        /*collection.on('add', function (model) {
            localPhotos.trigger('image:add', model);
        });*/

        return localPhotos;
    }
);