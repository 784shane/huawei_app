define([
        'module',
        'backbone',
        'app/model/remote-images'
    ],
    function (module, Backbone, RemoteImagesCollection) {
    
        var loginDeferred,
            isPopupClosedInterval,
            accessToken = module.config().accessToken,
            isMobileSafariWebview = false, // @todo detect mobile safari webview
            nextMaxId = null,
            recentMediaUrl = 'https://api.instagram.com/v1/users/self/media/recent/';

        var collection = new RemoteImagesCollection();

        var mobileLogin = function () {

                    var url = "https://instagram.com/oauth/authorize/?"+
                    "client_id="+module.config().clientId+
                    "&redirect_uri="+module.config().redirectUrl+
                    "&response_type=token";

                    window.top.location=url;


        };

        var login = function () {
            loginDeferred = new $.Deferred();

            if (accessToken) {
                loginDeferred.resolve();
            } else {
                if (!isMobileSafariWebview) {
                    // Client-side flow
                    openPopup();
                } else {
                    // Server-side flow
                    window.location.href = getLoginUrl('code');
                }
            }

            return loginDeferred.promise();
        };

        var getLoginUrl = function (type) {
            var loginUrl = 'https://instagram.com/oauth/authorize/?client_id=';
            loginUrl += module.config().clientId + '&redirect_uri=' + module.config().redirectUrl;
            loginUrl += '&response_type=' + type;
            return loginUrl;

            /*var loginUrl = 'https://instagram.com/oauth/authorize/?client_id=';
            loginUrl += 'd853e9856ea34eafb681b17965080bbb&redirect_uri=http://huawei.maido/login/instagram';
            loginUrl += '&response_type=' + type;
            return loginUrl;*/
        };

        var openPopup = function () {
            source_image_provider = 'instagram';
            var popup,
                width = 640,
                height = 410;

            popup = window.open('', 'igAuth', 'status=1');
            popup.moveTo((screen.width - width) / 2, (screen.height - height) / 2);
            popup.resizeTo(width, height);
            popup.location.href = getLoginUrl('token');

            if (popup) {
                // Check if the user cancelled
                clearInterval(isPopupClosedInterval);
                
                isPopupClosedInterval = setInterval(function () {
                    if (popup.closed) {
                        handleLoginResult({
                            success: false,
                            cancelled: true,
                            message: 'User cancelled'
                        });
                    }
                }, 100);
            }
        };

        var addPhotos = function (photos) {
            
            for (var i in photos) {
                photo = photos[i].images;
                collection.add({
                    'id': photos[i]['id'],
                    'url': photo['standard_resolution']['url'],
                    'thumb': photo['low_resolution']['url']
                });
            }
        };

        var LoadMobilePhotos = function (device) {
            accessToken = instagram_access_token;
            var tpl = _.template($('#instagram-photo-tpl').html());
            var defer = new $.Deferred();

            $.ajax({
                url: recentMediaUrl,
                type: 'GET',
                data: {
                    'access_token': accessToken,
                    'max_id': nextMaxId || '',
                    'count': 100
                },
                dataType: 'jsonp',
                cache: false

            }).always(function (result) {


            var friend_interior = $('.friend_interior');
            friend_interior.html('');
            var html = '';
            var count = 0;
            var num = 0;
            
            for(var i in result.data) {

                var photos = result.data[parseInt(i)];
                    html += tpl({
                        standard_resolution:photos.images.standard_resolution.url,
                        low_resolution:photos.images.low_resolution.url,
                        thumb:photos.images.thumbnail.url,
                        device:device,
                        num:count,
                        mb_last:count==2?true:false,
                        wb_last:count==3?true:false
                    });
                
                count++;
                if(count>3||i==(result.data.length-1)){
                    count=0;
                }


            }



            $('#friendbox_title').text('Instagram Photos');
            friend_interior.append(html);
            
            defer.resolve();

                /*if (result && result.data) {
                    if (result.data.length) {
                        addPhotos(result.data);
                        nextMaxId = result.pagination.next_max_id;
                    } else {
                        nextMaxId = false;
                    }
                    defer.resolve(collection);
                } else {
                    defer.reject();
                }*/
            });

            return defer.promise();

        };


        var loadPhotos = function () {
            var defer = new $.Deferred();

            $.ajax({
                url: recentMediaUrl,
                type: 'GET',
                data: {
                    'access_token': accessToken,
                    'max_id': nextMaxId || '',
                    'count': 100
                },
                dataType: 'jsonp',
                cache: false

            }).always(function (result) {



            var friend_interior = $('.friend_interior');
            friend_interior.html('');
            var html = '';
            var count = 0;
            
            for(var i in result.data) {

var photos = result.data[parseInt(i)];


            c="";
            if(count==3){c="last";}
            if(count==0){html += '<ul>';}

html += '<li class="selected_image_web selecting_image '+c+'" '+
'data-big="'+photos.images.standard_resolution.url+'" data-small="'+photos.images.thumbnail.url+'">'+
'<img src="'+photos.images.low_resolution.url+'" border="0" width="100%">'+
'</li>';
                
                //friend_interior.append('ul');
                //collection.add(photos[i]);
                count++;
                if(count>3||i==(result.data.length-1))
                {
html +='</ul>';
                    count=0;
                }


            }

            $('#friendbox_title').text('Instagram Images');
            friend_interior.append(html);
            Backbone.trigger('showPopup','photos');
                /*if (result && result.data) {
                    if (result.data.length) {
                        addPhotos(result.data);
                        nextMaxId = result.pagination.next_max_id;
                    } else {
                        nextMaxId = false;
                    }
                    defer.resolve(collection);
                } else {
                    defer.reject();
                }*/
            });

            return defer.promise();
        };

        var handleLoginResult = function (result) {

            clearInterval(isPopupClosedInterval);
            if (result.success) {
                accessToken = result.accessToken;
                loginDeferred.resolve();
            } else {
                if (!result.cancelled) {
                    Backbone.trigger('app:error', 'Instagram authentication error: ' + result.message);
                }
                loginDeferred.reject();
            }
        };

        // The popup window needs a global accesor
        window.instagramLoginResponse = function (result) {
            handleLoginResult(result);
        };

        var service = {
            login: function () {
                return login();
            },

            loadPhotos: function () {
                return login().then(loadPhotos);
            },

            LoadMobilePhotos: function (device) {
                return LoadMobilePhotos(device);
            },

            mobileLogin: function () {
                mobileLogin();
            }
        };

        collection.on('add', function (model) {
            service.trigger('image:added', model);
        });

        _.extend(service, Backbone.Events);

        return service;
    }
);