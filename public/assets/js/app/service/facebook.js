define(
    [
        'module',
        '//connect.facebook.net/en_GB/all.js',
        'backbone',
        'app/service/facebook-photos'
    ],
    function (module, sdk, Backbone, FacebookPhotos) {

        var me = {accessToken:null,signedRequest:null,userID:null},
            permissions = [],
            isIOSChrome =  navigator.userAgent.match('CriOS');

        var friends = {};

        var getLoginStatus = function () {
            var defer = new $.Deferred();

            FB.getLoginStatus(function (response) {
                if (response.authResponse) {

                    // successful login
                    me.accessToken = response.authResponse.accessToken;
                    me.signedRequest = response.authResponse.signedRequest;
                    me.userID = response.authResponse.userID;
                    defer.resolve();
                } else {
                    defer.reject();
                }
            });

            return defer.then(getMe);
        };

        var checkLoggedIn = function()
        {
            var defer = new $.Deferred();
            return defer;
        };

        var login = function ( perms , msg_func ) {
            var defer = new $.Deferred();

            if (perms === undefined || perms == []) {
                perms = module.config().appScope;
            }

            if (isIOSChrome) {
                forceRedirect(perms);
                return;
            }

            FB.getLoginStatus(function (response) {
                if (response.authResponse) {

                    me.accessToken = response.authResponse.accessToken;
                    me.signedRequest = response.authResponse.signedRequest;
                    me.userID = response.authResponse.userID;

                    //user is already logged in
                    if(msg_func){Backbone.trigger(msg_func);}

                    return false;
                } else {

                    FB.login(function (response) {

                        if (response.authResponse) {
                            // successful login
                            me.accessToken = response.authResponse.accessToken;
                            me.signedRequest = response.authResponse.signedRequest;
                            me.userID = response.authResponse.userID;
                            defer.resolve();
                    
                    if(msg_func){Backbone.trigger(msg_func);}

                        } else {
                            defer.reject();
                        }

                        if(msg_func){Backbone.trigger(msg_func);}

                    }, {
                        scope:perms.join(',')
                    });

                }
            });


            
            return defer.then(getMe);
            
        };

        var loginForPermission = function ( perms , msg_func, device ) {

            

            var defer = new $.Deferred();

            if (perms === undefined || perms == []) {
                perms = module.config().appScope;
            }

            if (isIOSChrome) {
                forceRedirect(perms);
                return;
            }




            FB.login(function (response) {

                if (response.authResponse) {
                    // successful login
                    me.accessToken = response.authResponse.accessToken;
                    me.signedRequest = response.authResponse.signedRequest;
                    defer.resolve();

                    if(msg_func){Backbone.trigger(msg_func,device);}

                } else {
                    defer.reject();

                    if(msg_func){Backbone.trigger(msg_func,device);}
                }

            }, {
                scope:perms.join(',')
            });

        


            
            return defer.then(getMe);
            
        };

        var forceRedirect = function (perms) {
            var h = 'https://www.facebook.com/dialog/oauth?client_id=';
            h += module.config().appId + '&redirect_uri=';
            h += encodeURI(module.config().redirectUrl) + '&scope=';
            h += perms.join(',');

            window.top.location.href = h;
        };

        var getPhotos = function () {
            
        };

        var getMe = function () {
            var defer = new $.Deferred();

            FB.api('/me', function (response) {
                if (response.name !== null){
                    me = $.extend({}, me, {
                        firstname: response.first_name,
                        lastname: response.last_name,
                        email: response.email,
                        fbid: response.id
                    });
                    defer.resolve();
                } else {
                    defer.reject();
                }
            });

            return defer.promise();
        };

        var hasPermissions = function (perms) {
            var i, hasPermissions = true;
            for (i in perms) {
                if (!permissions[perms[i]]) {
                    hasPermissions = false;
                }
            }
            return hasPermissions;
        };

        var requestPermissions = function (perms) {

            var defer = new $.Deferred();

            if (!hasPermissions(perms)) {
                defer = login(perms);
            } else {
                defer.resolve();
            }

            return defer.promise();
        };

        var requestSpecificPermissions = function (perm,device) {

            //var defer = new $.Deferred();

            FB.api('/me/permissions', function (response) {

                //'user_photos'
                //getPhotosNow
                if(response.data[0][perm]
                    &&response.data[0][perm]==1){

                    Backbone.trigger('getPhotosNow',device);
                    return false;

                } else {

                   loginForPermission([perm],'getPhotosNow',device);
                    return false;
                }
            } );

            return false;
        };

        var list_my_friends = function (device) {

            /*FB.api('/me/friendlists', function (response) {
                Backbone.trigger('friends_retrieved',response);
            });*/

            query = 'SELECT uid, first_name, last_name, name '+
            'FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1=me()) order by name';

            FB.api("/fql", {q: query}, function(frenz){

                            //save the friends
                            friends = frenz;

                            Backbone.trigger('friends_retrieved',friends,device);
            });

            return false;
        };

        var basicLogin = function (perms) {
            var defer = new $.Deferred();


            FB.login(function (response) {

                if (response.authResponse) {
                    // successful login
                    me.accessToken = response.authResponse.accessToken;
                    me.signedRequest = response.authResponse.signedRequest;
                     
                    getPermissions().done(function () {
                        if (hasPermissions(perms)) {
                            defer.resolve();
                        } else {
                            defer.reject();
                        }    
                    });
                    

                } else {
                    defer.reject();
                }

            }, {
                scope: perms.join(',')
            });


            return defer.promise();
        };

        var postPhoto = function (src)
        {
            var def = new $.Deferred(),
                data = {
                    'url': src,
                    'user_generated': true,
                    'from': module.config().appId
                };

            FB.api('/me/photos', 'post', data, 
                function(response){
                    if (response && response.id) {
                        def.resolve();
                    } else {
                        def.reject();
                    }
                }
            );

            return def.promise();
        };
        // Query Facebook for permissions
        var getPermissions = function () {
            var defer = new $.Deferred();

            FB.api('/me/permissions', function (response) {
                if (response && response.data) {
                    permissions = response.data[0];
                }

                defer.resolve(permissions);
            });

            return defer.promise();
        };

        var Facebook = {
            login: function (perms) {
                return login(perms);
            },
            getLoginStatus: function (perms) {
                return getLoginStatus();
            },
            allow: function (perms) {
                
                var defer = new $.Deferred();

                if (!hasPermissions(perms)) {
                    defer = basicLogin(perms);
                } else {
                    defer.resolve();
                }

                return defer.promise();

            },
            feed: function (attrs) {
                return requestPermissions(['publish_stream']).then(function () {
                    return feed(attrs);
                });
            },

            loadPhotos: function (device) {
                requestSpecificPermissions('user_photos',device);
            },
            
            loadPhotosAfterPerms: function () {

                return FacebookPhotos.load();
            },
            postPhoto: function (src) {
                return requestPermissions(['publish_stream']).then(function () {
                    return postPhoto(src);
                });
            },
            share: function (attrs) {
                var defer = new $.Deferred(),
                    attributes = $.extend({
                        method: 'feed'
                    }, attrs, {});

                FB.ui(attributes, function (response) {
                    if (response) {
                        Backbone.trigger('app:pageview', 'share/facebookyes');
                        defer.resolve();
                    } else {
                        defer.reject();
                        Backbone.trigger('app:pageview', 'share/facebookno');
                    }
                });

                return defer;
            },

            init: function(){

                FB.init({
                    appId: module.config().appId,
                    status: true,
                    cookie: true,
                    frictionlessRequests: true,
                    xfbml  : true
                });

                FB.Event.subscribe('xfbml.render', function()
                {
                    FB.Canvas.setSize();
                });

                getLoginStatus();
                

            },

            getMeObject:function () {
                return me;
            },

            getFriendsObject:function () {
                return friends;
            },

            isUserLoggedIn:function (arr,msg) {
                login(arr,msg);
            },

            getMainPhoto:function (arr,msg) {
                login(arr,msg);
            },

            get_friends:function (device) {

                //FB.login(function(response) {
                        list_my_friends(device);
                    //}, {scope: 'read_friendlists'});


            }


        };

        FacebookPhotos.on('image:added', function (model) {
            Facebook.trigger('image:added', model);
        });

        _.extend(Facebook, Backbone.Events);

        return Facebook;
});