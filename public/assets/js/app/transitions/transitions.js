define(
    [
        'backbone',
        'app/control/app-control'
    ],
    function (Backbone,AppControl) {


        var transitions;

        var originalDimensions = {'ctas_pic_button':{'originalWidth':21,'originalHeight':21}};
        var slider = function(){};

        

        var receiveUpdates = 0;

        slider.theTimeout = null;
    
        slider.slides=[
            {colors:{r:0, g:54, b:91}},    
            {colors:{r:24, g:126, b:80}},   
            {colors:{r:120, g:86, b:136}},   
            {colors:{r:42, g:124, b:151}},   
            {colors:{r:191, g:174, b:95}}, 
            {colors:{r:24, g:126, b:80}},  
        ];

        slider.viewportWidth = 200;

        slider.sliderWidth = 200;

        slider.sliderSpeed = 2000;

        slider.sliderCount = 1;

        slider.lastSliderCount = 0;

        slider.oldLeft = 0;

        slider.theStop=function()
        {
            clearTimeout(this.theTimeout);
        };

        slider.initialize=function()
        {

            if(!document.getElementById('home_slider')){return;}

            var self = this;

            var parentWidth = $('#home_slider').width();

            document.getElementById('home_phone_slider_container')
            .style.width = Math.floor(((69.5/100)*parentWidth))+"px";
            
            //get the actual width of the viewport
            this.viewportWidth = 
            $('#home_phone_slider_container').width();



            //now set the width of the container of the slides
            //this is based on the number of slides
            //available.
            var slides = document.getElementById('slides');

            this.sliderWidth = this.viewportWidth*this.slides.length;

            slides.style.width= this.sliderWidth+"px";

            //now set the single slides to a particular width
            $('.slides li').css('width',this.viewportWidth+'px');

            //after the ul object's width is set, we will now
            //get its height and set the home_phone_slider_container's
            //height the same
            var h = slides.offsetHeight;
            $('#home_phone_slider_container')
            .css('height',h+'px');


            clearTimeout(this.theTimeout);
            this.theTimeout = setTimeout(function()
            {
                self.slide_now();

            },this.sliderSpeed);

        };

        slider.slide_now=function(){

            var self = this;

            var posNow = $('#slides').position().left;
            var posNext = posNow-this.viewportWidth;

            if(posNext<0){
                if((this.sliderWidth+posNext)<this.viewportWidth){
                    posNext = 0;
                    this.sliderCount = 0;
                };
            }

            var d = function(viewportWidth){

                clearTimeout(self.theTimeout);
                self.theTimeout = setTimeout(function(){

                    /*if(posNext>-1){
                        resetScrollerElements(viewportWidth);
                    }else{*/
                        self.slide_now();
                    //}

                },self.sliderSpeed);
            };

            var resetScrollerElements = function(viewportWidth){

                //var sw = sliderWidth;
                $('#slides').parent().css('width',viewportWidth+'px');
                $('#slides').css('left','0px');

                self.slide_now();

                /*var numListed = $('#slides').find('li').length;
                $('#slides').find('li').each(function(a,b){


                    if(a==(numListed-1)){
                        self.slide_now();
                    }

                });*/

                
                

            };

            var rn = 255;
            var gn = 255;
            var bn = 255;

            if(this.lastSliderCount==(this.slides.length-1)){

                this.lastSliderCount = 0;
            }

            //starting colors
            var r = this.slides[this.lastSliderCount].colors.r;
            var g = this.slides[this.lastSliderCount].colors.g;
            var b = this.slides[this.lastSliderCount].colors.b;

            //ending colors
            var r1 = this.slides[this.sliderCount].colors.r;
            var g1 = this.slides[this.sliderCount].colors.g;
            var b1 = this.slides[this.sliderCount].colors.b;

            var rdir = r>r1?"m":"a";
            var gdir = g>g1?"m":"a";
            var bdir = b>b1?"m":"a";

            var rdiff = r>r1?r-r1:r1-r;
            var gdiff = g>g1?g-g1:g1-g;
            var bdiff = b>b1?b-b1:b1-b;


            var posDiffx = this.oldLeft>posNext?
            (this.oldLeft-posNext):(posNext-this.oldLeft);



            if(this.sliderCount==this.slides.length-1){this.sliderCount=0;}
            this.lastSliderCount = this.sliderCount;




            $('#slides')
            .animate({left:posNext+"px"},{

                step:function(now,elem){

                    if(elem.prop=="left"){

                    var posDiff = now>posNext?
                    (now-posNext):(posNext-now);

                    var perc = 100 - ((posDiff/posDiffx)*100);

                        var rNow = (perc/100)*rdiff;
                        if(rdir=="m"){rn = r-rNow;}else{rn = r+rNow;}

                        var bNow = (perc/100)*bdiff;
                        if(bdir=="m"){bn = b-bNow;}else{bn = b+bNow;}

                        var gNow = (perc/100)*gdiff;
                        if(gdir=="m"){gn = g-gNow;}else{gn = g+gNow;}

                        $("#bg_colour")
                        .css('background-color',
                            'rgb('+Math.round(rn)+','+
                                Math.round(gn)+','+
                                    Math.round(bn)+')');
                    }

                },

                complete:function()
                {

                    self.oldLeft = posNext;
                    self.sliderCount++;
                    d(self.viewportWidth);
                },

                duration:800
            });
            

        };


        transitions = 
        {

            ctaFadeDir : "down",

            ctaFadeOpacity : 0,

            cta_growth : {'up':300,'down':240},

            cta_timeout: null,

            thanksMsgs:['thanks_main_header',
                        'thanks_small_msgs',
                        'thanks_email_entry_block',
                        'thanks_email_share_block'],

            thanksMsgsLoadedCount:0,

            theTimeout:null,

            self:this,

            slider:new slider(),

            test:slider.prototype.test=function()
            {
                slider.initialize();
            },

            theStop:slider.prototype.theStop=function()
            {
                slider.theStop();
            },

            fade_out_page_0:function()
            {
                var self = this;
                $( "#home" ).animate({
                    opacity:0
                  }, 500, function() {
                    // Animation complete.
                    $("#home_content").hide();
                    $(".page1").show();
                    $(".page2").hide();
                    self.fade_in_page_1();
                });
            },

            fade_in_page_1:function()
            {
                var self = this;
                //slide this to a height of 478px

                $( "#home" ).animate({
                    opacity:1
                  }, 20, function() {
                    self.open_picture_pocket();
                });
            },

            open_picture_pocket:function()
            {
                var self = this;
                //slide this to a height of 478px

                $( "#add_picture_pocket" ).animate({
                    height:'478px'
                  }, 500, function() {
                    self.move_phone_in_place();
                });

                $('#tag_friend_area').animate({opacity:1}, 500);
            },

            move_phone_in_place:function()
            {

                var self = this;
                //slide this to a height of 478px

                $( "#add_picture_pocket_phone" ).animate({
                    top:0,
                    opacity:1
                  }, 1000, function() {

                    self.pulsate('ctas_button','up');

                });

            },

            move_phone_for_thanks:function()
            {

                var self = this;
                //slide this to a height of 478px

                //$('#add_picture_pocket_phone .pic_display_case').hide();

                var rn = 255;
                var gn = 255;
                var bn = 255;

                //starting colors
                var r = 255;
                var g = 255;
                var b = 255;

                //ending colors
                var r1 = 30;
                var g1 = 65;
                var b1 = 113;

                var rdir = r>r1?"m":"a";
                var gdir = g>g1?"m":"a";
                var bdir = b>b1?"m":"a";

                var rdiff = r>r1?r-r1:r1-r;
                var gdiff = g>g1?g-g1:g1-g;
                var bdiff = b>b1?b-b1:b1-b;

                $( "#add_picture_pocket_phone" ).animate({
                  left:365,
                  top:55,
                  width:'430px'
                }, {
                  step: function( now, fx ) {
                    if(fx.prop=="top")
                    {
                        var perc = ((now/55)*100);

                        var rNow = (perc/100)*rdiff;
                        if(rdir=="m"){rn = r-rNow;}else{rn = r+rNow;}

                        var bNow = (perc/100)*bdiff;
                        if(bdir=="m"){bn = b-bNow;}else{bn = b+bNow;}

                        var gNow = (perc/100)*gdiff;
                        if(gdir=="m"){gn = g-gNow;}else{gn = g+gNow;}

                        $("#page_background_tint")
                        .css('background-color',
                            'rgb('+Math.round(rn)+','+
                                Math.round(gn)+','+
                                    Math.round(bn)+')');
                        
                    }
                  },

                  duration:500,

                  complete:function()
                  {
                     $(this).animate({top:'17px'}, 1000, function()
                     {

                        for(var i=0; i<self.thanksMsgs.length; i++)
                        {
                            $('#'+self.thanksMsgs[i])
                            .css('opacity',0);
                        }

                        $('.thanks_main_window_left')
                        .css('display','block');

                        self.popInThanksMessages();

                     });
                  }
                });

            },

            popInThanksMessages:function()
            {
                Backbone.trigger('app:pageview', 'view/thankyou');

                this.displayBlock(this.thanksMsgs[this.thanksMsgsLoadedCount]);
                //increment to keep a count of 
                //the thanks messages loaded
                this.thanksMsgsLoadedCount++;
                if(this.thanksMsgsLoadedCount<this.thanksMsgs.length)
                {
                    this.xxx('thanks',0,80);
                }
                else
                {

                    $('#tag_friend_area')
                    .animate({opacity:0}, 200, function()
                    {
                        $('#tag_friend_area')
                        .css('display','none');

                        $('#tag_friend_area_cover')
                        .css('display','none');
                    });


                    $( "#tag_gray_area" ).animate({
                        height:'306px'
                    }, 300, function() {
                        
                        $('#thanks_sec_window_left')
                        .css('opacity',0);

                        $('.thanks_sec_window_right').
                        css('opacity',0);

                        $('.thanks_sec_window')
                        .css('display','block');


                        $('#thanks_sec_window_left')
                        .animate({top:'-46px', opacity:1});

                        $('.thanks_sec_window_right')
                        .animate({opacity:1},200);

                    });
                }
                
            },

            displayBlock:function(elem)
            {
                $('#'+elem).css('opacity',1);
            },

            xxx:function(msg,count,delay)
            {
                var self = this;
                clearTimeout(this.theTimeout);
                this.theTimeout = setTimeout(function()
                {
                    switch(msg){
                        case "thanks":{  
                            self.popInThanksMessages();
                            break;
                        }

                        case "pic_buttons":{  
                            self.show_pic_buttons(count);
                            break;
                        }

                        case "pulsing":{
                            self.pulsate('ctas_button','up'); 
                            break;
                        }

                        case "pulsing_m":{
                            self.pulsate('pulsing_m','up'); 
                            break;
                        }

                        case "pulsing_w":{
                            self.pulsate('ctas_button','up'); 
                            break;
                        }

                        case "blink_text":{
                            self.blink_text_entry(); 
                            break;
                        }

                        case "blink_text_mobile":{
                            self.blink_text_entry_mobile(); 
                            break;
                        }

                        case "blink_tag_button":{
                            self.blink_tag_button(); 
                            break;
                        }
                    }

                },delay);
            },

            show_pic_buttons:function(buttonCount)
            {

                if(buttonCount==0)
                {
                    $('.source_button')
                    .css('opacity',0);

                    $('.pic_source_block').fadeIn();

                    $('#fb_source_button').css('opacity',1);
                }



                buttonCount++;
                if(buttonCount==4)
                {
                    clearTimeout(this.theTimeout);
                }
                else
                {
                    switch(buttonCount)
                    {
                        case 2:{
                    $('#ist_source_button').css('opacity',1);
                    break;}
                        case 3:{
                    $('#hw_source_button').css('opacity',1);
                    break;}
                    }

                    this.xxx('pic_buttons',buttonCount,80);

                }

            },

            open_friends:function()
            {
                $('.pic_source_block').hide();
                //open facebook popup
                $('.popups').css('opacity',0);
                $('#friends_box').show();
                $('.popups').show();
                $( ".popups" ).animate({opacity:1}, 200);
            },

            close_friends:function(pic_selected)
            {

                var self = this;
                //when the photo selection is about to close
                //we must know what entry steps have been
                //completed.

                //If a photo has not been chosen we will have to 
                //take the user back to that call to action screen  
                
                $( ".popups" ).animate({opacity:0}, 200,function()
                {
                    $('.popups').hide();
                    $('.popups').css('opacity',1);
                    $('#friends_box').hide();

                    if(pic_selected==false)
                    {
                        self.show_pic_buttons(0);
                    }
                });
            },

            toggle_friend_list_box:function(state){

                switch(state){

                    case "on":{

                        $('#friend_list_box').css('display','block');
                        $('.popups').css('display','block');

                        break;
                    }

                    default:
                    {
                        $('#friend_list_box').css('display','none');
                        $('.popups').css('display','none');
                    }
                }

            },

            toggle_crop_tool:function(state,device,direction)
            {
                var self = this;
                switch(state){

                    case "off":{
                        
                        //now that we have selected and cropped our
                        //photo, we will call the next action
                        if(device=="mobile"){

                            $('#mobile_pinching').hide();

                            $('.cta_exp_extry').css('opacity',0);
                            
                            //Make sure that this is not in focus.
                            $('.cta_exp_extry').blur();

                            var cloned_img = $('#cloned_img');

                            var heightOfDisplayArea = 
                            $(cloned_img).position().top
                                +$(cloned_img).height();


                            if(heightOfDisplayArea<$('#add_picture_pocket_m').height())
                            {

                                //$('.pic_display_ctas_textarea').css('display','block');
                                    //self.blink_text_entry_mobile();

                            

                            $('#add_picture_pocket_m')
                            .animate({'height':heightOfDisplayArea+"px"},
                            {
                                complete:function()
                                {

                                    $('#pic_display_m')
                                    .css('height',heightOfDisplayArea+'px');

                                    //$('.pic_display_ctas_textarea').css('display','block');
                                    $('.textWordCount_m').fadeIn();
                                    self.blink_text_entry_mobile();
                                    
                                }
                            });

                            }else{

                                //$('.pic_display_ctas_textarea').css('display','block');
                                $('.textWordCount_m').fadeIn();
                                self.blink_text_entry_mobile();

                            }





                            

                        }else{


                            $('#crop_tool').hide();

                            if(direction == "forward"){

                                $('.pic_display_ctas_textarea_input').css('opacity',0);
                                
                                //Make sure that this is not in focus.
                                $('.pic_display_ctas_textarea_input').blur();

                                $('.pic_display_ctas_textarea').css('display','block');
                                $('.textWordCount').fadeIn();
                                this.blink_text_entry();

                            }

                        }

                        break;
                    }

                    default:{
                        $('#crop_tool').show();
                    }
                    
                }
            },

            blink_text_entry:function()
            {
                var opacity = $('.pic_display_ctas_textarea_input').css('opacity');
                if(opacity==1){opacity=0;}else{opacity=1;}

                var self = this;
               $('.pic_display_ctas_textarea_input')
                .animate({opacity:opacity},
                {
                    complete:function()
                    {
                        $('.pic_display_ctas_textarea_input').focus();
                        self.xxx('blink_text',0,80);
                    }
                });

            },

            blink_text_entry_mobile:function()
            {
                var opacity = $('.cta_exp_extry').css('opacity');
                if(opacity==1){opacity=0;}else{opacity=1;}

                var self = this;
               $('.cta_exp_extry')
                .animate({opacity:opacity},
                {
                    complete:function()
                    {
                        //$('.cta_exp_extry').focus();
                        self.xxx('blink_text_mobile',0,80);
                    }
                });

            },

            blink_tag_button:function()
            {
                var opacity = $('#add_f_button').css('opacity');
                if(opacity==1){opacity=0;}else{opacity=1;}

                var self = this;
               $('#add_f_button')
                .animate({opacity:opacity},
                {
                    complete:function()
                    {
                        self.xxx('blink_tag_button',0,80);
                    },

                    easing:'easeOutQuad',

                    duration:1000

                });

            },

            toggle_typeNowPopup:function(device, state){

                var newOpacity = 1;
                switch(device){

                    case "web":{

                        if(state == "on"){

                            $('.typeNowPopup').css({
                                'opacity':0,
                                'display':'block'
                            });
                        }else{
                            newOpacity = 0;
                        }

                        $('.typeNowPopup')
                        .animate({opacity:newOpacity},{
                            complete:function(){
                                if(state != "on"){

                                    $('.typeNowPopup').css({
                                        'opacity':1,
                                        'display':'none'
                                    });

                                }
                            },
                            duration:500
                        });


                        break;
                    }
                    
                    default:{


                        if(state == "on"){

                            $('.typeNowPopup_m').css({
                                'opacity':0,
                                'display':'block'
                            });
                        }else{
                            newOpacity = 0;
                        }

                        $('.typeNowPopup_m')
                        .animate({opacity:newOpacity},{
                            complete:function(){
                                if(state != "on"){

                                    $('.typeNowPopup_m').css({
                                        'opacity':1,
                                        'display':'none'
                                    });
                                    
                                }
                            },
                            duration:500
                        });


                        

                    }
                    
                }

            },

            toggle_tac_check:function()
            {
                var check_on = 
                $('#front_page_check_box .checked').css('display');

                switch(check_on){
                    case "block":{
                        $('#front_page_check_box .checked').css('display','none');
                        Backbone.trigger('tac_toggled','off');
                        break;
                    }

                    default:{
                        $('#front_page_check_box .checked').css('display','block');
                        Backbone.trigger('tac_toggled','on');
                    }
                }
            },

            get_ready_to_grow:function(){

                this.cta_growth.up = 
                $('#select_source_cta_m').width();

                this.cta_growth.down = 
                Math.round(this.cta_growth.up - (this.cta_growth.up*(20/100)));

                this.cta_growth.diff = 
                this.cta_growth.up - this.cta_growth.down;

                this.grow_elem('down');

            },

            grow_elem:function(dir){

                var newWidth, self = this;

                var wNow = $('#select_source_cta_m').width();

                $('#cta_action_glow_m').animate(
                {'opacity':self.ctaFadeOpacity},{
                    
                    step:function(a,b){

                        if(b.prop == "opacity"){
                            perc = ((b.now/1)*100);

                            newWidth = 
                                self.cta_growth.down + ((perc/100)*self.cta_growth.diff);
                            $('#select_source_cta_m').css({'width':newWidth+"px"});
                            //$('#select_source_cta_m').css({'height':newWidth+"px"});
                        }

                    },
                    
                    complete:function(){
                      //alert('done' + opac +" -- "+ dir);
                      self.switch_direction();
                      self.pause_blinking(dir);
                    },

                    duration:1000
                });
            },

            switch_direction:function()
            {
                if(this.ctaFadeDir == "up")
                { this.ctaFadeDir = "down"; this.ctaFadeOpacity=0; }
                else{ this.ctaFadeDir = "up"; this.ctaFadeOpacity=1; }
            },

            pause_blinking:function(dir)
            {
                var self = this;
                this.cta_timeout = setTimeout(function(){
                    self.grow_elem(dir);

                },200);
            },

            stop_blinking:function()
            {

                //Stop the elements from transforming
                $('#cta_action_glow_m').stop();
                 clearTimeout(this.cta_timeout);

                 //Reset the elements back to their original state.
                 $('#select_source_cta_m').css({'width':this.cta_growth.up+"px"});
                 $('#cta_action_glow_m').css({'opacity':0});
            },

            pulsate:function(what,dir)
            {

                var picContainer, glowContainer;

                switch(what){
                    case "ctas_button":{
                        picContainer = $('.pic_display_ctas_pic_button');
                        glowContainer = $('.pic_display_ctas_pic_glow');
                        break;
                    }

                    case "pulsing_m":{
                        picContainer = $('.cta_area_action_img');
                        glowContainer = $('.cta_action_glow_m');
                        break;
                    }
                }

                var self = this;

                var percGrowth = 20;
                //ctas_button
                var wNow = $(picContainer).width();
                var hNow = $(picContainer).height();
                var wDiff,ease,dur;


                switch(dir){
                    case "up":{
                        originalDimensions.ctas_pic_button.originalWidth = wNow;
                        originalDimensions.ctas_pic_button.originalHeight = hNow;
                        //{'ctas_pic_button':{'originalWidth':21,'originalHeight':21};
                        newWidth = wNow+((percGrowth/100)*wNow);
                        wDiff = newWidth-wNow;
                        newHeight = hNow+((percGrowth/100)*hNow);
                        ease = "easeOutQuad";
                        dur = 400;
                        break;
                    }

                    default:{

                        var presentW = wNow;
                        //var = oldH = hNow;
                        newWidth = originalDimensions.ctas_pic_button.originalWidth;
                        newHeight = originalDimensions.ctas_pic_button.originalHeight;

                        //newWidth = wNow-((percGrowth/100)*wNow);
                        wDiff = presentW-newWidth;
                        //newHeight = hNow-((percGrowth/100)*hNow);
                        ease = "easeOutCubic";
                        dur = 1000;
                    }
                }

                $(glowContainer).animate({opacity:1},300);

                //pic_display_ctas_pic_button
                $(picContainer)
                .animate({width:newWidth+'px', height:newHeight+'px'},
                {
                    step:function(now,elem)
                    {
                        if(elem.prop=="width")
                        {
                            var perc = (((elem.now-wNow)/wDiff)*100);
                            if(dir=="up"){
                            perc = (((elem.now-wNow)/wDiff)*100);}else{
                            perc = (((elem.now-newWidth)/wDiff)*100);}

                            // $(glowContainer)
                            // .css('opacity',Math.round((perc/100)*1));
                            
                        }
                    },

                    complete:function()
                    {

                        if(dir=="up"){dir="down";}else{dir="up";}
                        if(dir=="down"){self.pulsate(what,dir);}
                        else{
                            switch(what){
                                case "ctas_button":{
                                    self.xxx('pulsing',0,500);
                                    break;
                                }

                                case "pulsing_m":{
                                    self.xxx('pulsing_m',0,500);
                                    break;
                                }
                            }
                            
                        }
                        $(glowContainer).animate({opacity:0},300);
                    },
                    queue: false,
                    easing:ease,

                    duration:dur
                });
            },

            prepare_friend_tagging:function(device){
                //tag_friend_area_cover
                this.uncover_friend_tagging(device);
            },

            uncover_friend_tagging:function(device){
                var self = this;

                if(device=="mobile")
                {
                    $('.no_thanks_button_area')
                    .css('display','block');
                }
                //tag_friend_area_cover
                $('#tag_friend_area_cover')
                .animate({opacity:0},
                {
                    complete:function()
                    {
                        if(device=="web"){
                        Backbone.trigger('showAppropriateBar','bring_a_friend_bar');
                        }else{
                        Backbone.trigger('showAppropriateBar','mobile_bar_bring_along');
                        }

                        $(this).css({'display':'none','opacity':1});
                        setTimeout(function(){self.blink_tag_button();},400)
                    }
                });
            },

            getdiff:function(v1,v2){
                if(v1>v2){
                    return {'diff':(v1-v2), 'dir':'down'};
                }else{
                    return {'diff':(v2-v1), 'dir':'up'};
                }
            },

            is_email_entered:function()
            {
                var left = "47%",
                theEmail = $('#email_entry').val();

                var theReturn=true;
                var emptyStrExp=/(^\s*$)/;
                if(emptyStrExp.test(theEmail)){theReturn=false;}
                
                if(theReturn){
                var EmailExp=/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                if(!EmailExp.test(theEmail)){theReturn=false;}
                }

                return theReturn;

            },

            slide_email_entry_check:function(pos){

                $('#email_entry').removeClass('error');

                if(!this.is_email_entered()){

                    $('#email_entry').addClass('error');
                    $('#email_entry').val('Enter a valid email');

                    return;
                }

                if(receiveUpdates==0){
                    receiveUpdates=1; pos="on"; left = "47%"; rgb = 'rgb(155,210,80)';
                }else{receiveUpdates=0; pos="off"; left = "2%"; rgb = 'rgb(194,56,47)';}

                //disable the email entry box
                //$('#email_entry').prop('disabled', true);

                var current = 'on';
                var colors = [{'r':155,'g':210,'b':80},{'r':155,'g':210,'b':80}];
                var count = 0;

                var use = 0,
                diff_r, diff_g, diff_b;

                diff_r = this.getdiff(colors[1].r, colors[0].r);
                diff_g = this.getdiff(colors[1].g, colors[0].g);
                diff_b = this.getdiff(colors[1].b, colors[0].b);

                var color_r = colors[use].r;
                var color_g = colors[use].b;
                var color_b = colors[use].b;



                


                $('.m_thank_email_slider').animate({
                    'left':left
                },{
                    step:function(a,b){

                        /*if(b.prop=="left"){

                            var nowPerc = ((a/600)*100);
                            var r,g,b;


                            if(diff_r.dir=="down"){
                                r = color_r-((nowPerc/100)*diff_r.diff);
                            }else{
                                r = color_r+((nowPerc/100)*diff_r.diff);
                            }

                            if(diff_g.dir=="down"){
                                g = color_g-((nowPerc/100)*diff_g.diff);
                            }else{
                                g = color_g+((nowPerc/100)*diff_g.diff);
                            }

                            if(diff_b.dir=="down"){
                                b = color_b-((nowPerc/100)*diff_b.diff);
                            }else{
                                b = color_b+((nowPerc/100)*diff_b.diff);
                            }

                            $('.m_thank_email_right')
                            .css('background-color',
                                'rgb('+Math.round(r)+','+
                                    Math.round(g)+','+
                                    Math.round(b)+')');

                        }*/

                    },
                    complete:function(){

                        $('.m_thank_email_right')
                        .css('background-color',rgb);

                        //send_email_updates
                        Backbone.trigger('send_email_updates','mobile');

                    },
                    duration:200
                });


            }


        };

        return transitions;

});