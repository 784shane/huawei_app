$(document).ready(function () {
    $('a.button.remove').on('click', function () {
        if (confirm('This action cannot be undone. Do you wish to continue?')) {
            window.location.href = $(this).data('href');
        }
    });
});