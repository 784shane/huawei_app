$(document).ready(function () {
    
    var loading = false,
        end = false,
        trigger = 200,
        next = 2,
        experienceTpl = $('#experience-tpl').html(),
        $experiences = $('ul.experiences');

    var renderExperience = function (experience) {
        var h = _.template(experienceTpl, experience);
        $experiences.append(h);
    };

    var loadExperiences = function () {
        if (!loading && !end) {
            loading = true;
            $experiences.addClass('loading');
            $.get('/cms/experiences?page=' + next).done(function (result) {

                if (result.experiences && result.experiences.length) {
                    for (var i in result.experiences) {
                        renderExperience(result.experiences[i]);
                    }
                } else {
                    end = true;
                }
                next++;
            }).always(function () {
                loading = false;
                $experiences.removeClass('loading');
            });
        }
    };

    $(window).on('scroll', function () {
        var limit = $experiences.height() - window.innerHeight - trigger,
            scrollTop = $(window).scrollTop();

        if (scrollTop >= limit) {
            loadExperiences();
        }

        $('body').toggleClass('fixed', scrollTop > 200);
    });

});